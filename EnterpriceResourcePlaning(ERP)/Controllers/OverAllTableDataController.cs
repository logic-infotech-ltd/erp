﻿using EnterpriceResourcePlaning_ERP_.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EnterpriceResourcePlaning_ERP_.Controllers
{
    
    public class OverAllTableDataController : Controller
    {
        string connection = WebConfigurationManager.ConnectionStrings["ErpModelEntities"].ConnectionString;
        // GET: OverAllTableData
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllDataJsonfileCreate()
        {
            SqlConnection conn = new SqlConnection(connection);


            //////////1. For Module

            string modulequery = "SELECT [ModuleId] ,[ApplicationId] ,[ExistingModuleId] ,[IsNewCopy] ,[ModuleName] ,[ModuleCaption] ,[ReportTitle] "
                + ",[IsDrilldownReport] ,[CategoryId] ,[ImagePath] ,[ModuleType] ,[SlNo] ,[IsActive] ,[DeveloperSyncId] ,[Entry_User_Id]"
                + " ,[Entry_By] ,[Entry_Date] ,[Update_By] ,[Update_Date] ,[Check_By] ,[Check_Date] ,[Approve_By] ,[Approve_Date] ,[Delete_By] "
                + ",[Delete_Date] ,[Is_Approved]  ,[Is_Active] ,[Is_Deleted] ,[Work_Follow_Position] ,[Row_Id] ,[Cancel_By] ,[Cancel_Date]"
                + " ,[Is_Cancel] FROM [dbo].[Utl_Dynamic_Module]";

            SqlCommand modulCmd = new SqlCommand(modulequery, conn);
            conn.Open();
            SqlDataReader modulReader = modulCmd.ExecuteReader();

            var ModuleList = new List<Utl_Dynamic_Module>();
            while (modulReader.Read())
            {

                long ModuleId = long.Parse(modulReader["ModuleId"].ToString());
                int applicationId = Convert.ToInt32(modulReader["ApplicationId"]);
                long ExiModulId = long.Parse(modulReader["ExistingModuleId"].ToString());
                bool isNewCopy = bool.Parse(modulReader["IsNewCopy"].ToString());

                string moduleName = modulReader["ModuleName"].ToString();
                string moduleCaption = modulReader["ModuleCaption"].ToString();
                string reportTitle = modulReader["ReportTitle"].ToString();
                bool isDrillDownReport = bool.Parse(modulReader["IsDrilldownReport"].ToString());

                int categoryId = Convert.ToInt32(modulReader["CategoryId"]);
                string imagePath = modulReader["ImagePath"].ToString();
                int moduleType = Convert.ToInt32(modulReader["ModuleType"]);
                decimal siNo = decimal.Parse(modulReader["SlNo"].ToString());

                bool isActive = bool.Parse(modulReader["IsActive"].ToString());
                string developerSyId = modulReader["DeveloperSyncId"].ToString();
                string rowId = modulReader["Row_Id"].ToString();

                long entryUserId = (modulReader["Entry_User_Id"] != DBNull.Value) ? Convert.ToInt64(modulReader["Entry_User_Id"]) : 0;

                string entryBy = modulReader["Entry_By"].ToString();

                DateTime? entryDate = (modulReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(modulReader["Entry_Date"]);

                string updateBy = modulReader["Update_By"].ToString();
                DateTime? updateDate = (modulReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(modulReader["Update_Date"]);

                string checkBy = modulReader["Check_By"].ToString();
                DateTime? checkDate = (modulReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(modulReader["Check_Date"]);

                string approveBy = modulReader["Approve_By"].ToString();
                DateTime? approveDate = (modulReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(modulReader["Approve_Date"]);

                string deleteBy = modulReader["Delete_By"].ToString();
                DateTime? deleteDate = (modulReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(modulReader["Delete_Date"]);

                string cancelBy = modulReader["Cancel_By"].ToString();
                DateTime? cancelDate = (modulReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(modulReader["Cancel_Date"]);

                bool isApproved = bool.Parse(modulReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(modulReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(modulReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(modulReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(modulReader["Work_Follow_Position"]);


                Utl_Dynamic_Module ModulVM = new Utl_Dynamic_Module(ModuleId, applicationId, ExiModulId, isNewCopy, moduleName, moduleCaption, reportTitle, isDrillDownReport, categoryId,
                    imagePath, moduleType, siNo, isActive, developerSyId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                    deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                ModuleList.Add(ModulVM);
            }
            modulReader.Close();


            ////////////////2. For Table
            ///
            string Tablequery = "SELECT [TableId] ,[ModuleId], ParentModuleId, ForeignModuleId, [ApplicationId] ,[TableCaption] ,[ModuleTableCaption],[TableName] ,[RefreshType] ,[ParentTableId] "
                + ",[ForeignTableId]  ,[IsExternalDataShow] ,[ExternalDataCaption] ,[ExternalDataFilter]  ,[ExistingTableId]  ,[ModuleWidth] ,[ModuleHeight] ,[FormWidth]"
                + " ,[PositionExp] ,[RowPosition] ,[ColPosition] ,[IsReadOnly] ,[IsWorkFollow] ,[IsAutoToggle] ,[ProcedureName] ,[ProcedureCaption] ,[ProcedureValidation]"
                + " ,[ProcFireOnTrans],[ReportProcedureName] ,[RptProcForWorksheet] ,[ProcFooterView],[ProcFooterDml] ,[ReportFileName] ,[ShowOnReport],[SqlStatement], "
                + "[ObjectType] ,[TemplateType] ,[SlNo]  ,[DeveloperSyncId] ,[Entry_User_Id] ,[Entry_By]  ,[Entry_Date] ,[Update_By] ,[Update_Date] ,[Check_By],"
                + "[Check_Date]  ,[Approve_By] ,[Approve_Date] ,[Delete_By] ,[Delete_Date] ,[Is_Approved] ,[Is_Active] ,[Is_Deleted] ,[Work_Follow_Position],[Row_Id],"
                + "[Cancel_By] ,[Cancel_Date] ,[Is_Cancel] FROM [Utl_Dynamic_Table]";

            SqlCommand tableCmd = new SqlCommand(Tablequery, conn);
            SqlDataReader tableReader = tableCmd.ExecuteReader();

            var TableList = new List<Utl_Dynamic_Table>();
            while (tableReader.Read())
            {
                long tableId = long.Parse(tableReader["TableId"].ToString());
                long moduleId = long.Parse(tableReader["ModuleId"].ToString());
                long parentModuleId = long.Parse(tableReader["ParentModuleId"].ToString());
                long foreignModuleId = long.Parse(tableReader["ForeignModuleId"].ToString());
                long applicationId = (tableReader["ApplicationId"] != DBNull.Value) ? Convert.ToInt64(tableReader["ApplicationId"]) : 0;

                string tableCaption = tableReader["TableCaption"].ToString();
                string moduleTblCaption = tableReader["ModuleTableCaption"].ToString();
                string tableName = tableReader["TableName"].ToString();

                string refreshType = tableReader["RefreshType"].ToString();

                long parentTableId = long.Parse(tableReader["ParentTableId"].ToString());
                long foreignTableId = long.Parse(tableReader["ForeignTableId"].ToString());

                bool isExternalDataShow = bool.Parse(tableReader["IsExternalDataShow"].ToString());
                string externalDataCaption = tableReader["ExternalDataCaption"].ToString();
                string externalDataFilter = tableReader["ExternalDataFilter"].ToString();

                long existingTableId = (tableReader["ExistingTableId"] != DBNull.Value) ? Convert.ToInt64(tableReader["ExistingTableId"]) : 0;
                int moduleWidth = Convert.ToInt32(tableReader["ModuleWidth"]);
                int? moduleHeight = (tableReader["ModuleHeight"] != DBNull.Value) ? Convert.ToInt32(tableReader["ModuleHeight"]) : 0;
                int formWidth = Convert.ToInt32(tableReader["FormWidth"]);

                string positionExp = tableReader["PositionExp"].ToString();
                int rowPosition = Convert.ToInt32(tableReader["RowPosition"]);
                int colPosition = Convert.ToInt32(tableReader["ColPosition"]);

                bool isReadonly = bool.Parse(tableReader["IsReadOnly"].ToString());
                bool isWorkFollow = bool.Parse(tableReader["IsWorkFollow"].ToString());
                bool isAutoToggle = bool.Parse(tableReader["IsAutoToggle"].ToString());

                string procedureName = tableReader["ProcedureName"].ToString();
                string procedureCaption = tableReader["ProcedureCaption"].ToString();
                string procedureValidation = tableReader["ProcedureValidation"].ToString();

                bool proFireOntrans = bool.Parse(tableReader["ProcFireOnTrans"].ToString());
                string reportProcName = tableReader["ReportProcedureName"].ToString();
                bool rptProcForWorksheet = bool.Parse(tableReader["RptProcForWorksheet"].ToString());

                string procFooterView = tableReader["ProcFooterView"].ToString();
                string procFooterDml = tableReader["ProcFooterDml"].ToString();
                string rptFileName = tableReader["ReportFileName"].ToString();

                bool showOnReport = bool.Parse(tableReader["ShowOnReport"].ToString());
                string sqlStatement = tableReader["SqlStatement"].ToString();

                string objectType = tableReader["ObjectType"].ToString();
                string templateType = tableReader["TemplateType"].ToString();

                decimal slNo = decimal.Parse(tableReader["SlNo"].ToString());
                string developerSynId = tableReader["DeveloperSyncId"].ToString();
                string rowId = tableReader["Row_Id"].ToString();


                long entryUserId = (tableReader["Entry_User_Id"] != DBNull.Value) ? Convert.ToInt64(tableReader["Entry_User_Id"]) : 0;

                string entryBy = tableReader["Entry_By"].ToString();
                DateTime? entryDate = (tableReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableReader["Entry_Date"]);

                string updateBy = tableReader["Update_By"].ToString();
                DateTime? updateDate = (tableReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableReader["Update_Date"]);

                string checkBy = tableReader["Check_By"].ToString();
                DateTime? checkDate = (tableReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableReader["Check_Date"]);

                string approveBy = tableReader["Approve_By"].ToString();
                DateTime? approveDate = (tableReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableReader["Approve_Date"]);

                string deleteBy = tableReader["Delete_By"].ToString();
                DateTime? deleteDate = (tableReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableReader["Delete_Date"]);

                string cancelBy = tableReader["Cancel_By"].ToString();
                DateTime? cancelDate = (tableReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableReader["Cancel_Date"]);

                bool isApproved = bool.Parse(tableReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(tableReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(tableReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(tableReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(tableReader["Work_Follow_Position"]);


                Utl_Dynamic_Table TableVM = new Utl_Dynamic_Table(tableId, moduleId, parentModuleId, foreignModuleId, applicationId, tableCaption, moduleTblCaption, tableName, refreshType, parentTableId, foreignTableId,
                    isExternalDataShow, externalDataCaption, externalDataFilter, existingTableId, moduleWidth, moduleHeight, formWidth, positionExp, rowPosition, colPosition, isReadonly,
                    isWorkFollow, isAutoToggle, procedureName, procedureCaption, procedureValidation, proFireOntrans, reportProcName, rptProcForWorksheet, procFooterView, procFooterDml,
                    rptFileName, showOnReport, sqlStatement, objectType, templateType, slNo, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy,
                    checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                TableList.Add(TableVM);
            }
            tableReader.Close();



            /////////////3. For Table column 

            string TableColumnQuery = "SELECT [ColumnId],[TableId],ModuleId,LovModuleId,[ColumnName],[SelectColumnName],[ColumnDisplayName],[ColumnProperty],[ControlType],[ParentColumn],[ForeignKeyName]," +
                "[ForeignColumnName] ,[ForeignDataFilterBy],[ColumnWidth],[DbColumnWidth],[TransPolicyId],[GlobalParamId],[GlobalParamValue],[DefaultValue],[FilterBy],[User_Privilege]," +
                "[IsModuleSelect] ,[IsFvSelect],[IsGvSelect],[IsForeignTable],[IsDefaultFocus],[IsFvUpdate],[IsFvInsert] ,[IsFvDataFixed],[IsFvForceEdit],[DdlDataSource],[LovTableId]," +
                "[TableName],[ColumnValue],[ColumnText] ,[ColumnTextExp],[WhereExpression],[OrderByExpression] ,[SqlExpression],[LinkType],[LinkPageId],[LinkTableId],[LinkColumnName]," +
                "[LinkUrl] ,[IsShow],[ReportColumn],[SlNo],[DeveloperSyncId],[Entry_User_Id],[Entry_By],[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By]," +
                "[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active] ,[Is_Deleted],[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [Utl_Dynamic_Table_Column]";

            SqlCommand tableColumnCmd = new SqlCommand(TableColumnQuery, conn);
            SqlDataReader tableColumnReader = tableColumnCmd.ExecuteReader();

            var TableColumnList = new List<Utl_Dynamic_Table_Column>();


            while (tableColumnReader.Read())
            {

                long columnId = long.Parse(tableColumnReader["ColumnId"].ToString());
                long tableId = long.Parse(tableColumnReader["TableId"].ToString());
                long moduleId = long.Parse(tableColumnReader["ModuleId"].ToString());
                long lovModuleId = long.Parse(tableColumnReader["LovModuleId"].ToString());

                string columnName = tableColumnReader["ColumnName"].ToString();
                string selectColName = tableColumnReader["SelectColumnName"].ToString();
                string colDisplayName = tableColumnReader["ColumnDisplayName"].ToString();
                string colProperty = tableColumnReader["ColumnProperty"].ToString();
                string controlType = tableColumnReader["ControlType"].ToString();
                string parentcolumn = tableColumnReader["ParentColumn"].ToString();

                long foreignKeyName = (tableColumnReader["ForeignKeyName"] != DBNull.Value) ? Convert.ToInt64(tableColumnReader["ForeignKeyName"]) : 0;
                string foreignColName = tableColumnReader["ForeignColumnName"].ToString();
                string foreignDataFilterBy = tableColumnReader["ForeignDataFilterBy"].ToString();

                int columnWidth = Convert.ToInt32(tableColumnReader["ColumnWidth"].ToString());
                int dbColumnWidth = (tableColumnReader["DbColumnWidth"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["DbColumnWidth"]) : 0;
                int transPolicyId = Convert.ToInt32(tableColumnReader["TransPolicyId"].ToString());
                int globalParamId = (tableColumnReader["GlobalParamId"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["GlobalParamId"]) : 0;
                int globalParamValue = (tableColumnReader["GlobalParamValue"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["GlobalParamValue"]) : 0;

                string defaultValue = tableColumnReader["DefaultValue"].ToString();
                int filterBy = (tableColumnReader["FilterBy"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["FilterBy"]) : 0;
                int userPrivilege = (tableColumnReader["User_Privilege"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["User_Privilege"]) : 0;



                bool isModuleSelect = bool.Parse(tableColumnReader["IsModuleSelect"].ToString());
                bool isFvSelect = bool.Parse(tableColumnReader["IsFvSelect"].ToString());
                bool isGvSelect = bool.Parse(tableColumnReader["IsGvSelect"].ToString());
                bool? isForeignTable = (tableColumnReader["IsForeignTable"]) != DBNull.Value ? bool.Parse(tableColumnReader["IsForeignTable"].ToString()) : (bool?)null;
                bool? isDefaultFocus = (tableColumnReader["IsDefaultFocus"]) != DBNull.Value ? bool.Parse(tableColumnReader["IsDefaultFocus"].ToString()) : (bool?)null;

                bool isFvUpdate = bool.Parse(tableColumnReader["IsFvUpdate"].ToString());
                bool isFvInsert = bool.Parse(tableColumnReader["IsFvInsert"].ToString());
                bool? isFvDataFixed = (tableColumnReader["IsFvDataFixed"]) != DBNull.Value ? bool.Parse(tableColumnReader["IsFvDataFixed"].ToString()) : (bool?)null;
                bool? isFvForceEdit = (tableColumnReader["IsFvForceEdit"]) != DBNull.Value ? bool.Parse(tableColumnReader["IsFvForceEdit"].ToString()) : (bool?)null;




                int dblDataSource = (tableColumnReader["DdlDataSource"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["DdlDataSource"]) : 0;
                long lovTableId = (tableColumnReader["LovTableId"] != DBNull.Value) ? Convert.ToInt64(tableColumnReader["LovTableId"]) : 0;

                string tableName = tableColumnReader["TableName"].ToString();
                string columnValue = tableColumnReader["ColumnValue"].ToString();
                string columnText = tableColumnReader["ColumnText"].ToString();
                string columnTextExp = tableColumnReader["ColumnTextExp"].ToString();
                string WhereExpression = tableColumnReader["WhereExpression"].ToString();
                string orderByExpression = tableColumnReader["OrderByExpression"].ToString();
                string sqlExpression = tableColumnReader["SqlExpression"].ToString();

                string linkType = tableColumnReader["LinkType"].ToString();

                int linkPageId = (tableColumnReader["LinkPageId"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["LinkPageId"]) : 0;
                int linkTableId = (tableColumnReader["LinkTableId"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["LinkTableId"]) : 0;
                string linkColumnName = tableColumnReader["LinkColumnName"].ToString();



                string linkUrl = tableColumnReader["LinkUrl"].ToString();
                bool isShow = bool.Parse(tableColumnReader["IsShow"].ToString());
                int reportColumn = (tableColumnReader["ReportColumn"]) != DBNull.Value ? Convert.ToInt32(tableColumnReader["ReportColumn"]) : 0;

                decimal slNo = decimal.Parse(tableColumnReader["SlNo"].ToString());
                string DeveloperSynId = tableColumnReader["DeveloperSyncId"].ToString();
                string rowId = tableColumnReader["Row_Id"].ToString();


                long entryUserId = (tableColumnReader["Entry_User_Id"] != DBNull.Value) ? Convert.ToInt64(tableColumnReader["Entry_User_Id"]) : 0;

                string entryBy = tableColumnReader["Entry_By"].ToString();
                DateTime? entryDate = (tableColumnReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableColumnReader["Entry_Date"]);

                string updateBy = tableColumnReader["Update_By"].ToString();
                DateTime? updateDate = (tableColumnReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableColumnReader["Update_Date"]);

                string checkBy = tableColumnReader["Check_By"].ToString();
                DateTime? checkDate = (tableColumnReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableColumnReader["Check_Date"]);

                string approveBy = tableColumnReader["Approve_By"].ToString();
                DateTime? approveDate = (tableColumnReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableColumnReader["Approve_Date"]);

                string deleteBy = tableColumnReader["Delete_By"].ToString();
                DateTime? deleteDate = (tableColumnReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableColumnReader["Delete_Date"]);

                string cancelBy = tableColumnReader["Cancel_By"].ToString();
                DateTime? cancelDate = (tableColumnReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableColumnReader["Cancel_Date"]);

                bool isApproved = bool.Parse(tableColumnReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(tableColumnReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(tableColumnReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(tableColumnReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(tableColumnReader["Work_Follow_Position"]);

                Utl_Dynamic_Table_Column tableColumnVM = new Utl_Dynamic_Table_Column(columnId, tableId, moduleId, lovModuleId, columnName, selectColName, colDisplayName, colProperty, controlType, parentcolumn, foreignKeyName,
                    foreignColName, foreignDataFilterBy, columnWidth, dbColumnWidth, transPolicyId, globalParamId, globalParamValue, defaultValue, filterBy, userPrivilege, isModuleSelect, isFvSelect,
                    isGvSelect, isForeignTable, isDefaultFocus, isFvUpdate, isFvInsert, isFvDataFixed, isFvForceEdit, dblDataSource, lovTableId, tableName, columnValue, columnText, columnTextExp,
                    WhereExpression, orderByExpression, sqlExpression, linkType, linkPageId, linkTableId, linkColumnName, linkUrl, isShow, reportColumn, slNo, DeveloperSynId, rowId, entryUserId,
                    entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel,
                    workFollowPosition);

                TableColumnList.Add(tableColumnVM);
            }
            tableColumnReader.Close();



            ////////////4. For AutoCode Property.
            string autocodePropertyQuery = "SELECT [PropertyId],[ColumnId],[FormatType],[Format],[Expression],[ColumnName],[SlNo],[IsActive] ,[DeveloperSyncId],[Entry_User_Id],[Entry_By]," +
                "[Entry_Date],[Update_By],[Update_Date] ,[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active],[Is_Deleted],[Work_Follow_Position]," +
                "[Row_Id] ,[Cancel_By] ,[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Autocode_Property]";

            SqlCommand autocodePropertyCmd = new SqlCommand(autocodePropertyQuery, conn);
            SqlDataReader autocodePropertyReader = autocodePropertyCmd.ExecuteReader();

            var autocodePropertyList = new List<Utl_Dynamic_Autocode_Property>();

            while (autocodePropertyReader.Read())
            {

                long propertyId = long.Parse(autocodePropertyReader["PropertyId"].ToString());
                long moduleId = long.Parse(autocodePropertyReader["ModuleId"].ToString());
                long tableId = long.Parse(autocodePropertyReader["TableId"].ToString());
                long columnId = long.Parse(autocodePropertyReader["ColumnId"].ToString());

                string formatType = autocodePropertyReader["FormatType"].ToString();
                string format = autocodePropertyReader["Format"].ToString();
                string expression = autocodePropertyReader["Expression"].ToString();
                string columnName = autocodePropertyReader["ColumnName"].ToString();

                int slNo = Convert.ToInt32(autocodePropertyReader["SlNo"].ToString());
                bool isActive = bool.Parse(autocodePropertyReader["IsActive"].ToString());
                string developerSynId = autocodePropertyReader["DeveloperSyncId"].ToString();
                string rowId = autocodePropertyReader["Row_Id"].ToString();

                long entryUserId = (autocodePropertyReader["Entry_User_Id"]) != DBNull.Value ? Convert.ToInt64(autocodePropertyReader["Entry_User_Id"]) : 0;
                string entryBy = autocodePropertyReader["Entry_By"].ToString();

                DateTime? entryDate = (autocodePropertyReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(autocodePropertyReader["Entry_Date"]);

                string updateBy = autocodePropertyReader["Update_By"].ToString();
                DateTime? updateDate = (autocodePropertyReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(autocodePropertyReader["Update_Date"]);

                string checkBy = autocodePropertyReader["Check_By"].ToString();
                DateTime? checkDate = (autocodePropertyReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(autocodePropertyReader["Check_Date"]);

                string approveBy = autocodePropertyReader["Approve_By"].ToString();
                DateTime? approveDate = (autocodePropertyReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(autocodePropertyReader["Approve_Date"]);

                string deleteBy = autocodePropertyReader["Delete_By"].ToString();
                DateTime? deleteDate = (autocodePropertyReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(autocodePropertyReader["Delete_Date"]);

                string cancelBy = autocodePropertyReader["Cancel_By"].ToString();
                DateTime? cancelDate = (autocodePropertyReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(autocodePropertyReader["Cancel_Date"]);


                bool isApproved = bool.Parse(autocodePropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(autocodePropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(autocodePropertyReader["Is_Deleted"].ToString());
                bool isCancel = bool.Parse(autocodePropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(autocodePropertyReader["Work_Follow_Position"]);




                Utl_Dynamic_Autocode_Property autocodePropertyVM = new Utl_Dynamic_Autocode_Property(propertyId, moduleId, tableId, columnId, formatType, format, expression, columnName, slNo, isActive,
                    developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy,
                    cancelDate, isApproved, is_Active, isDelete, isCancel, workFollowPosition);

                autocodePropertyList.Add(autocodePropertyVM);
            }
            autocodePropertyReader.Close();


            ////////////5. For DropDown Property.
            string ddPropertyQuery = "SELECT [PropertyId],[ColumnId],ModuleId,TableId,[PropertyType],[TableName],[ColumnValue],[ColumnText],[ColumnTextExp],[WhereExpression],[OrderByExpression]," +
                "[SqlExpression],[IsActive] ,[DeveloperSyncId],[Entry_User_Id],[Entry_By],[Entry_Date],[Update_By] ,[Update_Date] ,[Check_By],[Check_Date] ,[Approve_By],[Approve_Date]," +
                "[Delete_By],[Delete_Date],[Is_Approved] ,[Is_Active],[Is_Deleted],[Work_Follow_Position] ,[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Dropdown_Property]";

            SqlCommand ddPropertyCmd = new SqlCommand(ddPropertyQuery, conn);
            SqlDataReader ddPropertyReader = ddPropertyCmd.ExecuteReader();

            var ddPropertyList = new List<Utl_Dynamic_Dropdown_Property>();

            while (ddPropertyReader.Read())
            {

                long propertyId = long.Parse(ddPropertyReader["PropertyId"].ToString());
                long columnId = long.Parse(ddPropertyReader["ColumnId"].ToString());
                long moduleId = long.Parse(autocodePropertyReader["ModuleId"].ToString());
                long tableId = long.Parse(autocodePropertyReader["TableId"].ToString());

                int propertyType = Convert.ToInt32(ddPropertyReader["PropertyType"].ToString());

                string tableName = ddPropertyReader["TableName"].ToString();
                string columnValue = ddPropertyReader["ColumnValue"].ToString();
                string columnText = ddPropertyReader["ColumnText"].ToString();
                string columnTextExp = ddPropertyReader["ColumnTextExp"].ToString();
                string whereExpression = ddPropertyReader["WhereExpression"].ToString();
                string orderByExpression = ddPropertyReader["OrderByExpression"].ToString();
                string sqlExpression = ddPropertyReader["SqlExpression"].ToString();


                bool isActive = bool.Parse(ddPropertyReader["IsActive"].ToString());
                string developerSynId = ddPropertyReader["DeveloperSyncId"].ToString();
                string rowId = ddPropertyReader["Row_Id"].ToString();


                long entryUserId = (ddPropertyReader["Entry_User_Id"]) != DBNull.Value ? Convert.ToInt64(ddPropertyReader["Entry_User_Id"]) : 0;
                string entryBy = ddPropertyReader["Entry_By"].ToString();

                DateTime? entryDate = (ddPropertyReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(ddPropertyReader["Entry_Date"]);

                string updateBy = ddPropertyReader["Update_By"].ToString();
                DateTime? updateDate = (ddPropertyReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(ddPropertyReader["Update_Date"]);

                string checkBy = ddPropertyReader["Check_By"].ToString();
                DateTime? checkDate = (ddPropertyReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(ddPropertyReader["Check_Date"]);

                string approveBy = ddPropertyReader["Approve_By"].ToString();
                DateTime? approveDate = (ddPropertyReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(ddPropertyReader["Approve_Date"]);

                string deleteBy = ddPropertyReader["Delete_By"].ToString();
                DateTime? deleteDate = (ddPropertyReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(ddPropertyReader["Delete_Date"]);

                string cancelBy = ddPropertyReader["Cancel_By"].ToString();
                DateTime? cancelDate = (ddPropertyReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(ddPropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(ddPropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(ddPropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(ddPropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(ddPropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(ddPropertyReader["Work_Follow_Position"]);


                Utl_Dynamic_Dropdown_Property ddPropertyVM = new Utl_Dynamic_Dropdown_Property(propertyId, columnId, moduleId, tableId, propertyType, tableName, columnValue, columnText, columnTextExp,
                    whereExpression, orderByExpression, sqlExpression, isActive, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate,
                    approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                ddPropertyList.Add(ddPropertyVM);
            }
            ddPropertyReader.Close();



            ////////////6. For Filter Property.
            string filterPropertyQuery = "SELECT [PropertyId],[TableId],ModuleId,[ColumnId],[OperatorId],[AndExpression],[OrExpression],[IsActive],[DeveloperSyncId],[Entry_User_Id],[Entry_By]," +
                "[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active],[Is_Deleted]," +
                "[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Filter_Property]";

            SqlCommand filterPropertyCmd = new SqlCommand(filterPropertyQuery, conn);
            SqlDataReader filterPropertyReader = filterPropertyCmd.ExecuteReader();

            var filterPropertyList = new List<Utl_Dynamic_Filter_Property>();

            while (filterPropertyReader.Read())
            {
                long propertyId = long.Parse(filterPropertyReader["PropertyId"].ToString());
                long tableId = long.Parse(filterPropertyReader["TableId"].ToString());
                long moduleId = long.Parse(filterPropertyReader["ModuleId"].ToString());
                long columnId = long.Parse(filterPropertyReader["ColumnId"].ToString());

                int operatorId = Convert.ToInt32(filterPropertyReader["OperatorId"].ToString());

                string andExpression = filterPropertyReader["AndExpression"].ToString();
                string OrExpression = filterPropertyReader["OrExpression"].ToString();

                bool isActive = bool.Parse(filterPropertyReader["IsActive"].ToString());
                string developerSynId = filterPropertyReader["DeveloperSyncId"].ToString();
                string rowId = filterPropertyReader["Row_Id"].ToString();

                long entryUserId = (filterPropertyReader["Entry_User_Id"]) != DBNull.Value ? Convert.ToInt64(filterPropertyReader["Entry_User_Id"]) : 0;
                string entryBy = filterPropertyReader["Entry_By"].ToString();

                DateTime? entryDate = (filterPropertyReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(filterPropertyReader["Entry_Date"]);

                string updateBy = filterPropertyReader["Update_By"].ToString();
                DateTime? updateDate = (filterPropertyReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(filterPropertyReader["Update_Date"]);

                string checkBy = filterPropertyReader["Check_By"].ToString();
                DateTime? checkDate = (filterPropertyReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(filterPropertyReader["Check_Date"]);

                string approveBy = filterPropertyReader["Approve_By"].ToString();
                DateTime? approveDate = (filterPropertyReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(filterPropertyReader["Approve_Date"]);

                string deleteBy = filterPropertyReader["Delete_By"].ToString();
                DateTime? deleteDate = (filterPropertyReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(filterPropertyReader["Delete_Date"]);

                string cancelBy = filterPropertyReader["Cancel_By"].ToString();
                DateTime? cancelDate = (filterPropertyReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(filterPropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(filterPropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(filterPropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(filterPropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(filterPropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(filterPropertyReader["Work_Follow_Position"]);


                Utl_Dynamic_Filter_Property filterPropertyVM = new Utl_Dynamic_Filter_Property(propertyId, moduleId, tableId, columnId, operatorId, andExpression, OrExpression, isActive, developerSynId,
                    rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate,
                    isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                filterPropertyList.Add(filterPropertyVM);
            }
            filterPropertyReader.Close();


            ////////////7. For Validation Property.
            string validationPropertyQuery = "SELECT [PropertyId],[ColumnId],[TableId],ModuleId,[OperatorId],[ValueExpression],[ValueDataType],[ErrorMessage],[IsActive],[DeveloperSyncId],[Entry_User_Id]," +
                "[Entry_By],[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved] ,[Is_Active] ,[Is_Deleted]," +
                "[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Validation_Property]";

            SqlCommand validationPropertyCmd = new SqlCommand(validationPropertyQuery, conn);
            SqlDataReader validationPropertyReader = validationPropertyCmd.ExecuteReader();

            var validationPropertyList = new List<Utl_Dynamic_Validation_Property>();

            while (validationPropertyReader.Read())
            {
                long propertyId = long.Parse(validationPropertyReader["PropertyId"].ToString());
                long columnId = long.Parse(validationPropertyReader["ColumnId"].ToString());
                long tableId = long.Parse(validationPropertyReader["TableId"].ToString());
                long moduleId = long.Parse(validationPropertyReader["ModuleId"].ToString());
                int operatorId = Convert.ToInt32(validationPropertyReader["OperatorId"].ToString());

                string valueExpression = validationPropertyReader["ValueExpression"].ToString();
                string valueDataType = validationPropertyReader["ValueDataType"].ToString();
                string errorMessasge = validationPropertyReader["ErrorMessage"].ToString();

                bool isActive = bool.Parse(validationPropertyReader["IsActive"].ToString());
                string developerSynId = validationPropertyReader["DeveloperSyncId"].ToString();
                string rowId = validationPropertyReader["Row_Id"].ToString();

                long entryUserId = (validationPropertyReader["Entry_User_Id"]) != DBNull.Value ? Convert.ToInt64(validationPropertyReader["Entry_User_Id"]) : 0;
                string entryBy = validationPropertyReader["Entry_By"].ToString();
                DateTime? entryDate = (validationPropertyReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(validationPropertyReader["Entry_Date"]);

                string updateBy = validationPropertyReader["Update_By"].ToString();
                DateTime? updateDate = (validationPropertyReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(validationPropertyReader["Update_Date"]);

                string checkBy = validationPropertyReader["Check_By"].ToString();
                DateTime? checkDate = (validationPropertyReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(validationPropertyReader["Check_Date"]);

                string approveBy = validationPropertyReader["Approve_By"].ToString();
                DateTime? approveDate = (validationPropertyReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(validationPropertyReader["Approve_Date"]);

                string deleteBy = validationPropertyReader["Delete_By"].ToString();
                DateTime? deleteDate = (validationPropertyReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(validationPropertyReader["Delete_Date"]);

                string cancelBy = validationPropertyReader["Cancel_By"].ToString();
                DateTime? cancelDate = (validationPropertyReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(validationPropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(validationPropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(validationPropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(validationPropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(validationPropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(validationPropertyReader["Work_Follow_Position"]);



                Utl_Dynamic_Validation_Property validationPropertyVM = new Utl_Dynamic_Validation_Property(propertyId, columnId, tableId, moduleId, operatorId, valueExpression, valueDataType,
                    errorMessasge, isActive, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                    deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                validationPropertyList.Add(validationPropertyVM);
            }
            validationPropertyReader.Close();




            ////////////8. For Table Function.
            string tableFunQuery = "SELECT [TableFunctionId],[TableId],ModuleId,[ColumnName],[AggregateFunction],[EvaluateValue],[EffectColumn],[IsDataShort],[IsDescending],[SLNo],[DeveloperSyncId]," +
                "[Entry_User_Id],[Entry_By],[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active]," +
                "[Is_Deleted],[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Table_Function]";

            SqlCommand tableFunCmd = new SqlCommand(tableFunQuery, conn);
            SqlDataReader tableFunReader = tableFunCmd.ExecuteReader();

            var tableFunList = new List<Utl_Dynamic_Table_Function>();

            while (tableFunReader.Read())
            {
                long tableFunctionId = long.Parse(tableFunReader["TableFunctionId"].ToString());
                long tableId = long.Parse(tableFunReader["TableId"].ToString());
                long moduleId = long.Parse(tableFunReader["ModuleId"].ToString());

                string columnName = tableFunReader["ColumnName"].ToString();
                string aggregateFunction = tableFunReader["AggregateFunction"].ToString();
                string evaluateValue = tableFunReader["EvaluateValue"].ToString();
                string effectColumn = tableFunReader["EffectColumn"].ToString();

                bool isDataShort = bool.Parse(tableFunReader["IsDataShort"].ToString());
                bool isDescending = bool.Parse(tableFunReader["IsDescending"].ToString());
                decimal slNo = decimal.Parse(tableFunReader["SLNo"].ToString());


                string developerSynId = tableFunReader["DeveloperSyncId"].ToString();
                string rowId = tableFunReader["Row_Id"].ToString();


                long entryUserId = (tableFunReader["Entry_User_Id"]) != DBNull.Value ? Convert.ToInt64(tableFunReader["Entry_User_Id"]) : 0;
                string entryBy = tableFunReader["Entry_By"].ToString();
                DateTime? entryDate = (tableFunReader["Entry_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableFunReader["Entry_Date"]);

                string updateBy = tableFunReader["Update_By"].ToString();
                DateTime? updateDate = (tableFunReader["Update_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableFunReader["Update_Date"]);

                string checkBy = tableFunReader["Check_By"].ToString();
                DateTime? checkDate = (tableFunReader["Check_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableFunReader["Check_Date"]);

                string approveBy = tableFunReader["Approve_By"].ToString();
                DateTime? approveDate = (tableFunReader["Approve_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableFunReader["Approve_Date"]);

                string deleteBy = tableFunReader["Delete_By"].ToString();
                DateTime? deleteDate = (tableFunReader["Delete_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableFunReader["Delete_Date"]);

                string cancelBy = tableFunReader["Cancel_By"].ToString();
                DateTime? cancelDate = (tableFunReader["Cancel_Date"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(tableFunReader["Cancel_Date"]);

                bool isApproved = bool.Parse(tableFunReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(tableFunReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(tableFunReader["Is_Deleted"].ToString());
                bool isCancel = bool.Parse(tableFunReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(tableFunReader["Work_Follow_Position"]);


                Utl_Dynamic_Table_Function tableFunVM = new Utl_Dynamic_Table_Function(tableFunctionId, tableId, moduleId, columnName, aggregateFunction, evaluateValue, effectColumn, isDataShort,
                    isDescending, slNo, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                    deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCancel, workFollowPosition);

                tableFunList.Add(tableFunVM);
            }
            tableFunReader.Close();
            conn.Close();


            JsonTableList jsonTableModel = new JsonTableList();
            List<JsonTableList> jsonTableLists = new List<JsonTableList>();

            jsonTableModel.Utl_Dynamic_Module = ModuleList;
            jsonTableModel.Utl_Dynamic_Table = TableList;
            jsonTableModel.Utl_Dynamic_Table_Column = TableColumnList;
            jsonTableModel.Utl_Dynamic_Autocode_Property = autocodePropertyList;
            jsonTableModel.Utl_Dynamic_Dropdown_Property = ddPropertyList;
            jsonTableModel.Utl_Dynamic_Filter_Property = filterPropertyList;
            jsonTableModel.Utl_Dynamic_Validation_Property = validationPropertyList;
            jsonTableModel.Utl_Dynamic_Table_Function = tableFunList;

            jsonTableLists.Add(jsonTableModel);


            string jsonDatapath = "~/JsonDataStore/";
            JsonDataConvater(jsonDatapath, jsonTableLists);


            return View("../JsonDataGenerate/FirstPage");
        }

        public void JsonDataConvater<T>(string path, List<T> modelList)
        {
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue, RecursionLimit = 100 };

            string AllTableList = serializer.Serialize(modelList);
            //string AllTableList = new JavaScriptSerializer().Serialize(modelList);
            string fulpath = Server.MapPath(path);
            //string path = Server.MapPath("~/App_Data/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText(fulpath + "AllTableJsonData.json", AllTableList);
        }
    }
}