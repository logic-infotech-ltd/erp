﻿using EnterpriceResourcePlaning_ERP_.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace EnterpriceResourcePlaning_ERP_.Controllers
{
    public class SearchingTablesController : Controller
    {
        // GET: SearchingTables
        string connS = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        // GET: ApprovalTable
        public ActionResult SearchingTable()
        {
            SqlConnection con = new SqlConnection(connS);
            string query = "SELECT E.Emp_Id, E.EmpName, E.Age, D.Dep_Name, D.Dep_Code, E.Dep_Id  FROM Department AS D INNER JOIN Employee AS E ON D.Dep_Id = E.Dep_Id";
            SqlCommand cmd = new SqlCommand(query, con);

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            var EmployeeViewM = new List<EmployeeDepModel>();
            while (reader.Read())
            {
                int empId = Convert.ToInt32(reader["Emp_Id"]);
                string empName = reader["EmpName"].ToString();
                int age = Convert.ToInt32(reader["Age"]);
                string depName = reader["Dep_Name"].ToString();
                string depCode = reader["Dep_Code"].ToString();
                int depId = Convert.ToInt32(reader["Dep_Id"]);

                EmployeeDepModel edm = new EmployeeDepModel(empId, empName, age, depName, depCode, depId);
                EmployeeViewM.Add(edm);
            }
            reader.Close();
            con.Close();

            return View(EmployeeViewM);
        }
    }
}