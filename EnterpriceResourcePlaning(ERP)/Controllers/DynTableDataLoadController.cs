﻿using EnterpriceResourcePlaning_ERP_.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace EnterpriceResourcePlaning_ERP_.Controllers
{
    public class DynTableDataLoadController : Controller
    {
        string connectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection1"].ConnectionString;
        public ActionResult DynTableDataRead()
        {

            SqlConnection connection = new SqlConnection(connectionString);
            string query1 = "SELECT * FROM FormInfo WHERE FormId = '" + 2 + "'";
            SqlCommand command1 = new SqlCommand(query1, connection);

            connection.Open();

            SqlDataReader reader1 = command1.ExecuteReader();
            var formList = new List<string>();

            while (reader1.Read())
            {

                string FormElement = reader1["FormElement"].ToString();
                formList.Add(FormElement);

            }
            reader1.Close();


            ViewBag.tds = formList;



            string query = "SELECT * FROM Teacher";
            SqlCommand command = new SqlCommand(query, connection);

            SqlDataReader reader = command.ExecuteReader();

            var TableRow = new List<string>();
            var field = reader.FieldCount;

            while (reader.Read())
            {
                string rowValue = "";
                for (int i =0; i < field; i++)
                {
                    if(rowValue == ""){
                        rowValue = reader[i].ToString();
                    }
                    else
                    {
                        rowValue += "^@^"+ reader[i].ToString();
                    }
                }
                TableRow.Add(rowValue);
              
            }
            reader.Close();
            connection.Close();
          
            ViewBag.TableData = TableRow;

            return View();
        }
        public ActionResult Create()
        {
            return View();

        }
    }
}