﻿using EnterpriceResourcePlaning_ERP_.BaseRepository;
using EnterpriceResourcePlaning_ERP_.Models;
using EnterpriceResourcePlaning_ERP_.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnterpriceResourcePlaning_ERP_.Controllers
{
    public class GetDynamicTableInfoController : Controller
    {
        // GET: GetDynamicTableInfo
        public JsonResult JsonDataRead(/*FormCollection formCollection*/ int moduleId, int tableId, string pkId, string fkId, string dmlType)
        {
            
            string startTime = DateTime.Now.Second.ToString() +":"+ DateTime.Now.Millisecond.ToString();

            //int ModuleId = (formCollection["ModuleId"]) == ""? 0: Convert.ToInt32(formCollection["ModuleId"]);
            //int TableId = (formCollection["TableId"]) == "" ? 0 : Convert.ToInt32(formCollection["TableId"]);
            //string PkId = formCollection["PkId"];
            //string FkId = formCollection["FkId"];
            //string DmlType = formCollection["DmlType"];

            //string pathWithAll_module = Server.MapPath("~/JsonDataStore/AllTableJsonData.json");
            //string pathWithOne_module = Server.MapPath("~/JsonDataStore/Page_"+ModuleId+".json");


            Repo_GetDynamicTable rgdt = new Repo_GetDynamicTable();
            string dmlQueryData = rgdt.Get_Dynamic_Table_Info(moduleId, tableId, pkId, fkId, dmlType/*, pathWithAll_module*/);


            //ViewBag.QueryData = dmlQueryData;

            string EndTime = DateTime.Now.Second.ToString() + ":" + DateTime.Now.Millisecond.ToString();

            List<string> dataList = new List<string>();
            dataList.Add(startTime);
            dataList.Add(dmlQueryData);
            dataList.Add(EndTime);

            return Json(dataList);
        }

        //JsonDataWriteAndRead JsonWriteAndRead = new JsonDataWriteAndRead();
        public JsonResult Get_Dynamic_Grid_View(long ModuleId, long TableId, string FkId, string FilterExpression, string WorkfollowAction, int IsDeleteShow, 
            int IsWorkFollow, string DisplayOn, string UserId, int RecordType)
        {
            string startTime = DateTime.Now.Second.ToString() + ":" + DateTime.Now.Millisecond.ToString();

            JsonDataWriteAndRead jsonWriteRead = new JsonDataWriteAndRead();
            var jsonTableLists = jsonWriteRead.jsonFileRead(ModuleId);

            var DynamicTableColumn = jsonTableLists.Utl_Dynamic_Table_Column;
            List<Utl_Dynamic_Table> Dynamic_Table = jsonTableLists.Utl_Dynamic_Table;
            List<Utl_Dynamic_Table_Function> DynamicTblFunc = jsonTableLists.Utl_Dynamic_Table_Function;
            var TblFilterProperty = jsonTableLists.Utl_Dynamic_Filter_Property;
            //var TblOperator = jsonTableLists.

            Base_GetDynamicGridView DynamicGridView = new Base_GetDynamicGridView();
            List<ColumnNameForGridView> DisplayOnModel = new List<ColumnNameForGridView>();

            FkId = "'" + FkId + "'";
            int? IsWorkFollowModule = 0;

            long parmTableId = 0;
            parmTableId = TableId;

          
            if (DisplayOn == "module")
            {
                IsWorkFollowModule = Dynamic_Table.Where(s => s.TableId == TableId && s.IsWorkFollow == true).Count();
            }

            long? FkTableId = 0;
            string Sql = "";

            string RptProcedureName = Dynamic_Table.Where(t => t.TableId == TableId && t.RptProcForWorksheet == true).Select(r => r.ReportProcedureName).FirstOrDefault();

            RptProcedureName = RptProcedureName == null ? "" : RptProcedureName;

            if (RptProcedureName.Length > 5)
            {
                string PkId = FkId;
                RptProcedureName = RptProcedureName + ' ' + PkId;
                int FkTableid = 0;
                var DynTable = Dynamic_Table.Where(t => t.TableId == TableId).Select(t => new { t.TableCaption, t.ReportFileName, t.ProcedureCaption, t.IsExternalDataShow, t.ExternalDataCaption }).FirstOrDefault();

                Sql = DynTable.TableCaption + FkTableid + DynTable.ReportFileName + DynTable.ProcedureCaption + DynTable.IsExternalDataShow + DynTable.ExternalDataCaption;
            }
            else
            {
                
                string GroupSql = "";
                string GroupClause = "";
                string TableName = "";
                string ColumnName = "";
                string ColumnAsName = "";
                string FK_NAME = "";
                string FK_EXPRESSION = "";
                string ForeignDataExpression = "";
                long? FK_TABLE_ID = null;
                string FK_TABLE_NAME = "";
                string FK_TABLE_PK_COLUMN = "";
                string FK_TABLE_FK_COLUMN = "";
                string FK_VALUE = "";
                string QuickSearchColumn = "";
                string QuickSearch = "";
                string ExternalDataFilter = "";


                if (FilterExpression.Length >1)
                {
                    if (FilterExpression.Substring(0,2) == "@@")
                    {
                        QuickSearch = FilterExpression.Substring(3, FilterExpression.Length - 2);
                        FilterExpression = "";
                    }
                }
                List<string> tmpActionUser = new List<string>();
                if (DisplayOn == "module")
                {
                    
                    if (RecordType == 2 || RecordType == 3)
                    {
                       
                        tmpActionUser.Add("entry_by");
                        tmpActionUser.Add("entry_date");
                        tmpActionUser.Add("update_by");
                        tmpActionUser.Add("update_date");

                        if (IsWorkFollowModule == 1)
                        {
                          
                            tmpActionUser.Add("check_by");
                            tmpActionUser.Add("check_date");
                            tmpActionUser.Add("approve_by");
                            tmpActionUser.Add("approve_date");
                        }

                        if (RecordType == 2)
                        {
                            tmpActionUser.Add("delete_by");
                            tmpActionUser.Add("delete_date");
                        }
                    }
                }

                if (FkId != "" && FkId != "0")
                {
                    long? PTableId = Dynamic_Table.Where(t => t.TableId == TableId).Select(t => t.ParentTableId).FirstOrDefault(); //Dynamic_Table.Where(s => s.TableId == )
                    //    .Select(s => new { s.TableId, s.TableName });
                    var tblIdName = Dynamic_Table.Where(s => s.TableId == PTableId).Select(s => new { s.ModuleId, s.TableId, s.TableName }).FirstOrDefault();

                    FK_TABLE_ID = tblIdName.TableId; //.Select(s => s.TableId).FirstOrDefault();
                    FK_TABLE_NAME = tblIdName.TableName; //.Select(s => s.TableName).FirstOrDefault();

                    var fkTablePkFkColurm = DynamicTableColumn.Where(s => s.TableId == FK_TABLE_ID && (s.ColumnProperty == "pk" || s.ColumnProperty == "pkt"))
                        .Select(c => new { c.ColumnName, c.ColumnProperty });


                    FK_TABLE_PK_COLUMN = fkTablePkFkColurm.Where(s => s.ColumnProperty == "pk").Select(s => s.ColumnName).FirstOrDefault();
                    FK_TABLE_FK_COLUMN = fkTablePkFkColurm.Where(s => s.ColumnProperty == "pkt").Select(s => s.ColumnName).FirstOrDefault();

                    if (FK_TABLE_NAME != "" && FK_TABLE_PK_COLUMN != "" && FK_TABLE_FK_COLUMN != "")
                    {
                        FK_VALUE = "(SELECT " + FK_TABLE_FK_COLUMN + " FROM " + FK_TABLE_NAME + " WHERE " + FK_TABLE_PK_COLUMN + "=" + FkId + ")";
                    }
                    FK_VALUE = "SELECT '" + FK_VALUE + "'";

                    if (FilterExpression == "DataKey")
                    {
                        string PK_COLUMN = DynamicTableColumn.Where(t => t.TableId == TableId && t.ColumnProperty == "pk").Select(t => t.ColumnName).FirstOrDefault();

                        FilterExpression = PK_COLUMN != "" ? PK_COLUMN + "=" + FkId : FilterExpression;
                    }
                }

                long? ParentTableId;
                string FilterTmpExp = "";
                string ForeignColumnName = "";
                string FilterColumnName = "";
                string FilterExp = "";
                string ParentTable = ""; 
                string PkColumn = "";

                if (DisplayOn == "formParrent")
                {
                        
                    TableId = (long)Dynamic_Table.Where(s => s.TableId == TableId).Select(s => s.ParentTableId).FirstOrDefault();
                    FK_NAME = DynamicTableColumn.Where(t => t.TableId == TableId && t.ColumnProperty == "pk").Select(t => t.ColumnName).FirstOrDefault();

                    if (FK_NAME != "0" && FK_NAME != "" && FkId != "" && FkId != "0")
                    {
                            
                        FK_EXPRESSION = " t." + FK_NAME + "=" + FkId;
                            
                    }
                       
                    TableName = Dynamic_Table.Where(w => w.TableId == TableId).Select(s => s.TableName).FirstOrDefault();

                    //FROM UTL_DYNAMIC_TABLE_COLUMN where TableId = @TableId and(IsGvSelect = 1)and IsShow = 1 and ColumnProperty<>'pk'
                    //order by case when isnull(ReportColumn,0)= 0 then SlNo+200 else ReportColumn end
 
                    var dynTableColumn = DynamicTableColumn.Where(s => s.TableId == TableId && (s.IsGvSelect == true) && s.IsShow == true && s.ColumnProperty != "pk")
                        .OrderBy(s => new { V = (s.ReportColumn == null ? 0 : s.ReportColumn) == 0? Convert.ToInt32(s.SlNo):s.ReportColumn });

                        DisplayOnModel = DynamicGridView.DisplayOnModel(dynTableColumn, FK_VALUE);

                }
                else if (DisplayOn == "formDetail")
                {
                        
                    FK_NAME = DynamicTableColumn.Where(t => t.TableId == TableId && t.ColumnProperty == "pk").Select(t => t.ColumnName).FirstOrDefault();
                    if (FK_NAME != "0" && FK_NAME != "")
                    {
                        FK_EXPRESSION = " t." + FK_NAME + "=" + FkId;
                    }
                    TableName = Dynamic_Table.Where(w => w.TableId == TableId).Select(s => s.TableName).FirstOrDefault();

                    IEnumerable<Utl_Dynamic_Table_Column> dynamicTableColumn = DynamicTableColumn.Where(s => s.TableId == TableId && ((s.IsGvSelect == true && s.IsShow == true) || s.ColumnProperty == "pk")).OrderBy(s => s.SlNo);

                        DisplayOnModel = DynamicGridView.DisplayOnModel(dynamicTableColumn, FK_VALUE);

                }
                else if (DisplayOn == "WorkFollowParrent")
                {
                    FK_NAME = DynamicTableColumn.Where(s => s.TableId == TableId && s.ColumnProperty == "pk").Select(t => t.ColumnName).FirstOrDefault();
                    FK_NAME = FK_NAME == null ? "" : FK_NAME;

                    if (FK_NAME != "0" && FK_NAME !="" && FkId !="0" && FkId !="")
                    {
                        FK_EXPRESSION = " t." + FK_NAME + "=" + FkId;
                    }
                    TableName = Dynamic_Table.Where(w => w.TableId == TableId).Select(s => s.TableName).FirstOrDefault();

                    IEnumerable<Utl_Dynamic_Table_Column> dynamicTableColumn = DynamicTableColumn.Where(s => s.TableId == TableId && ((s.IsModuleSelect == true && s.IsShow == true) || s.ColumnProperty == "pk"))
                        .OrderBy(s => new { V = (s.ReportColumn == null ? 0 : s.ReportColumn) == 0 ? Convert.ToInt32(s.SlNo) : s.ReportColumn });

                        DisplayOnModel = DynamicGridView.DisplayOnModel(dynamicTableColumn, FK_VALUE);
                }
                else if (DisplayOn == "formForeign")
                {
                    var utlTable = Dynamic_Table.Where(t => t.TableId == TableId).FirstOrDefault();
                    FkTableId = utlTable.ForeignTableId;
                    ExternalDataFilter = utlTable.ExternalDataFilter == null?"":utlTable.ExternalDataFilter;
                    TableName = Dynamic_Table.Where(s => s.TableId == FkTableId).Select(s=>s.TableName).FirstOrDefault();

                    var foreignColumnName = DynamicTableColumn.Where(f => f.TableId == TableId && f.IsForeignTable == true && (f.ForeignColumnName == null ? "" : f.ForeignColumnName) != ""
                    && (f.ForeignColumnName == null ? "" : f.ForeignColumnName) != "0").Select(a=>a.ForeignColumnName);

                    IEnumerable<Utl_Dynamic_Table_Column> dynamicTableColumn = DynamicTableColumn.Where(w => w.TableId == FkTableId && w.IsShow == true && (w.ColumnProperty == "pk" ||
                    w.IsForeignTable == true || foreignColumnName.Contains(w.ColumnName))).OrderBy(s=>s.SlNo);

                        DisplayOnModel = DynamicGridView.DisplayOnModel(dynamicTableColumn, FK_VALUE);

                        
                    if (FkId != "" && FkId !="0")
                    {
                        //string ForeignColumnName = "", FilterColumnName = "", FilterExp = "", ParentTable = "", PkColumn = "";
                        

                        ParentTableId = Dynamic_Table.Where(s => s.TableId == TableId).Select(s => s.ParentTableId).FirstOrDefault();
                        ParentTable = Dynamic_Table.Where(s => s.TableId == ParentTableId).Select(s=> s.TableName).FirstOrDefault();
                        PkColumn = DynamicTableColumn.Where(i => i.TableId == ParentTableId && i.ColumnProperty == "pk").Select(s => s.ColumnName).FirstOrDefault();

                        var colNameDataFilter = DynamicTableColumn.Where(i => i.TableId == TableId && (i.ForeignColumnName == null ? "" : i.ForeignColumnName) != "" && (i.ForeignColumnName == null ? "" : i.ForeignColumnName) != "0" &&
                            (i.ForeignDataFilterBy == null ? "" : i.ForeignDataFilterBy) != "" && (i.ForeignDataFilterBy == null ? "" : i.ForeignDataFilterBy) != "0").Select(s=> new { s.ForeignColumnName, s.ForeignDataFilterBy });
                            
                        foreach (var item in colNameDataFilter)
                        {
                            if (FilterExp != "")
                            {
                                FilterExp = FilterExp + " AND ";
                                FilterTmpExp = FilterTmpExp + " AND ";
                            }
                            ForeignColumnName = item.ForeignColumnName;
                            FilterColumnName = item.ForeignDataFilterBy;

                            FilterTmpExp = FilterTmpExp + "'" + "ft." + ForeignColumnName + "='''+cast(ft." + FilterColumnName + " as varchar)+''''";
                            FilterExp = FilterExp + "'" + ForeignColumnName + "=''' + cast(" + FilterColumnName + " as varchar)+''''";
                        }

                        if (FilterExp != "" && ParentTable != "" && PkColumn != "")
                        {
                            //select CAST(null as varchar(8000)) AS Filter into #tmpFilterExp
                            //SET @FilterExp = 'UPDATE #tmpFilterExp SET Filter=(SELECT ' + @FilterExp + ' FROM ' + @ParentTable + ' WHERE ' + @PkColumn + '=' + @FkId + ')'
                            List<string> Filter = new List<string>();
                            FilterExp = "(SELECT " + FilterExp + " FROM " + ParentTable + " WHERE " + PkColumn + "=" + FkId + ")";
                            ForeignDataExpression = FilterExp;

                            //      SET @ForeignDataExpression = (SELECT Filter FROM #tmpFilterExp)
                            //--print @ForeignDataExpression
                            //      SET @FilterTmpExp = 'UPDATE #tmpFilterExp SET Filter=(SELECT ' + @FilterTmpExp + ' FROM ' + @ParentTable + ' ft WHERE ft.' + @PkColumn + '=' + @FkId + ')'

                            FilterTmpExp = "(SELECT " + FilterTmpExp + " FROM " + ParentTable + " ft WHERE ft." + PkColumn + "=" + FkId + ")";
                        }
                    }
                }
                else
                {
                    string Key = "fk"; 
                    long? Parent = 0;
                    Parent = Dynamic_Table.Where(t => t.TableId == TableId).Select(s => s.ParentTableId).FirstOrDefault();
                    Key = DisplayOn == "report" && FilterExpression != "" ? "pk" : Key;
                    
                    //SET @FK_NAME = isnull((SELECT top 1 ColumnName FROM UTL_DYNAMIC_TABLE_COLUMN where TableId = @TableId AND[ColumnProperty] = @Key),'')
                    //IF @FK_NAME not in ('0', '') and @FkId not in ('''','''0''')
                    //   SET @FK_EXPRESSION = ' t.' + @FK_NAME + '=' + @FkId

                    FK_NAME = DynamicTableColumn.Where(t => t.TableId == TableId && t.ColumnProperty == Key).Select(s => s.ColumnName).FirstOrDefault();
                    FK_NAME = FK_NAME == null ? "" : FK_NAME;
                    if (FK_NAME != "0" && FK_NAME!="" && FkId != "''" && FkId!="'0'")
                    {
                        FK_EXPRESSION = " t." + FK_NAME + "=" + FkId;
                    }

                    TableName = Dynamic_Table.Where(s => s.TableId == TableId).Select(s => s.TableName).FirstOrDefault();

                    if (DisplayOn == "" || DisplayOn == "form")
                    {
                        IEnumerable<Utl_Dynamic_Table_Column> dyTableColumn = DynamicTableColumn.Where(f => f.TableId == TableId && ((f.IsGvSelect == true && f.IsShow == true) || f.ColumnProperty == "pk")).OrderBy(s=>s.SlNo);

                            DisplayOnModel = DynamicGridView.DisplayOnModel(dyTableColumn, FK_VALUE);
                    }

                    if (DisplayOn == "module")
                    {
                        //insert into #tmpColumnname SELECT CASE WHEN @FK_VALUE<>'' AND ColumnProperty='fk' then @FK_VALUE when ControlType in ('dfl','dft','dfd') and 
                        //isnull(ForeignColumnName,'') not in ('','0') then 'ft.'+ForeignColumnName when ISNULL(SelectColumnName,'')='' then ColumnName  else SelectColumnName end AS ColumnName,
                        //case when ColumnProperty = 'pk' then 'DataKey'  else '[' + ColumnDisplayName + ']' end as ColumnAsName,SlNo,
                        //ControlType,ColumnId,case when len(parentColumn)> 3 then 't.' + substring(parentColumn, 4, len(parentColumn) - 3) else '' end as ParentControl,
                        //isnull(ForeignKeyName, 0) as ForeignKeyName ,FilterBy,ISNULL(DefaultValue, ''),ColumnName,GlobalParamId
                        //,isnull(TableName, ''),isnull(ColumnValue, ''),isnull(ColumnText, ''),isnull(ColumnTextExp, ''),isnull(WhereExpression, ''),isnull(OrderByExpression, ''),isnull(SqlExpression, '')
                        //FROM UTL_DYNAMIC_TABLE_COLUMN where TableId = @TableId  and(((IsModuleSelect = 1 or ColumnName in (Select ColumnName from #tmpActionUser))  and IsShow=1)  or ColumnProperty='pk') 
                        //order by case when isnull(ReportColumn, 0) = 0 then SlNo else ReportColumn end

                        IEnumerable<Utl_Dynamic_Table_Column> TableColumnModule = DynamicTableColumn.Where(i=>i.TableId == TableId && 
                        (((i.IsModuleSelect == true || tmpActionUser.Contains(i.ColumnName)) && i.IsShow == true) || i.ColumnProperty == "pk"))
                            .OrderBy(i=> (i.ReportColumn == null ? 0 : i.ReportColumn) == 0 ? Convert.ToInt32(i.SlNo) : i.ReportColumn );

                        DisplayOnModel = DynamicGridView.DisplayOnModel(TableColumnModule, FK_VALUE);
                    }

                    if (DisplayOn == "report")
                    {
                        //insert into #tmpColumnname SELECT CASE WHEN @FK_VALUE<>'' AND ColumnProperty='fk' then @FK_VALUE when ControlType in ('dfl','dft','dfd') and 
                        //isnull(ForeignColumnName,'') not in ('','0') then 'ft.'+ForeignColumnName when ISNULL(SelectColumnName,'')='' then ColumnName  else SelectColumnName end AS ColumnName,
                        //case when ColumnProperty = 'pk' then 'DataKey'  else '[' + ColumnDisplayName + ']' end as ColumnAsName,ReportColumn,
                        //ControlType,ColumnId,case when len(parentColumn)> 3 then 't.' + substring(parentColumn, 4, len(parentColumn) - 3) else '' end as ParentControl,
                        //isnull(ForeignKeyName, 0) as ForeignKeyName ,FilterBy,ISNULL(DefaultValue, ''''),ColumnName,GlobalParamId
                        //,isnull(TableName, ''),isnull(ColumnValue, ''),isnull(ColumnText, ''),isnull(ColumnTextExp, ''),isnull(WhereExpression, ''),isnull(OrderByExpression, ''),isnull(SqlExpression, '')
                        //FROM UTL_DYNAMIC_TABLE_COLUMN where TableId = @TableId  and((ISNULL(ReportColumn, 0) > 0 and IsShow = 1) or ColumnProperty = 'pk') order by ReportColumn

                        IEnumerable<Utl_Dynamic_Table_Column> TableColumnReport = DynamicTableColumn.Where(s => s.TableId == TableId &&
                        (((s.ReportColumn == null ? 0 : s.ReportColumn) > 0 && s.IsShow == true) || s.ColumnProperty == "pk")).OrderBy(f => f.ReportColumn);

                        DisplayOnModel = DynamicGridView.DisplayOnModel(TableColumnReport, FK_VALUE);
                    }
                }
                
                string ForeignTableName = "";
                int IsDummyExit = 0;
                string ForeignTableScript = "";


                if (DisplayOn == "formForeign")
                {
                    long? foreignTableId = Dynamic_Table.Where(s => s.TableId == TableId).Select(p => p.ForeignTableId).FirstOrDefault();
                    //SELECT TOP 1 TableName FROM  UTL_DYNAMIC_TABLE WHERE TableId = (SELECT ForeignTableId FROM  UTL_DYNAMIC_TABLE WHERE TableId = @TableId)
                    ForeignTableName = Dynamic_Table.Where(s => s.TableId == foreignTableId).Select(s => s.TableName).FirstOrDefault();
                    //SELECT TOP 1 @ForeignTableScript = ' LEFT OUTER JOIN ' + @ForeignTableName + ' ft ON t.' + ColumnName + '=ft.' + ForeignColumnName FROM UTL_DYNAMIC_TABLE_COLUMN 
                    //where TableId = @TableId AND ISNULL(ForeignColumnName,'')<> '' and[ColumnProperty] in ('fdk', 'pk') SET @ForeignTableScript = ISNULL(@ForeignTableScript, '')

                    var tableColumn = DynamicTableColumn.Where(t => t.TableId == TableId && t.ForeignColumnName != "" && t.ColumnProperty == "fdk" && t.ColumnProperty == "pk")
                        .Select(s=> new { s.ColumnName, s.ForeignColumnName}).FirstOrDefault();
                    ForeignTableScript = " LEFT OUTER JOIN " + ForeignTableName + " ft ON t." + tableColumn.ColumnName + "=ft." + tableColumn.ForeignColumnName;

                }

                string ColumnId = "";
                string ParentControl = "";
                string ControlType = "";
                long AltTableId = 0;
                string AltTableScript = "";
                string Column = "";

                string GroupFunc = "";
                string GroupExp = "";
                string BaseColumn = "";
                int GlobalParam = 0;
                bool IsGroupFunc = false;
                string linkColumn = "";
                int? linkTableId = 0;
                int? linkPageId = 0;
                int? linkType = 0;
                string linkPkColumn = "";

                string ddlTableName = "";
                string ColumnValue = "";
                string ColumnText = "";
                string ColumnTextExp = "";
                string WhereExpression = "";
                string DdlTextScript = "";
                string DdlJoinTable = "";
                string alias = "";
                int ColIndex = 0;


                foreach (var item in DisplayOnModel)
                {
                    linkColumn = "";
                    linkPageId = 0;
                    linkTableId = 0;
                    linkPkColumn = "";
                    linkType = 0;

                    if (DisplayOn != "report" && DisplayOn != "formForeign")
                    {
                        //select @linkPageId = linkPageId, @linkTableId = LinkTableId, @linkPkColumn = LinkColumnName, @linkType = LinkType
                        //from Utl_Dynamic_Table_Column where TableId = @parmTableId and ColumnName = CAST(@ColumnName as varchar(500))

                        var tblCol = DynamicTableColumn.Where(r => r.TableId == parmTableId && r.ColumnName == item.ColumnName.ToLower() ).FirstOrDefault();
                        linkPageId = tblCol.LinkPageId;
                        linkTableId = tblCol.LinkTableId;
                        linkPkColumn = tblCol.LinkColumnName;
                        linkType = Convert.ToInt16(tblCol.LinkType);
                        WhereExpression = item.WhereExpression;

                        if ((linkType = linkType == null ? 0 : linkType) != 0 && (linkPkColumn = linkPkColumn == null ? "0" : linkPkColumn).Length > 1)
                        {

                            if (linkType == 1 && (linkTableId = linkTableId == null ? 0 : linkTableId) > 0)
                            {

                                linkColumn = "''" + "<a href='DynamicReport.aspx? Worksheet = " + linkPageId.ToString() + "&User=" + UserId.ToString() + "&App=0&Comp=0&TableId=" +
                                    linkTableId.ToString() + "&PkId=''+ cast(" + "t." + linkPkColumn + " as Varchar(200)) +''&Type=3&filter='" + " style='color: blue'  target='_blank'>''" +
                                    "+cast(@Column@ as varchar)+" + "''</a>" + "''";
                            }
                            else if (linkType == 2)
                            {
                                linkColumn = "''" + "<a href='DynamicReport.aspx? Worksheet = '' + cast(t.Module_Id as Varchar(200)) + '' & User = " + UserId.ToString() +
                                    "'&App=0&Comp=0&TableId=''+ cast(t.Table_Id as Varchar(200)) +''&PkId=''+ cast(" + "t." + linkPkColumn + " as Varchar(200)) +''&Type=3&filter='" +
                                    " style='color: blue'  target='_blank'>''" + "+cast(@Column@ as varchar)+" + "''</a>" + "''";
                            }
                            else if (linkType == 3)
                            {
                                linkColumn = "''" + "<a href='DynamicReport.aspx? Worksheet = '' + cast((select Module_Id from inv_mf_sys_mm_process where Process_Id = t.Source_Ref_Type) as Varchar(200)) +'' & User = " +
                                    UserId.ToString() + "&App=0&Comp=0&TableId=''+ cast((select Table_Id from inv_mf_sys_mm_process where Process_Id = t.Source_Ref_Type) as Varchar(200)) +'' & PkId = '' + cast(" +
                                    "t." + linkPkColumn + " as Varchar(200)) +''&Type=3&filter='" + " style='color: blue'  target='_blank'>''" + "+cast(@Column@ as varchar)+" + "''</a>" + "''";
                            }
                        }
                    }

                    linkColumn = linkColumn == null ? "" : linkColumn;

                    IsDummyExit = item.ControlType == "dfl" || item.ControlType == "dft" || item.ControlType == "dfd" ? 1 : IsDummyExit;
                    Column = "";

                    if ((item.ColumnName.Substring(0, 7).ToUpper()) == "SELECT ")
                    {
                        Column = "(" + item.ColumnName + ") AS " + item.ColumnAsName;
                    }
                    else if (item.ControlType == "ddl" || item.ControlType == "dfd")
                    {
                        DdlTextScript = "";
                        alias = " l" + item.ColumnId;

                        ColumnText = item.ColumnText != "" && item.ColumnText != "0" ? alias + "." + item.ColumnText : item.ColumnText;
                        ColumnText = item.ColumnTextExp != "" && item.ColumnTextExp != "0" ? item.ColumnTextExp.Replace("@.", alias.TrimStart() + "."): item.ColumnText;

                        if ((item.ddlTableName != "" && item.ddlTableName != "0") && (item.ColumnValue != "" && item.ColumnValue != "0") && (item.ColumnText != "" && item.ColumnText !="0"))
                        {
                            DdlJoinTable = DdlJoinTable + " left outer join " + item.ddlTableName;
                            DdlJoinTable = DdlJoinTable + alias + " on cast(" + alias + "." + item.ColumnValue + " as varchar(200))=cast(t." + item.ColumnName + " as varchar(200))";

                            if (WhereExpression != "")
                            {
                                WhereExpression = WhereExpression.Replace("@fk", FkId);
                                WhereExpression = WhereExpression.Replace("@parent", ParentControl);
                                WhereExpression = WhereExpression.Replace("@columnid", ColumnId);
                                WhereExpression = WhereExpression.Replace("User_Id=@user and", " ");
                                WhereExpression = WhereExpression.Replace("@.", alias.TrimStart() + ".");
                                DdlJoinTable = DdlJoinTable + " and " + WhereExpression;
                            }
                            DdlTextScript = ColumnText;
                        }

                        if (linkColumn != "")
                        {
                            DdlTextScript = DdlTextScript != "" ? linkColumn.Replace("@Column@", DdlTextScript) : linkColumn.Replace("@Column@", "t." + item.ColumnName);
                        }
                        Column = DdlTextScript != "" ? "(" + DdlTextScript + ") AS " + item.ColumnAsName : "(t." + item.ColumnName + ") AS " + item.ColumnAsName;

                    }
                    else if ((item.ControlType == "dfl" || item.ControlType == "dft") && ForeignTableScript !="")
                    {
                        Column = item.ColumnName + " AS " + item.ColumnAsName;
                    }
                    else if (item.AltTableId > 0)
                    {
                        string AltTableName = Dynamic_Table.Where(n=>n.TableId == item.AltTableId).Select(n=>n.TableName).FirstOrDefault();
                        AltTableName = AltTableName == null ? "" : AltTableName;

                        string AltPkColName = DynamicTableColumn.Where(d=> d.TableId == item.AltTableId && d.ColumnProperty == "pk").Select(d=>d.ColumnName).FirstOrDefault();
                        AltPkColName = AltPkColName == null ? "" : AltPkColName;

                        string AltColName = DynamicTableColumn.Where(d => d.TableId == item.AltTableId && d.ColumnProperty == "pkt").Select(d => d.ColumnName).FirstOrDefault();
                        AltColName = AltColName == null ? "" : AltColName;

                        string AltTableAlias = "";
                        if ((AltTableName != "" && AltTableName != "0") && (AltPkColName != "" && AltPkColName!="0") && (AltColName !=""&& AltColName!="0"))
                        {
                               
                            AltTableAlias = "t" + item.ColumnId.ToString();
                            Column = "(" + AltTableAlias + "." + AltColName + ") AS " + item.ColumnAsName;
                            AltTableScript = AltTableScript + " LEFT OUTER JOIN (SELECT " + AltPkColName + "," + AltColName + " FROM " + AltTableName + ") " 
                                + AltTableAlias + " ON t." + item.ColumnName + "=" + AltTableAlias + "." + AltPkColName;
                        }
                    }
                    else
                    {
                        if (item.GlobalParam == 8)
                        {
                            Column = " Case when year(t." + item.ColumnName + ")=1900 then '' else Convert(varchar,t." + item.ColumnName + ",9) end AS " + item.ColumnAsName;
                        }
                        else if (item.GlobalParam == 9)
                        {
                            Column = " Case when year(t." + item.ColumnName + ")=1900 then '' else Convert(varchar,t." +item.ColumnName +",6) end AS " + item.ColumnAsName;
                        }
                        else if (item.GlobalParam == 10)
                        {
                            Column = " Convert(varchar,t." + item.ColumnName + ",8) AS " + item.ColumnAsName;
                        }
                        else
                        {

                            Column = linkColumn != "" ? "(" + linkColumn.Replace("@Column@", "t." + item.ColumnName) + ") AS " + item.ColumnAsName : 
                                "(t." + item.ColumnName + ") AS " + item.ColumnAsName;
                                
                        }
                    }

                    if (QuickSearch != "" && item.ColumnAsName != "DataKey")
                    {
                        if (QuickSearchColumn != "")
                        {
                            QuickSearchColumn = QuickSearchColumn + "+ isnull(convert(varchar(2000)," + item.ColumnAsName + "),'')";
                        }
                        else
                        {
                            QuickSearchColumn = QuickSearchColumn + "isnull(convert(varchar(2000)," + item.ColumnAsName + "),'')";
                        }
                    }

                    if (Sql != "" && Column != "" && Column !="0")
                    {
                        Sql = Sql + "," + Column;
                    }
                    else if (Column != "" && Column != "0")
                    {
                        Sql = Sql + Column;
                    }

                    if (ColIndex == 1 && IsWorkFollowModule == 1 && WorkfollowAction == "2")
                    {
                        Sql = Sql + ", (case when t.work_follow_position=2 then 'Prepared' "+
                            "when t.work_follow_position > 2 and len(isnull(t.Check_By,''))> 1 and isnull(t.Is_Approved,0)= 0 then 'Checked'"+
                            " when isnull(t.Is_Approved,0)= 1 then 'Approved' else 'Recommend' end) as Status";
                    }
                    ColIndex = ColIndex + 1;

                    if (item.BaseColumn != "Dummy" && DisplayOn != "formForeign")
                    {
                        //select @GroupFunc = ISNULL(AggregateFunction, ''), @GroupExp = ISNULL(EvaluateValue, '') from UTL_DYNAMIC_TABLE_FUNCTION where TableId = @TableId and 
                        //ColumnName = @BaseColumn and ISNULL(AggregateFunction, '') NOT IN('','0') AND ISNULL(EvaluateValue,'')<> ''

                        var tblFunction = DynamicTblFunc.Where(w => w.TableId == TableId && w.ColumnName == item.BaseColumn && (w.AggregateFunction != "" && w.AggregateFunction != "0")
                        && (w.EvaluateValue = w.EvaluateValue == null ? "" : w.EvaluateValue) != "").Select(f => new { f.AggregateFunction, f.EvaluateValue }).FirstOrDefault();

                        if (tblFunction != null)
                        {
                            GroupFunc = tblFunction.AggregateFunction == null ? "" : tblFunction.AggregateFunction;
                            GroupExp = tblFunction.EvaluateValue == null ? "" : tblFunction.EvaluateValue;
                        }
                        

                        if (GroupFunc != "" && GroupExp != "")
                        {
                            GroupExp = GroupFunc + "(" + GroupExp + ") AS " + item.BaseColumn;
                            IsGroupFunc = true;
                        }
                        else
                        {
                            GroupExp = item.BaseColumn;
                            GroupClause = GroupClause != "" ? GroupClause + "," : GroupClause;
                            GroupClause = GroupClause + item.BaseColumn;
                        }
                        GroupSql = GroupSql != "" ? GroupSql + "," : GroupSql;
                        GroupSql = GroupSql + GroupExp;

                    }
                    

                }

                string DefaultExpression = "";
                int? FilterBy = 0;
                string DefaultValue = "";
                int IsPreviledge = 0;
                int IsTransUser = 0;
                int AdminPrivilegeType = 0;

                if (ModuleId == 187)  //--change password
                {
                    IsPreviledge = 1;
                    IsTransUser = 1;
                }
                else
                {

                    //set @AdminPrivilegeType = isnull((select AdminPrivilegeType from wf_User_Registration where UserId = @UserId),0)

                    IsPreviledge = (ModuleId == 3 && AdminPrivilegeType == 5) || AdminPrivilegeType == 0 ? IsPreviledge = 1 : IsPreviledge;
                    //if (@ModuleId = 3 and @AdminPrivilegeType = 5) or @AdminPrivilegeType = 0
                    //set @IsPreviledge = 1

                    //set @IsTransUser = isnull((select max(case when IsNew = 1 then 1 else 0 end) from wf_User_Privilege where ModuleId = @ModuleId and UserId = @UserId and Is_Deleted = 0),0) 
                }

                if (DisplayOn != "formForeign")
                {
                    FkTableId = TableId;
                    

                    var tblColForm = DynamicTableColumn.Where(r => r.TableId == FkTableId && r.IsShow == true && ((r.FilterBy == 1 && 
                    (r.GlobalParamId == 5 || r.GlobalParamId == 18)) || (r.FilterBy == 2 && r.DefaultValue != ""))).OrderBy(o => o.SlNo).FirstOrDefault();

                    DefaultValue = tblColForm.DefaultValue;
                    FilterBy = tblColForm.FilterBy;
                    ColumnName = tblColForm.ColumnName;

                    if (tblColForm.GlobalParamId == 18)
                    {
                        DefaultValue = ModuleId.ToString();
                    }
                    else if (tblColForm.GlobalParamId == 5 && UserId != "" && IsPreviledge == 1 && IsTransUser == 1)
                    {
                        DefaultValue = UserId;
                    }
                    else
                    {
                        DefaultValue = DefaultValue == null ? "" : DefaultValue;
                    }


                    if (tblColForm != null)
                    {
                        if ((DisplayOn == "module" && FilterBy == 1) || FilterBy == 2)
                        {
                            if ((ColumnName.ToLower() != "is_deleted" || IsDeleteShow != 1) && ColumnName.ToLower() != "work_follow_position")
                            {
                                DefaultExpression = DefaultExpression != "" && DefaultValue != "" ? DefaultExpression + " AND " : DefaultExpression;
                                DefaultExpression = DefaultValue != "" ? DefaultExpression + "t." + ColumnName + "=''" + DefaultValue + "''" : DefaultExpression;
                            }
                        }
                    }

                }

                if((IsPreviledge ==1 && (DisplayOn =="module" || DisplayOn == "formForeign")) || (DisplayOn == "report" && UserId !=""))
                {
                    string UserPrivilege = "";
                    string UserPrivilegeExp = "";
                    int isAll = 0;

                    var privilegeData = DynamicTableColumn.Where(t => t.TableId == FkTableId && t.IsShow == true && t.User_Privilege > 0).OrderBy(o => o.SlNo)
                        .Select(s => new { s.ColumnName, s.User_Privilege }).FirstOrDefault();
                    if (privilegeData != null)
                    {
                        ColumnName = privilegeData.ColumnName;
                        UserPrivilege = privilegeData.User_Privilege.ToString();
                    }
                    

                    if (privilegeData != null)
                    {
                        //isAll = (SELECT min(isnull(Obj_Item_Id, 0)) FROM wf_User_Object_Privilege where User_Id = @UserId and Object_Id = @UserPrivilege and Is_Active = 1)

                        if (isAll != 0)
                        {
                            if (UserPrivilegeExp != "")
                            {
                                UserPrivilegeExp = UserPrivilegeExp + " AND ";
                            }

                            UserPrivilegeExp = UserPrivilegeExp + "t." + ColumnName + " in (SELECT Obj_Item_Id FROM wf_User_Object_Privilege where User_Id='" + 
                                UserId + "' and Object_Id= '" + UserPrivilege + "' and Is_Active=1)";
                        }
                    }

                    if (UserPrivilegeExp != "")
                    {
                        DefaultExpression = DefaultExpression != "" ? DefaultExpression + " and " + UserPrivilegeExp : UserPrivilegeExp;
                    }
                }

                bool? IsDesc = false;
                string ORDER_BY = "";
                string DESC = "";

                FkTableId = DisplayOn != "formForeign" ? TableId : FkTableId;

                var tblFunc = DynamicTblFunc.Where(s => s.TableId == FkTableId && s.IsDataShort == true).OrderBy(s => s.SLNo)
                    .Select(s => new { s.ColumnName, s.IsDescending }).FirstOrDefault();
                
                if (tblFunc != null)
                {
                    ColumnName = tblFunc.ColumnName;
                    IsDesc = tblFunc.IsDescending == null ? false : tblFunc.IsDescending;
                }
                
                
                if (tblFunc != null)
                {
                    DESC = "";
                    DESC = IsDesc != false ? " desc" : DESC;

                    if (ColumnName != "")
                    {
                        ORDER_BY = ORDER_BY == "" ? " ORDER BY t." + ColumnName + DESC : ORDER_BY + ", " + "t." + ColumnName + DESC;
                    }
                }

                if (Sql != "")
                {
                    string Expression = "";
                    string AndExp = "";
                    string OrExp = "";
                    string SqlAndExp = "";
                    //string SqlOrExp = "";
                    string Operator = "";

                    if (DisplayOn != "formForeign")
                    {
                        FkTableId = TableId;
                        List<Utl_Dynamic_Operator> OperatorTbl = jsonWriteRead.SingleTbljsonFileRead<Utl_Dynamic_Operator>("OperatorTable.json");
                   
                        var filterColumnOperatorTbl = from f in TblFilterProperty
                                                      join c in DynamicTableColumn on f.ColumnId equals c.ColumnId
                                                      join o in OperatorTbl on f.OperatorId equals o.OperatorId
                                                      where f.TableId == FkTableId && f.IsActive == true
                                                      select new
                                                      {
                                                          ColumnName = string.Format("t.{0}", c.ColumnName),
                                                          o.OperatorValue,
                                                          f.AndExpression,
                                                          f.OrExpression
                                                      };

                        foreach (var item in filterColumnOperatorTbl)
                        {
                            ColumnName = item.ColumnName;
                            Operator = item.OperatorValue;
                            AndExp = item.AndExpression;
                            OrExp = item.OrExpression;
                        }

                        if (ColumnName.ToUpper() != "T.IS_DELETED" || IsDeleteShow != 1)
                        {
                            SqlAndExp = SqlAndExp != "" ? SqlAndExp + " AND " : SqlAndExp;
                            SqlAndExp = SqlAndExp + ColumnName + " " + Operator + " " + AndExp;
                        }
                    }

                    Expression = SqlAndExp;

                    if (IsWorkFollowModule == 1)
                    {
                        DefaultExpression = DefaultExpression != "" ? DefaultExpression + " and " : DefaultExpression;
                        DefaultExpression = WorkfollowAction == "0" || WorkfollowAction == "1" ? DefaultExpression + " t.work_follow_position in ('0','1')"
                            : DefaultExpression+ " t.work_follow_position>1";
                    }

                    FilterTmpExp = ForeignTableScript == "" ? "" : FilterTmpExp;

                    if (ExternalDataFilter != "")
                    {
                        FilterTmpExp = FilterTmpExp != "" ? FilterTmpExp + " and " + ExternalDataFilter : ExternalDataFilter;
                    }
                    
                    if (RecordType == 2)
                    {
                        if (DefaultExpression.Contains("t.is_deleted"))
                        {
                            DefaultExpression = DefaultExpression.Replace("t.is_deleted='0'", "t.is_deleted='1'");
                        }
                    }

                    if (Expression == "")
                    {
                        Expression = DefaultExpression;
                    }
                    else if(DefaultExpression != "")
                    {
                        Expression = Expression + " AND " + DefaultExpression;
                    }

                    Expression = Expression != "" && FK_EXPRESSION != "" ? Expression + " AND " : Expression;
                    Expression = Expression + FK_EXPRESSION;

                    Expression = Expression != "" && ForeignDataExpression != "" ? Expression + " AND " : Expression;
                    Expression = Expression + ForeignDataExpression;

                    Expression = Expression != "" ? " WHERE " + Expression: Expression;

                    string TempScript = "";
                    string TabAlias = "";

                    TabAlias = " t ";
                    GroupClause = GroupClause != "" ? " GROUP BY " + GroupClause : GroupClause;

                    TempScript = IsGroupFunc == true ? "SELECT " + GroupSql + " INTO #" + TableName + " FORM " + TableName + TabAlias + Expression + GroupClause
                        : "SELECT * INTO #" + TableName + " FROM " + TableName + TabAlias + Expression;

                    FilterExpression = FilterExpression != "" ? " WHERE " + FilterExpression : FilterExpression;

                    if (FilterExpression != ""&& FilterTmpExp != "")
                    {
                        FilterExpression = FilterExpression + " AND " + FilterTmpExp;
                    }
                    else if (FilterExpression == "" && FilterTmpExp != "")
                    {
                        FilterExpression = " WHERE " + FilterTmpExp;
                    }

                    TableName = DisplayOn == "formForeign" ? TableName + " t " + ForeignTableScript + AltTableScript + DdlJoinTable + " "
                        : TableName + " t " + AltTableScript + DdlJoinTable + " ";

                    string top = "";
                    if (DisplayOn == "formForeign")
                    {
                        top = "top 400";
                    }
                    else if (DisplayOn == "report")
                    {
                        top = "";
                    }
                    else
                    {
                        top = "top 500";
                    }


                    if (QuickSearch != "")
                    {
                        QuickSearch = QuickSearch.Replace("''", "'");
                        Sql = TempScript + " SELECT " + Sql + " INTO #tempData" + " FROM #" + TableName + FilterExpression + ORDER_BY;
                        Sql = Sql + " SELECT " + top + " * from #tempData where " + QuickSearchColumn + " like '%" + QuickSearch + "%'";
                    }
                    else
                    {
                        Sql = TempScript + " SELECT " + top + Sql + " FROM #" + TableName + FilterExpression + ORDER_BY;
                    }

                    
                    try
                    {
                        string sql = Sql;
                        var tblData = Dynamic_Table.Where(t => t.TableId == TableId).Select(s => new
                        {
                            s.TableCaption,
                            FkTableId,
                            s.ReportFileName,
                            s.ProcedureCaption,
                            s.IsExternalDataShow,
                            s.ExternalDataCaption
                        });

                    }
                    catch (Exception e)
                    {
                        string msg = e.Message;
                        
                        //--Storeprocedure
                        //EXEC Tmp_Execute_EventLog @ModuleId,@TableId,@UserId,@FkId,'Select',@DisplayOn,@Sql,@msg

                        int DataKey = 0;

                        var tblData = Dynamic_Table.Where(t => t.TableId == TableId).Select(s => new
                        {
                            s.TableCaption,
                            FkTableId,
                            s.ReportFileName,
                            s.ProcedureCaption,
                            s.IsExternalDataShow,
                            s.ExternalDataCaption
                        }).ToList();

                    }



                }
                

            }

            string EndTime = DateTime.Now.Second.ToString() + ":" + DateTime.Now.Millisecond.ToString();

            List<string> dataList = new List<string>();
            dataList.Add(startTime);
            dataList.Add(Sql);
            dataList.Add(EndTime);

            return Json( dataList);
        }
    }
}