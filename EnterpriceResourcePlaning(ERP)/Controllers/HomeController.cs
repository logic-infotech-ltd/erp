﻿using EnterpriceResourcePlaning_ERP_.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace EnterpriceResourcePlaning_ERP_.Controllers
{
    public class HomeController : Controller
    {
        string connS = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public ActionResult HomePage()
        {
            SqlConnection cont = new SqlConnection(connS);
            string query = "SELECT [Id],[Applications],[ImageUrl], [ApplicationLink] FROM [dbo].[HomePage]";
            SqlCommand cmd = new SqlCommand(query, cont);

            cont.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            var viewList = new List<HomePageApp>();
            while (reader.Read())
            {
                int id = Convert.ToInt32(reader["Id"]);
                string application = reader["Applications"].ToString();
                string imageUrl = reader["ImageUrl"].ToString();
                string applicationLink = reader["ApplicationLink"].ToString();
                HomePageApp VMModule = new HomePageApp(id, application, imageUrl, applicationLink);
                viewList.Add(VMModule);
            }
            reader.Close();
            cont.Close();
            return View(viewList);
        }

        public ActionResult Material()
        {
            SqlConnection con = new SqlConnection(connS);
            string query = "SELECT [Id],[Modules] ,[ModuleLink] FROM [dbo].[MaterialModule]";
            SqlCommand cmd = new SqlCommand(query, con);

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            var materialList = new List<MaterialModule>();
            while (reader.Read())
            {
                int id = Convert.ToInt32(reader["Id"]);
                string module = reader["Modules"].ToString();
                string moduleLink = reader["ModuleLink"].ToString();
                MaterialModule MaterialViewModel = new MaterialModule(id, module, moduleLink);
                materialList.Add(MaterialViewModel);
            }
            reader.Close();
            con.Close();
            return View(materialList);
        }

        public ActionResult NavbarView()
        {
            return View();
        }

        public JsonResult GetSubMenus(int? parentId)
        {
            List<ManuBar> ManuBarList = new List<ManuBar>();

            ManuBar MenuBar = new ManuBar();
            MenuBar.ActionName = "About";
            MenuBar.ControllerName = "Home";

            ManuBar MenuBar2 = new ManuBar();
            MenuBar2.ActionName = "Contact";
            MenuBar2.ControllerName = "Home";

            ManuBar MenuBar3 = new ManuBar();
            MenuBar3.ActionName = "Index";
            MenuBar3.ControllerName = "Home";

            ManuBar MenuBar4 = new ManuBar();
            MenuBar4.ActionName = "Purchase Order";
            MenuBar4.ControllerName = "Home";

            ManuBar MenuBar5 = new ManuBar();
            MenuBar5.ActionName = "Work Order";
            MenuBar5.ControllerName = "Home";

            ManuBar MenuBar6 = new ManuBar();
            MenuBar6.ActionName = "Cash Purchase";
            MenuBar6.ControllerName = "Home";

            ManuBar MenuBar7 = new ManuBar();
            MenuBar7.ActionName = "Proforma Invoice";
            MenuBar7.ControllerName = "Home";

            ManuBar MenuBar8 = new ManuBar();
            MenuBar8.ActionName = "LC Open";
            MenuBar8.ControllerName = "Home";

            ManuBar MenuBar9 = new ManuBar();
            MenuBar9.ActionName = "LC Documents";
            MenuBar9.ControllerName = "Home";

            ManuBar MenuBar10 = new ManuBar();
            MenuBar10.ActionName = "Goods Receiving Note (Purchase)";
            MenuBar10.ControllerName = "Home";

            ManuBar MenuBar11 = new ManuBar();
            MenuBar11.ActionName = "Goods Receive (Opening/Loan/Adjustment)";
            MenuBar11.ControllerName = "Home";

            ManuBar MenuBar12 = new ManuBar();
            MenuBar12.ActionName = "Goods Receiving Inspaction (GRI)";
            MenuBar12.ControllerName = "Home";

            ManuBar MenuBar13 = new ManuBar();
            MenuBar13.ActionName = "Quality Control (QC)";
            MenuBar13.ControllerName = "Home";


            ManuBarList.Add(MenuBar);
            ManuBarList.Add(MenuBar2);
            ManuBarList.Add(MenuBar3);
            ManuBarList.Add(MenuBar4);
            ManuBarList.Add(MenuBar5);
            ManuBarList.Add(MenuBar6);
            ManuBarList.Add(MenuBar7);
            ManuBarList.Add(MenuBar8);
            ManuBarList.Add(MenuBar9);
            ManuBarList.Add(MenuBar10);
            ManuBarList.Add(MenuBar11);
            ManuBarList.Add(MenuBar12);
            ManuBarList.Add(MenuBar13);


            return Json(ManuBarList);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}