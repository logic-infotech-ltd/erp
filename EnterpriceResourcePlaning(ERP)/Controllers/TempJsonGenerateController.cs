﻿using EnterpriceResourcePlaning_ERP_.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EnterpriceResourcePlaning_ERP_.Controllers
{
    public class JsonDataGenerate1Controller : Controller
    {
        string connection = WebConfigurationManager.ConnectionStrings["ErpModelEntities"].ConnectionString;


        

        
        // GET: JsonDataGenerate
        public ActionResult JsonfileCreate()
        {
            SqlConnection conn = new SqlConnection(connection);


            //////////1. For Module
            
            string modulequery = "SELECT [ModuleId] ,[ApplicationId] ,[ExistingModuleId] ,[IsNewCopy] ,[ModuleName] ,[ModuleCaption] ,[ReportTitle] "
                +",[IsDrilldownReport] ,[CategoryId] ,[ImagePath] ,[ModuleType] ,[SlNo] ,[IsActive] ,[DeveloperSyncId] ,[Entry_User_Id]"
                +" ,[Entry_By] ,[Entry_Date] ,[Update_By] ,[Update_Date] ,[Check_By] ,[Check_Date] ,[Approve_By] ,[Approve_Date] ,[Delete_By] "
                +",[Delete_Date] ,[Is_Approved]  ,[Is_Active] ,[Is_Deleted] ,[Work_Follow_Position] ,[Row_Id] ,[Cancel_By] ,[Cancel_Date]"
                +" ,[Is_Cancel] FROM [dbo].[Utl_Dynamic_Module]";

            SqlCommand modulCmd = new SqlCommand(modulequery, conn);
            conn.Open();
            SqlDataReader modulReader = modulCmd.ExecuteReader();

            var ModuleList = new List<Utl_Dynamic_Module>();
            while (modulReader.Read())
            {

                long moduleId = long.Parse(modulReader["ModuleId"].ToString());
                int applicationId = Convert.ToInt32(modulReader["ApplicationId"]);
                long ExiModulId = long.Parse(modulReader["ExistingModuleId"].ToString());
                bool isNewCopy = bool.Parse(modulReader["IsNewCopy"].ToString());

                string moduleName = modulReader["ModuleName"].ToString();
                string moduleCaption = modulReader["ModuleCaption"].ToString();
                string reportTitle = modulReader["ReportTitle"].ToString();
                bool isDrillDownReport = bool.Parse(modulReader["IsDrilldownReport"].ToString());

                int categoryId = Convert.ToInt32(modulReader["CategoryId"]);
                string imagePath = modulReader["ImagePath"].ToString();
                int moduleType = Convert.ToInt32(modulReader["ModuleType"]);
                decimal siNo = decimal.Parse(modulReader["SlNo"].ToString());

                bool isActive =bool.Parse(modulReader["IsActive"].ToString());
                string developerSyId = modulReader["DeveloperSyncId"].ToString();
                string rowId = modulReader["Row_Id"].ToString();

                long entryUserId = long.Parse(modulReader["Entry_User_Id"].ToString());
                string entryBy = modulReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(modulReader["Entry_Date"]);

                string updateBy = modulReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(modulReader["Update_Date"]);

                string checkBy = modulReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(modulReader["Check_Date"]);

                string approveBy = modulReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(modulReader["Approve_Date"]);

                string deleteBy = modulReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(modulReader["Delete_Date"]);

                string cancelBy = modulReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(modulReader["Cancel_Date"]);

                bool isApproved = bool.Parse(modulReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(modulReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(modulReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(modulReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(modulReader["Work_Follow_Position"]);


                Utl_Dynamic_Module ModulVM = new Utl_Dynamic_Module(moduleId, applicationId, ExiModulId, isNewCopy, moduleName, moduleCaption, reportTitle, isDrillDownReport, categoryId,
                    imagePath, moduleType, siNo, isActive, developerSyId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                    deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                ModuleList.Add(ModulVM);
            }
            modulReader.Close();


            ////////////////2. For Table
            ///
            string Tablequery = "SELECT [TableId] ,[ModuleId] ,ParentModuleId,ForeignModuleId, [ApplicationId] ,[TableCaption] ,[ModuleTableCaption],[TableName] ,[RefreshType] ,[ParentTableId] "
                + ",[ForeignTableId]  ,[IsExternalDataShow] ,[ExternalDataCaption] ,[ExternalDataFilter]  ,[ExistingTableId]  ,[ModuleWidth] ,[ModuleHeight] ,[FormWidth]"
                +" ,[PositionExp] ,[RowPosition] ,[ColPosition] ,[IsReadOnly] ,[IsWorkFollow] ,[IsAutoToggle] ,[ProcedureName] ,[ProcedureCaption] ,[ProcedureValidation]"
                +" ,[ProcFireOnTrans],[ReportProcedureName] ,[RptProcForWorksheet] ,[ProcFooterView],[ProcFooterDml] ,[ReportFileName] ,[ShowOnReport],[SqlStatement], "
                +"[ObjectType] ,[TemplateType] ,[SlNo]  ,[DeveloperSyncId] ,[Entry_User_Id] ,[Entry_By]  ,[Entry_Date] ,[Update_By] ,[Update_Date] ,[Check_By],"
                +"[Check_Date]  ,[Approve_By] ,[Approve_Date] ,[Delete_By] ,[Delete_Date] ,[Is_Approved] ,[Is_Active] ,[Is_Deleted] ,[Work_Follow_Position],[Row_Id],"
                +"[Cancel_By] ,[Cancel_Date] ,[Is_Cancel] FROM [Utl_Dynamic_Table]";

            SqlCommand tableCmd = new SqlCommand(Tablequery, conn);
            SqlDataReader tableReader = tableCmd.ExecuteReader();

            var TableList = new List<Utl_Dynamic_Table>();
            while (tableReader.Read())
            {

                long tableId = long.Parse(tableReader["TableId"].ToString());
                long moduleId = long.Parse(tableReader["ModuleId"].ToString());
                long parentModuleId = long.Parse(tableReader["ParentModuleId"].ToString());
                long foreignModuleId = long.Parse(tableReader["ForeignModuleId"].ToString());
                long applicationId = long.Parse(tableReader["ApplicationId"].ToString());

                string tableCaption = tableReader["TableCaption"].ToString();
                string moduleTblCaption = tableReader["ModuleTableCaption"].ToString();
                string tableName = tableReader["TableName"].ToString();

                string refreshType = tableReader["RefreshType"].ToString();

                long parentTableId = long.Parse(tableReader["ParentTableId"].ToString());
                long foreignTableId = long.Parse(tableReader["ForeignTableId"].ToString());

                bool isExternalDataShow = bool.Parse(tableReader["IsExternalDataShow"].ToString());
                string externalDataCaption = tableReader["ExternalDataCaption"].ToString();
                string externalDataFilter = tableReader["ExternalDataFilter"].ToString();

                long existingTableId =long.Parse(tableReader["ExistingTableId"].ToString());
                int moduleWidth = Convert.ToInt32(tableReader["ModuleWidth"]);
                int moduleHeight = Convert.ToInt32(tableReader["ModuleHeight"]);
                int formWidth = Convert.ToInt32(tableReader["FormWidth"]);

                string positionExp = tableReader["PositionExp"].ToString();
                int rowPosition = Convert.ToInt32(tableReader["RowPosition"]);
                int colPosition = Convert.ToInt32(tableReader["ColPosition"]);

                bool isReadonly = bool.Parse(tableReader["IsReadOnly"].ToString());
                bool isWorkFollow = bool.Parse(tableReader["IsWorkFollow"].ToString());
                bool isAutoToggle = bool.Parse(tableReader["IsAutoToggle"].ToString());

                string procedureName = tableReader["ProcedureName"].ToString();
                string procedureCaption = tableReader["ProcedureCaption"].ToString();
                string procedureValidation = tableReader["ProcedureValidation"].ToString();

                bool proFireOntrans = bool.Parse(tableReader["ProcFireOnTrans"].ToString());
                string reportProcName = tableReader["ReportProcedureName"].ToString();
                bool rptProcForWorksheet = bool.Parse(tableReader["RptProcForWorksheet"].ToString());

                string procFooterView = tableReader["ProcFooterView"].ToString();
                string procFooterDml = tableReader["ProcFooterDml"].ToString();
                string rptFileName = tableReader["ReportFileName"].ToString();

                bool showOnReport = bool.Parse(tableReader["ShowOnReport"].ToString());
                string sqlStatement = tableReader["SqlStatement"].ToString();

                string objectType = tableReader["ObjectType"].ToString();
                string templateType = tableReader["TemplateType"].ToString();

                decimal slNo = decimal.Parse(tableReader["SlNo"].ToString());
                string developerSynId = tableReader["DeveloperSyncId"].ToString();
                string rowId = tableReader["Row_Id"].ToString();


                long entryUserId = long.Parse(tableReader["Entry_User_Id"].ToString());
                string entryBy = tableReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(tableReader["Entry_Date"]);

                string updateBy = tableReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(tableReader["Update_Date"]);

                string checkBy = tableReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(tableReader["Check_Date"]);

                string approveBy = tableReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(tableReader["Approve_Date"]);

                string deleteBy = tableReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(tableReader["Delete_Date"]);

                string cancelBy = tableReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(tableReader["Cancel_Date"]);

                bool isApproved = bool.Parse(tableReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(tableReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(tableReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(tableReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(tableReader["Work_Follow_Position"]);


                Utl_Dynamic_Table TableVM = new Utl_Dynamic_Table(tableId, moduleId, parentModuleId, foreignModuleId, applicationId, tableCaption, moduleTblCaption, tableName, refreshType, parentTableId, foreignTableId,
                    isExternalDataShow, externalDataCaption, externalDataFilter, existingTableId, moduleWidth, moduleHeight, formWidth, positionExp, rowPosition, colPosition, isReadonly, 
                    isWorkFollow, isAutoToggle, procedureName, procedureCaption, procedureValidation, proFireOntrans, reportProcName, rptProcForWorksheet, procFooterView, procFooterDml, 
                    rptFileName, showOnReport, sqlStatement, objectType, templateType, slNo, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, 
                    checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                TableList.Add(TableVM);
            }
            tableReader.Close();
           


            /////////////3. For Table column 

            string TableColumnQuery = "SELECT [ColumnId],[TableId],ModuleId,LovModuleId,[ColumnName],[SelectColumnName],[ColumnDisplayName],[ColumnProperty],[ControlType],[ParentColumn],[ForeignKeyName]," +
                "[ForeignColumnName] ,[ForeignDataFilterBy],[ColumnWidth],[DbColumnWidth],[TransPolicyId],[GlobalParamId],[GlobalParamValue],[DefaultValue],[FilterBy],[User_Privilege],"+
                "[IsModuleSelect] ,[IsFvSelect],[IsGvSelect],[IsForeignTable],[IsDefaultFocus],[IsFvUpdate],[IsFvInsert] ,[IsFvDataFixed],[IsFvForceEdit],[DdlDataSource],[LovTableId],"+
                "[TableName],[ColumnValue],[ColumnText] ,[ColumnTextExp],[WhereExpression],[OrderByExpression] ,[SqlExpression],[LinkType],[LinkPageId],[LinkTableId],[LinkColumnName],"+
                "[LinkUrl] ,[IsShow],[ReportColumn],[SlNo],[DeveloperSyncId],[Entry_User_Id],[Entry_By],[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],"+
                "[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active] ,[Is_Deleted],[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [Utl_Dynamic_Table_Column]";

            SqlCommand tableColumnCmd = new SqlCommand(TableColumnQuery, conn);
            SqlDataReader tableColumnReader = tableColumnCmd.ExecuteReader();

            var TableColumnList = new List<Utl_Dynamic_Table_Column>();


            while (tableColumnReader.Read())
            {

                long columnId = long.Parse(tableColumnReader["ColumnId"].ToString());
                long tableId = long.Parse(tableColumnReader["TableId"].ToString());
                long moduleId = long.Parse(tableColumnReader["ModuleId"].ToString());
                long lovModuleId = long.Parse(tableColumnReader["LovModuleId"].ToString());

                string columnName = tableColumnReader["ColumnName"].ToString();
                string selectColName = tableColumnReader["SelectColumnName"].ToString();
                string colDisplayName = tableColumnReader["ColumnDisplayName"].ToString();
                string colProperty = tableColumnReader["ColumnProperty"].ToString();
                string controlType = tableColumnReader["ControlType"].ToString();
                string parentcolumn = tableColumnReader["ParentColumn"].ToString();

                long foreignKeyName = long.Parse(tableColumnReader["ForeignKeyName"].ToString());
                string foreignColName = tableColumnReader["ForeignColumnName"].ToString();
                string foreignDataFilterBy = tableColumnReader["ForeignDataFilterBy"].ToString();

                int columnWidth = Convert.ToInt32(tableColumnReader["ColumnWidth"].ToString());
                int dbColumnWidth = Convert.ToInt32(tableColumnReader["DbColumnWidth"].ToString());
                int transPolicyId = Convert.ToInt32(tableColumnReader["TransPolicyId"].ToString());
                int globalParamId = Convert.ToInt32(tableColumnReader["GlobalParamId"].ToString());
                int globalParamValue = Convert.ToInt32(tableColumnReader["GlobalParamValue"].ToString());

                string defaultValue = tableColumnReader["DefaultValue"].ToString();
                int filterBy = Convert.ToInt32(tableColumnReader["FilterBy"]);
                int userPrivilege = Convert.ToInt32(tableColumnReader["User_Privilege"]);

                

                bool isModuleSelect = bool.Parse(tableColumnReader["IsModuleSelect"].ToString());
                bool isFvSelect = bool.Parse(tableColumnReader["IsFvSelect"].ToString());
                bool isGvSelect = bool.Parse(tableColumnReader["IsGvSelect"].ToString());
                bool isForeignTable = bool.Parse(tableColumnReader["IsForeignTable"].ToString());
                bool isDefaultFocus = bool.Parse(tableColumnReader["IsDefaultFocus"].ToString());

                bool isFvUpdate = bool.Parse(tableColumnReader["IsFvUpdate"].ToString());
                bool isFvInsert = bool.Parse(tableColumnReader["IsFvInsert"].ToString());
                bool isFvDataFixed = bool.Parse(tableColumnReader["IsFvDataFixed"].ToString());
                bool isFvForceEdit = bool.Parse(tableColumnReader["IsFvForceEdit"].ToString());


                
                int dblDataSource = Convert.ToInt32(tableColumnReader["DdlDataSource"].ToString());
                long lovTableId = long.Parse(tableColumnReader["LovTableId"].ToString());

                string tableName = tableColumnReader["TableName"].ToString();
                string columnValue = tableColumnReader["ColumnValue"].ToString();
                string columnText =tableColumnReader["ColumnText"].ToString();
                string columnTextExp = tableColumnReader["ColumnTextExp"].ToString();
                string WhereExpression = tableColumnReader["WhereExpression"].ToString();
                string orderByExpression = tableColumnReader["OrderByExpression"].ToString();
                string sqlExpression = tableColumnReader["SqlExpression"].ToString();

                string linkType = tableColumnReader["LinkType"].ToString();

                int linkPageId = Convert.ToInt32(tableColumnReader["LinkPageId"]);
                int linkTableId = Convert.ToInt32(tableColumnReader["LinkTableId"]);
                string linkColumnName = tableColumnReader["LinkColumnName"].ToString();

                

                string linkUrl = tableColumnReader["LinkUrl"].ToString();
                bool isShow = bool.Parse(tableColumnReader["IsShow"].ToString());
                int reportColumn = Convert.ToInt32(tableColumnReader["ReportColumn"].ToString());

                decimal slNo = decimal.Parse(tableColumnReader["SlNo"].ToString());
                string DeveloperSynId = tableColumnReader["DeveloperSyncId"].ToString();
                string rowId = tableColumnReader["Row_Id"].ToString();


                long entryUserId = long.Parse(tableColumnReader["Entry_User_Id"].ToString());
                string entryBy = tableColumnReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(tableColumnReader["Entry_Date"]);

                string updateBy = tableColumnReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(tableColumnReader["Update_Date"]);

                string checkBy = tableColumnReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(tableColumnReader["Check_Date"]);

                string approveBy = tableColumnReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(tableColumnReader["Approve_Date"]);

                string deleteBy = tableColumnReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(tableColumnReader["Delete_Date"]);

                string cancelBy = tableColumnReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(tableColumnReader["Cancel_Date"]);

                bool isApproved = bool.Parse(tableColumnReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(tableColumnReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(tableColumnReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(tableColumnReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(tableColumnReader["Work_Follow_Position"]);

                Utl_Dynamic_Table_Column tableColumnVM = new Utl_Dynamic_Table_Column(columnId, tableId, moduleId, lovModuleId, columnName, selectColName, colDisplayName, colProperty, controlType, parentcolumn, foreignKeyName,
                    foreignColName, foreignDataFilterBy, columnWidth, dbColumnWidth, transPolicyId,globalParamId, globalParamValue, defaultValue, filterBy, userPrivilege, isModuleSelect, isFvSelect, 
                    isGvSelect, isForeignTable, isDefaultFocus, isFvUpdate, isFvInsert, isFvDataFixed, isFvForceEdit, dblDataSource, lovTableId, tableName, columnValue, columnText, columnTextExp, 
                    WhereExpression, orderByExpression, sqlExpression, linkType, linkPageId, linkTableId, linkColumnName, linkUrl, isShow, reportColumn, slNo, DeveloperSynId, rowId, entryUserId, 
                    entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, 
                    workFollowPosition);

                TableColumnList.Add(tableColumnVM);
            }
            tableColumnReader.Close();



            ////////////4. For AutoCode Property.
            string autocodePropertyQuery = "SELECT [PropertyId], ModuleId, TableId, [ColumnId],[FormatType],[Format],[Expression],[ColumnName],[SlNo],[IsActive] ,[DeveloperSyncId],[Entry_User_Id],[Entry_By]," +
                "[Entry_Date],[Update_By],[Update_Date] ,[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active],[Is_Deleted],[Work_Follow_Position],"+
                "[Row_Id] ,[Cancel_By] ,[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Autocode_Property]";

            SqlCommand autocodePropertyCmd = new SqlCommand(autocodePropertyQuery, conn);
            SqlDataReader autocodePropertyReader = autocodePropertyCmd.ExecuteReader();

            var autocodePropertyList = new List<Utl_Dynamic_Autocode_Property>();

            while (autocodePropertyReader.Read())
            {

                long propertyId = long.Parse(autocodePropertyReader["PropertyId"].ToString());
                long moduleId = long.Parse(autocodePropertyReader["ModuleId"].ToString());
                long tableId = long.Parse(autocodePropertyReader["TableId"].ToString());
                long columnId = long.Parse(autocodePropertyReader["ColumnId"].ToString());

                string formatType = autocodePropertyReader["FormatType"].ToString();
                string format = autocodePropertyReader["Format"].ToString();
                string expression = autocodePropertyReader["Expression"].ToString();
                string columnName = autocodePropertyReader["ColumnName"].ToString();

                int slNo = Convert.ToInt32(autocodePropertyReader["SlNo"].ToString());
                bool isActive = bool.Parse(autocodePropertyReader["IsActive"].ToString());
                string developerSynId = autocodePropertyReader["DeveloperSyncId"].ToString();
                string rowId = autocodePropertyReader["Row_Id"].ToString();

                long entryUserId = long.Parse(autocodePropertyReader["Entry_User_Id"].ToString());
                string entryBy = autocodePropertyReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(autocodePropertyReader["Entry_Date"]);

                string updateBy = autocodePropertyReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(autocodePropertyReader["Update_Date"]);

                string checkBy = autocodePropertyReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(autocodePropertyReader["Check_Date"]);

                string approveBy = autocodePropertyReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(autocodePropertyReader["Approve_Date"]);

                string deleteBy = autocodePropertyReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(autocodePropertyReader["Delete_Date"]);

                string cancelBy = autocodePropertyReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(autocodePropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(autocodePropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(autocodePropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(autocodePropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(autocodePropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(autocodePropertyReader["Work_Follow_Position"]);




                Utl_Dynamic_Autocode_Property autocodePropertyVM = new Utl_Dynamic_Autocode_Property(propertyId, moduleId, tableId, columnId, formatType, format, expression, columnName, slNo, isActive,
                    developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, 
                    cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                autocodePropertyList.Add(autocodePropertyVM);
            }
            autocodePropertyReader.Close();


            ////////////5. For DropDown Property.
            string ddPropertyQuery = "SELECT [PropertyId],[ColumnId],ModuleId,TableId,[PropertyType],[TableName],[ColumnValue],[ColumnText],[ColumnTextExp],[WhereExpression],[OrderByExpression]," +
                "[SqlExpression],[IsActive] ,[DeveloperSyncId],[Entry_User_Id],[Entry_By],[Entry_Date],[Update_By] ,[Update_Date] ,[Check_By],[Check_Date] ,[Approve_By],[Approve_Date],"+
                "[Delete_By],[Delete_Date],[Is_Approved] ,[Is_Active],[Is_Deleted],[Work_Follow_Position] ,[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Dropdown_Property]";

            SqlCommand ddPropertyCmd = new SqlCommand(ddPropertyQuery, conn);
            SqlDataReader ddPropertyReader = ddPropertyCmd.ExecuteReader();

            var ddPropertyList = new List<Utl_Dynamic_Dropdown_Property>();

            while (ddPropertyReader.Read())
            {

                long propertyId = long.Parse(ddPropertyReader["PropertyId"].ToString());
                long columnId = long.Parse(ddPropertyReader["ColumnId"].ToString());
                long moduleId = long.Parse(ddPropertyReader["ModuleId"].ToString());
                long tableId = long.Parse(ddPropertyReader["TableId"].ToString());

                int propertyType = Convert.ToInt32(ddPropertyReader["PropertyType"].ToString());

                string tableName = ddPropertyReader["TableName"].ToString();
                string columnValue = ddPropertyReader["ColumnValue"].ToString();
                string columnText = ddPropertyReader["ColumnText"].ToString();
                string columnTextExp = ddPropertyReader["ColumnTextExp"].ToString();
                string whereExpression = ddPropertyReader["WhereExpression"].ToString();
                string orderByExpression = ddPropertyReader["OrderByExpression"].ToString();
                string sqlExpression = ddPropertyReader["SqlExpression"].ToString();


                bool isActive = bool.Parse(ddPropertyReader["IsActive"].ToString());
                string developerSynId = ddPropertyReader["DeveloperSyncId"].ToString();
                string rowId = ddPropertyReader["Row_Id"].ToString();


                long entryUserId = long.Parse(ddPropertyReader["Entry_User_Id"].ToString());
                string entryBy = ddPropertyReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(ddPropertyReader["Entry_Date"]);

                string updateBy = ddPropertyReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(ddPropertyReader["Update_Date"]);

                string checkBy = ddPropertyReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(ddPropertyReader["Check_Date"]);

                string approveBy = ddPropertyReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(ddPropertyReader["Approve_Date"]);

                string deleteBy = ddPropertyReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(ddPropertyReader["Delete_Date"]);

                string cancelBy = ddPropertyReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(ddPropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(ddPropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(ddPropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(ddPropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(ddPropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(ddPropertyReader["Work_Follow_Position"]);


                Utl_Dynamic_Dropdown_Property ddPropertyVM = new Utl_Dynamic_Dropdown_Property(propertyId, columnId, moduleId, tableId, propertyType, tableName, columnValue, columnText, columnTextExp, 
                    whereExpression, orderByExpression, sqlExpression, isActive, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, 
                    approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                ddPropertyList.Add(ddPropertyVM);
            }
            ddPropertyReader.Close();



            ////////////6. For Filter Property.
            string filterPropertyQuery = "SELECT [PropertyId],[TableId],ModuleId,[ColumnId],[OperatorId],[AndExpression],[OrExpression],[IsActive],[DeveloperSyncId],[Entry_User_Id],[Entry_By]," +
                "[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active],[Is_Deleted],"+
                "[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Filter_Property]";

            SqlCommand filterPropertyCmd = new SqlCommand(filterPropertyQuery, conn);
            SqlDataReader filterPropertyReader = filterPropertyCmd.ExecuteReader();

            var filterPropertyList = new List<Utl_Dynamic_Filter_Property>();

            while (filterPropertyReader.Read())
            {
                long propertyId = long.Parse(filterPropertyReader["PropertyId"].ToString());
                long tableId = long.Parse(filterPropertyReader["TableId"].ToString());
                long moduleId = long.Parse(filterPropertyReader["ModuleId"].ToString());
                long columnId = long.Parse(filterPropertyReader["ColumnId"].ToString());

                int operatorId = Convert.ToInt32(filterPropertyReader["OperatorId"].ToString());

                string andExpression = filterPropertyReader["AndExpression"].ToString();
                string OrExpression = filterPropertyReader["OrExpression"].ToString();

                bool isActive = bool.Parse(filterPropertyReader["IsActive"].ToString());
                string developerSynId =filterPropertyReader["DeveloperSyncId"].ToString();
                string rowId = filterPropertyReader["Row_Id"].ToString();

                long entryUserId = long.Parse(filterPropertyReader["Entry_User_Id"].ToString());
                string entryBy = filterPropertyReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(filterPropertyReader["Entry_Date"]);

                string updateBy = filterPropertyReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(filterPropertyReader["Update_Date"]);

                string checkBy = filterPropertyReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(filterPropertyReader["Check_Date"]);

                string approveBy = filterPropertyReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(filterPropertyReader["Approve_Date"]);

                string deleteBy = filterPropertyReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(filterPropertyReader["Delete_Date"]);

                string cancelBy = filterPropertyReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(filterPropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(filterPropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(filterPropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(filterPropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(filterPropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(filterPropertyReader["Work_Follow_Position"]);


                Utl_Dynamic_Filter_Property filterPropertyVM = new Utl_Dynamic_Filter_Property(propertyId, moduleId, tableId, columnId, operatorId, andExpression, OrExpression, isActive, developerSynId,
                    rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, 
                    isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                filterPropertyList.Add(filterPropertyVM);
            }
            filterPropertyReader.Close();


            ////////////7. For Validation Property.
            string validationPropertyQuery = "SELECT [PropertyId],[ColumnId],[TableId],ModuleId,[OperatorId],[ValueExpression],[ValueDataType],[ErrorMessage],[IsActive],[DeveloperSyncId],[Entry_User_Id]," +
                "[Entry_By],[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved] ,[Is_Active] ,[Is_Deleted],"+
                "[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Validation_Property]";

            SqlCommand validationPropertyCmd = new SqlCommand(validationPropertyQuery, conn);
            SqlDataReader validationPropertyReader = validationPropertyCmd.ExecuteReader();

            var validationPropertyList = new List<Utl_Dynamic_Validation_Property>();

            while (validationPropertyReader.Read())
            {
                long propertyId = long.Parse(validationPropertyReader["PropertyId"].ToString());
                long columnId = long.Parse(validationPropertyReader["ColumnId"].ToString());
                long tableId = long.Parse(validationPropertyReader["TableId"].ToString());
                long moduleId = long.Parse(validationPropertyReader["ModuleId"].ToString());
                int operatorId = Convert.ToInt32(validationPropertyReader["OperatorId"].ToString());

                string valueExpression = validationPropertyReader["ValueExpression"].ToString();
                string valueDataType = validationPropertyReader["ValueDataType"].ToString();
                string errorMessasge = validationPropertyReader["ErrorMessage"].ToString();

                bool isActive = bool.Parse(validationPropertyReader["IsActive"].ToString());
                string developerSynId =validationPropertyReader["DeveloperSyncId"].ToString();
                string rowId =validationPropertyReader["Row_Id"].ToString();

                long entryUserId = long.Parse(validationPropertyReader["Entry_User_Id"].ToString());
                string entryBy = validationPropertyReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(validationPropertyReader["Entry_Date"]);

                string updateBy = validationPropertyReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(validationPropertyReader["Update_Date"]);

                string checkBy = validationPropertyReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(validationPropertyReader["Check_Date"]);

                string approveBy = validationPropertyReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(validationPropertyReader["Approve_Date"]);

                string deleteBy = validationPropertyReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(validationPropertyReader["Delete_Date"]);

                string cancelBy = validationPropertyReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(validationPropertyReader["Cancel_Date"]);

                bool isApproved = bool.Parse(validationPropertyReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(validationPropertyReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(validationPropertyReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(validationPropertyReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(validationPropertyReader["Work_Follow_Position"]);



                Utl_Dynamic_Validation_Property validationPropertyVM = new Utl_Dynamic_Validation_Property(propertyId, columnId, tableId, moduleId, operatorId, valueExpression, valueDataType,
                    errorMessasge, isActive, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, 
                    deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                validationPropertyList.Add(validationPropertyVM);
            }
            validationPropertyReader.Close();




            ////////////8. For Table Function.
            string tableFunQuery = "SELECT [TableFunctionId],[TableId],ModuleId,[ColumnName],[AggregateFunction],[EvaluateValue],[EffectColumn],[IsDataShort],[IsDescending],[SLNo],[DeveloperSyncId]," +
                "[Entry_User_Id],[Entry_By],[Entry_Date],[Update_By],[Update_Date],[Check_By],[Check_Date],[Approve_By],[Approve_Date],[Delete_By],[Delete_Date],[Is_Approved],[Is_Active],"+
                "[Is_Deleted],[Work_Follow_Position],[Row_Id],[Cancel_By],[Cancel_Date],[Is_Cancel] FROM [dbo].[Utl_Dynamic_Table_Function]";

            SqlCommand tableFunCmd = new SqlCommand(tableFunQuery, conn);
            SqlDataReader tableFunReader = tableFunCmd.ExecuteReader();

            var tableFunList = new List<Utl_Dynamic_Table_Function>();

            while (tableFunReader.Read())
            {
                long tableFunctionId = long.Parse(tableFunReader["TableFunctionId"].ToString());
                long tableId = long.Parse(tableFunReader["TableId"].ToString());
                long moduleId = long.Parse(tableFunReader["ModuleId"].ToString());

                string columnName = tableFunReader["ColumnName"].ToString();
                string aggregateFunction = tableFunReader["AggregateFunction"].ToString();
                string evaluateValue = tableFunReader["EvaluateValue"].ToString();
                string effectColumn = tableFunReader["EffectColumn"].ToString();

                bool isDataShort = bool.Parse(tableFunReader["IsDataShort"].ToString());
                bool isDescending = bool.Parse(tableFunReader["IsDescending"].ToString());
                decimal slNo = decimal.Parse(tableFunReader["SLNo"].ToString());


                string developerSynId = tableFunReader["DeveloperSyncId"].ToString();
                string rowId = tableFunReader["Row_Id"].ToString();

                //tableFunctionId,tableId,columnName,aggregateFunction,evaluateValue,effectColumn,isDataShort,isDescending,slNo,developerSynId,rowId,

                long entryUserId = long.Parse(tableFunReader["Entry_User_Id"].ToString());
                string entryBy = tableFunReader["Entry_By"].ToString();
                DateTime entryDate = Convert.ToDateTime(tableFunReader["Entry_Date"]);

                string updateBy = tableFunReader["Update_By"].ToString();
                DateTime updateDate = Convert.ToDateTime(tableFunReader["Update_Date"]);

                string checkBy = tableFunReader["Check_By"].ToString();
                DateTime checkDate = Convert.ToDateTime(tableFunReader["Check_Date"]);

                string approveBy = tableFunReader["Approve_By"].ToString();
                DateTime approveDate = Convert.ToDateTime(tableFunReader["Approve_Date"]);

                string deleteBy = tableFunReader["Delete_By"].ToString();
                DateTime deleteDate = Convert.ToDateTime(tableFunReader["Delete_Date"]);

                string cancelBy = tableFunReader["Cancel_By"].ToString();
                DateTime cancelDate = Convert.ToDateTime(tableFunReader["Cancel_Date"]);

                bool isApproved = bool.Parse(tableFunReader["Is_Approved"].ToString());
                bool is_Active = bool.Parse(tableFunReader["Is_Active"].ToString());
                bool isDelete = bool.Parse(tableFunReader["Is_Deleted"].ToString());
                bool isCandel = bool.Parse(tableFunReader["Is_Cancel"].ToString());
                int workFollowPosition = Convert.ToInt32(tableFunReader["Work_Follow_Position"]);


                Utl_Dynamic_Table_Function tableFunVM = new Utl_Dynamic_Table_Function(tableFunctionId, tableId, moduleId, columnName,  aggregateFunction, evaluateValue, effectColumn, isDataShort, 
                    isDescending, slNo, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                    deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                tableFunList.Add(tableFunVM);
            }
            tableFunReader.Close();
            conn.Close();


            JsonTableList jsonTableModel = new JsonTableList();
            List<JsonTableList> jsonTableLists = new List<JsonTableList>();

            jsonTableModel.Utl_Dynamic_Module = ModuleList;
            jsonTableModel.Utl_Dynamic_Table = TableList;
            jsonTableModel.Utl_Dynamic_Table_Column = TableColumnList;
            jsonTableModel.Utl_Dynamic_Autocode_Property = autocodePropertyList;
            jsonTableModel.Utl_Dynamic_Dropdown_Property = ddPropertyList;
            jsonTableModel.Utl_Dynamic_Filter_Property = filterPropertyList;
            jsonTableModel.Utl_Dynamic_Validation_Property = validationPropertyList;
            jsonTableModel.Utl_Dynamic_Table_Function = tableFunList;

            jsonTableLists.Add(jsonTableModel);


            string jsonDatapath = "~/JsonDataStore/";
            JsonDataConvater(jsonDatapath, jsonTableLists);


            return View("FirstPage");
        }

        public void JsonDataConvater<T>(string path, List<T> modelList)
        {
            string AllTableList = new JavaScriptSerializer().Serialize(modelList);
            string fulpath = Server.MapPath(path);
            //string path = Server.MapPath("~/App_Data/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText(fulpath + "AllTableJsonData.json", AllTableList);
        }






        /// <summary>
        /// .................................Get json Data.....................
        /// </summary>
        public ActionResult JsonfileRead()
        {
           

            // Json file Data read and store ViewModel, list.
            StreamReader streamReader = new StreamReader(Server.MapPath("~/JsonDataStore/EightTableData.json"));
            string data = streamReader.ReadToEnd();
            List<JsonTableList> jsonTableLists = JsonConvert.DeserializeObject<List<JsonTableList>>(data);


            List<Utl_Dynamic_Module> utl_Dynamic_Module = jsonTableLists.Select(s => s.Utl_Dynamic_Module).FirstOrDefault();
            var sd = utl_Dynamic_Module.Select(s => s.Approve_Date).First();




            List<Utl_Dynamic_Module> ModuleList = new List<Utl_Dynamic_Module>();
            List<Utl_Dynamic_Table> TableList = new List<Utl_Dynamic_Table>();
            List<Utl_Dynamic_Table_Column> TableColumnList = new List<Utl_Dynamic_Table_Column>();
            List<Utl_Dynamic_Autocode_Property> autocodePropertieList = new List<Utl_Dynamic_Autocode_Property>();
            List<Utl_Dynamic_Dropdown_Property> dropdownProList = new List<Utl_Dynamic_Dropdown_Property>();
            List<Utl_Dynamic_Filter_Property> filterPropertiesList = new List<Utl_Dynamic_Filter_Property>();
            List<Utl_Dynamic_Validation_Property> validationPropertiesList = new List<Utl_Dynamic_Validation_Property>();
            List<Utl_Dynamic_Table_Function> tableFunctionsList = new List<Utl_Dynamic_Table_Function>();

            foreach (var item in jsonTableLists)
            {
                ////Utl_Dynamic_Module
                foreach (var module in item.Utl_Dynamic_Module)
                {
                    long moduleId = module.ModuleId;
                    int applicationId = (int)module.ApplicationId;

                    long ExiModulId = (long)module.ExistingModuleId;
                    bool isNewCopy = (bool)module.IsNewCopy;

                    string moduleName = module.ModuleName;
                    string moduleCaption = module.ModuleCaption;
                    string reportTitle = module.ReportTitle;
                    bool isDrillDownReport = (bool)module.IsDrilldownReport;

                    int categoryId = (int)module.CategoryId;
                    string imagePath = module.ImagePath;
                    int moduleType = module.ModuleType;
                    decimal siNo = (decimal)module.SlNo;

                    bool isActive = (bool)module.IsActive;
                    string developerSyId = module.DeveloperSyncId;
                    string rowId = module.Row_Id;

                    long entryUserId = (long)module.Entry_User_Id;
                    string entryBy = module.Entry_By;
                    DateTime entryDate = (DateTime)module.Entry_Date;

                    string updateBy = module.Update_By;
                    DateTime updateDate = (DateTime)module.Update_Date;

                    string checkBy = module.Check_By;
                    DateTime checkDate = (DateTime)module.Check_Date;

                    string approveBy = module.Approve_By;
                    DateTime approveDate = (DateTime)module.Approve_Date;

                    string deleteBy = module.Delete_By;
                    DateTime deleteDate = (DateTime)module.Delete_Date;

                    string cancelBy = module.Cancel_By;
                    DateTime cancelDate = (DateTime)module.Cancel_Date;

                    bool isApproved = (bool)module.Is_Approved;
                    bool is_Active = (bool)module.Is_Active;
                    bool isDelete = (bool)module.Is_Deleted;
                    bool isCandel = (bool)module.Is_Cancel;
                    int workFollowPosition = module.Work_Follow_Position;


                    Utl_Dynamic_Module moduleVM = new Utl_Dynamic_Module(moduleId, applicationId, ExiModulId, isNewCopy, moduleName, moduleCaption, reportTitle, isDrillDownReport, categoryId,
                        imagePath, moduleType, siNo, isActive, developerSyId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                        deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                    
                    ModuleList.Add(moduleVM);
                }

                ///Utl_Dynamic_Table
                foreach (var table in item.Utl_Dynamic_Table)
                {
                    long tableId = (long)table.TableId;
                    long moduleId = (long)table.ModuleId;
                    long applicationId = (long)table.ApplicationId;

                    long? parentModuleId = table.ParentModuleId;
                    long? foreignModuleId = table.ForeignModuleId;

                    string tableCaption = table.TableCaption;
                    string moduleTblCaption = table.ModuleTableCaption;
                    string tableName = table.TableName;
                    string refreshType = table.RefreshType;

                    long parentTableId = (long)table.ParentTableId;
                    long foreignTableId = (long)table.ForeignTableId;

                    bool isExternalDataShow = (bool)table.IsExternalDataShow;
                    string externalDataCaption = table.ExternalDataCaption;
                    string externalDataFilter = table.ExternalDataFilter;

                    long existingTableId = (long)table.ExistingTableId;
                    int moduleWidth = (int)table.ModuleWidth;
                    int moduleHeight = (int)table.ModuleHeight;
                    int formWidth = (int)table.FormWidth;

                    string positionExp = table.PositionExp;
                    int rowPosition = (int)table.RowPosition;
                    int colPosition = (int)table.ColPosition;

                    bool isReadonly = (bool)table.IsReadOnly;
                    bool isWorkFollow = (bool)table.IsWorkFollow;
                    bool isAutoToggle = (bool)table.IsAutoToggle;

                    string procedureName = table.ProcedureName;
                    string procedureCaption = table.ProcedureCaption;
                    string procedureValidation = table.ProcedureValidation;

                    bool proFireOntrans = (bool)table.ProcFireOnTrans;
                    string reportProcName = table.ReportProcedureName;
                    bool rptProcForWorksheet = (bool)table.RptProcForWorksheet;

                    string procFooterView = table.ProcFooterView;
                    string procFooterDml = table.ProcFooterDml;
                    string rptFileName = table.ReportFileName;

                    bool showOnReport = (bool)table.ShowOnReport;
                    string sqlStatement = table.SqlStatement;

                    string objectType = table.ObjectType;
                    string templateType = table.TemplateType;

                    decimal slNo = (decimal)table.SlNo;
                    string developerSynId = table.DeveloperSyncId;
                    string rowId = table.Row_Id;


                    long entryUserId = (long)table.Entry_User_Id;
                    string entryBy = table.Entry_By;
                    DateTime entryDate = (DateTime)table.Entry_Date;

                    string updateBy = table.Update_By;
                    DateTime updateDate = (DateTime)table.Update_Date;

                    string checkBy = table.Check_By;
                    DateTime checkDate = (DateTime)table.Check_Date;

                    string approveBy = table.Approve_By;
                    DateTime approveDate = (DateTime)table.Approve_Date;

                    string deleteBy = table.Delete_By;
                    DateTime deleteDate = (DateTime)table.Delete_Date;

                    string cancelBy = table.Cancel_By;
                    DateTime cancelDate = (DateTime)table.Cancel_Date;

                    bool isApproved = (bool)table.Is_Approved;
                    bool is_Active = (bool)table.Is_Active;
                    bool isDelete = (bool)table.Is_Deleted;
                    bool isCandel = (bool)table.Is_Cancel;
                    int workFollowPosition = table.Work_Follow_Position;

                    Utl_Dynamic_Table TableVM = new Utl_Dynamic_Table(tableId, moduleId, parentModuleId, foreignModuleId, applicationId, tableCaption, moduleTblCaption, tableName, refreshType, parentTableId, foreignTableId,
                        isExternalDataShow, externalDataCaption, externalDataFilter, existingTableId, moduleWidth, moduleHeight, formWidth, positionExp, rowPosition, colPosition, isReadonly,
                        isWorkFollow, isAutoToggle, procedureName, procedureCaption, procedureValidation, proFireOntrans, reportProcName, rptProcForWorksheet, procFooterView, procFooterDml,
                        rptFileName, showOnReport, sqlStatement, objectType, templateType, slNo, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy,
                        checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                    TableList.Add(TableVM);
                }

                /// Utl_Dynamic_Table_Column
                foreach (var tblClm in item.Utl_Dynamic_Table_Column)
                {

                    long columnId = tblClm.ColumnId;
                    long tableId = (long)tblClm.TableId;
                    long moduleId = (long)tblClm.ModuleId;
                    long lovModuleId = (long)tblClm.LovModuleId ;

                    string columnName = tblClm.ColumnName;
                    string selectColName = tblClm.SelectColumnName;
                    string colDisplayName = tblClm.ColumnDisplayName;
                    string colProperty = tblClm.ColumnProperty;
                    string controlType = tblClm.ControlType;
                    string parentcolumn = tblClm.ParentColumn;

                    long foreignKeyName = (long)tblClm.ForeignKeyName;
                    string foreignColName = tblClm.ForeignColumnName;
                    string foreignDataFilterBy = tblClm.ForeignDataFilterBy;

                    int columnWidth = (int)tblClm.ColumnWidth;
                    int dbColumnWidth = (int)tblClm.DbColumnWidth;
                    int transPolicyId = tblClm.TransPolicyId;
                    int globalParamId = (int)tblClm.GlobalParamId;
                    int globalParamValue = (int)tblClm.GlobalParamValue;

                    string defaultValue = tblClm.DefaultValue;
                    int filterBy = (int)tblClm.FilterBy;
                    int userPrivilege = (int)tblClm.User_Privilege;

                    bool isModuleSelect = (bool)tblClm.IsModuleSelect;
                    bool isFvSelect = (bool)tblClm.IsFvSelect;
                    bool isGvSelect = (bool)tblClm.IsGvSelect;
                    bool isForeignTable = (bool)tblClm.IsForeignTable;
                    bool isDefaultFocus = (bool)tblClm.IsDefaultFocus;

                    bool isFvUpdate = tblClm.IsFvUpdate;
                    bool isFvInsert = tblClm.IsFvInsert;
                    bool isFvDataFixed = (bool)tblClm.IsFvDataFixed;
                    bool isFvForceEdit = (bool)tblClm.IsFvForceEdit;



                    int dblDataSource = (int)tblClm.DdlDataSource;
                    long lovTableId = (long)tblClm.LovTableId;

                    string tableName = tblClm.TableName;
                    string columnValue = tblClm.ColumnValue;
                    string columnText = tblClm.ColumnText;
                    string columnTextExp = tblClm.ColumnTextExp;
                    string WhereExpression = tblClm.WhereExpression;
                    string orderByExpression = tblClm.OrderByExpression;
                    string sqlExpression = tblClm.SqlExpression;

                    string linkType = tblClm.LinkType;

                    int linkPageId = (int)tblClm.LinkPageId;
                    int linkTableId = (int)tblClm.LinkTableId;
                    string linkColumnName = tblClm.LinkColumnName;



                    string linkUrl = tblClm.LinkUrl;
                    bool isShow = (bool)tblClm.IsShow;
                    int reportColumn = (int)tblClm.ReportColumn;

                    decimal slNo = (decimal)tblClm.SlNo;
                    string DeveloperSynId = tblClm.DeveloperSyncId;
                    string rowId = tblClm.Row_Id;


                    long entryUserId = (long)tblClm.Entry_User_Id;
                    string entryBy = tblClm.Entry_By;
                    DateTime entryDate = (DateTime)tblClm.Entry_Date;

                    string updateBy = tblClm.Update_By;
                    DateTime updateDate = (DateTime)tblClm.Update_Date;

                    string checkBy = tblClm.Check_By;
                    DateTime checkDate = (DateTime)tblClm.Check_Date;

                    string approveBy = tblClm.Approve_By;
                    DateTime approveDate = (DateTime)tblClm.Approve_Date;

                    string deleteBy = tblClm.Delete_By;
                    DateTime deleteDate = (DateTime)tblClm.Delete_Date;

                    string cancelBy = tblClm.Cancel_By;
                    DateTime cancelDate = (DateTime)tblClm.Cancel_Date;

                    bool isApproved = (bool)tblClm.Is_Approved;
                    bool is_Active = (bool)tblClm.Is_Active;
                    bool isDelete = (bool)tblClm.Is_Deleted;
                    bool isCandel = (bool)tblClm.Is_Cancel;
                    int workFollowPosition = tblClm.Work_Follow_Position;

                    Utl_Dynamic_Table_Column tableColumnVM = new Utl_Dynamic_Table_Column(columnId, tableId, moduleId, lovModuleId, columnName, selectColName, colDisplayName, colProperty, controlType, parentcolumn, foreignKeyName,
                        foreignColName, foreignDataFilterBy, columnWidth, dbColumnWidth, transPolicyId, globalParamId, globalParamValue, defaultValue, filterBy, userPrivilege, isModuleSelect, isFvSelect,
                        isGvSelect, isForeignTable, isDefaultFocus, isFvUpdate, isFvInsert, isFvDataFixed, isFvForceEdit, dblDataSource, lovTableId, tableName, columnValue, columnText, columnTextExp,
                        WhereExpression, orderByExpression, sqlExpression, linkType, linkPageId, linkTableId, linkColumnName, linkUrl, isShow, reportColumn, slNo, DeveloperSynId, rowId, entryUserId,
                        entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel,
                        workFollowPosition);



                    TableColumnList.Add(tableColumnVM);
                }


                ///////Utl_Dynamic_Autocode_Property
                foreach (var autoCdProperty in item.Utl_Dynamic_Autocode_Property)
                {

                    long propertyId = (long)autoCdProperty.PropertyId;
                    long columnId = (long)autoCdProperty.ColumnId;
                    long moduleId = autoCdProperty.ModuleId;
                    long tableId = autoCdProperty.TableId;

                    string formatType = autoCdProperty.FormatType;
                    string format = autoCdProperty.Format;
                    string expression = autoCdProperty.Expression;
                    string columnName = autoCdProperty.ColumnName;

                    int slNo = autoCdProperty.SlNo;
                    bool isActive = autoCdProperty.IsActive;
                    string developerSynId = autoCdProperty.DeveloperSyncId;
                    string rowId = autoCdProperty.Row_Id;

                    long entryUserId = (long)autoCdProperty.Entry_User_Id;
                    string entryBy = autoCdProperty.Entry_By;
                    DateTime entryDate = (DateTime)autoCdProperty.Entry_Date;

                    string updateBy = autoCdProperty.Update_By;
                    DateTime updateDate = (DateTime)autoCdProperty.Update_Date;

                    string checkBy = autoCdProperty.Check_By;
                    DateTime checkDate = (DateTime)autoCdProperty.Check_Date;

                    string approveBy = autoCdProperty.Approve_By;
                    DateTime approveDate = (DateTime)autoCdProperty.Approve_Date;

                    string deleteBy = autoCdProperty.Delete_By;
                    DateTime deleteDate = (DateTime)autoCdProperty.Delete_Date;

                    string cancelBy = autoCdProperty.Cancel_By;
                    DateTime cancelDate = (DateTime)autoCdProperty.Cancel_Date;

                    bool isApproved = (bool)autoCdProperty.Is_Approved;
                    bool is_Active = (bool)autoCdProperty.Is_Active;
                    bool isDelete = (bool)autoCdProperty.Is_Deleted;
                    bool isCandel = (bool)autoCdProperty.Is_Cancel;
                    int workFollowPosition = autoCdProperty.Work_Follow_Position;


                    Utl_Dynamic_Autocode_Property autocodePropertyVM = new Utl_Dynamic_Autocode_Property(propertyId, moduleId, tableId, columnId, formatType, format, expression, columnName, slNo, isActive,
                        developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy,
                        cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                    autocodePropertieList.Add(autocodePropertyVM);
                }


                //////////..........Utl_Dynamic_Dropdown_Property..............
                //
                foreach (var dropdownPro in item.Utl_Dynamic_Dropdown_Property)
                {

                    long propertyId = (long)dropdownPro.PropertyId;
                    long columnId = (long)dropdownPro.ColumnId;
                    long moduleId = dropdownPro.ModuleId;
                    long tableId = dropdownPro.TableId;

                    int propertyType = dropdownPro.PropertyType;

                    string tableName = dropdownPro.TableName;
                    string columnValue = dropdownPro.ColumnValue;
                    string columnText = dropdownPro.ColumnText;
                    string columnTextExp = dropdownPro.ColumnTextExp;
                    string whereExpression = dropdownPro.WhereExpression;
                    string orderByExpression = dropdownPro.OrderByExpression;
                    string sqlExpression = dropdownPro.SqlExpression;


                    bool isActive = (bool)dropdownPro.IsActive;
                    string developerSynId = dropdownPro.DeveloperSyncId;
                    string rowId = dropdownPro.Row_Id;


                    long entryUserId = (long)dropdownPro.Entry_User_Id;
                    string entryBy = dropdownPro.Entry_By;
                    DateTime entryDate = (DateTime)dropdownPro.Entry_Date;

                    string updateBy = dropdownPro.Update_By;
                    DateTime updateDate = (DateTime)dropdownPro.Update_Date;

                    string checkBy = dropdownPro.Check_By;
                    DateTime checkDate = (DateTime)dropdownPro.Check_Date;

                    string approveBy = dropdownPro.Approve_By;
                    DateTime approveDate = (DateTime)dropdownPro.Approve_Date;

                    string deleteBy = dropdownPro.Delete_By;
                    DateTime deleteDate = (DateTime)dropdownPro.Delete_Date;

                    string cancelBy = dropdownPro.Cancel_By;
                    DateTime cancelDate = (DateTime)dropdownPro.Cancel_Date;

                    bool isApproved = (bool)dropdownPro.Is_Approved;
                    bool is_Active = (bool)dropdownPro.Is_Active;
                    bool isDelete = (bool)dropdownPro.Is_Deleted;
                    bool isCandel = (bool)dropdownPro.Is_Cancel;
                    int workFollowPosition = dropdownPro.Work_Follow_Position;


                    Utl_Dynamic_Dropdown_Property dropdownProVM = new Utl_Dynamic_Dropdown_Property(propertyId, columnId, moduleId, tableId, propertyType, tableName, columnValue, columnText, columnTextExp,
                        whereExpression, orderByExpression, sqlExpression, isActive, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate,
                        approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                    dropdownProList.Add(dropdownProVM);
                }


                //////////..........Utl_Dynamic_Filter_Property..............
                //
                foreach (var filterProperty in item.Utl_Dynamic_Filter_Property)
                {

                    long propertyId = (long)filterProperty.PropertyId;
                    long tableId = (long)filterProperty.TableId;
                    long moduleId = filterProperty.ModuleId;
                    long columnId = (long)filterProperty.ColumnId;

                    int operatorId = filterProperty.OperatorId;

                    string andExpression = filterProperty.AndExpression;
                    string OrExpression = filterProperty.OrExpression;

                    bool isActive = (bool)filterProperty.IsActive;
                    string developerSynId = filterProperty.DeveloperSyncId;
                    string rowId = filterProperty.Row_Id;

                    long entryUserId = (long)filterProperty.Entry_User_Id;
                    string entryBy = filterProperty.Entry_By;
                    DateTime entryDate = (DateTime)filterProperty.Entry_Date;

                    string updateBy = filterProperty.Update_By;
                    DateTime updateDate = (DateTime)filterProperty.Update_Date;

                    string checkBy = filterProperty.Check_By;
                    DateTime checkDate = (DateTime)filterProperty.Check_Date;

                    string approveBy = filterProperty.Approve_By;
                    DateTime approveDate = (DateTime)filterProperty.Approve_Date;

                    string deleteBy = filterProperty.Delete_By;
                    DateTime deleteDate = (DateTime)filterProperty.Delete_Date;

                    string cancelBy = filterProperty.Cancel_By;
                    DateTime cancelDate = (DateTime)filterProperty.Cancel_Date;

                    bool isApproved = (bool)filterProperty.Is_Approved;
                    bool is_Active = (bool)filterProperty.Is_Active;
                    bool isDelete = (bool)filterProperty.Is_Deleted;
                    bool isCandel = (bool)filterProperty.Is_Cancel;
                    int workFollowPosition = filterProperty.Work_Follow_Position;


                    Utl_Dynamic_Filter_Property filterPropertyVM = new Utl_Dynamic_Filter_Property(propertyId, moduleId, tableId, columnId, operatorId, andExpression, OrExpression, isActive, developerSynId,
                        rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy, deleteDate, cancelBy, cancelDate,
                        isApproved, is_Active, isDelete, isCandel, workFollowPosition);


                    filterPropertiesList.Add(filterPropertyVM);

                }


                //////////..........Utl_Dynamic_Validation_Property..............
                //
                foreach (var validtionPro in item.Utl_Dynamic_Validation_Property)
                {
                    long propertyId = (long)validtionPro.PropertyId;
                    long columnId = (long)validtionPro.ColumnId;
                    long tableId = (long)validtionPro.TableId;
                    long moduleId = (long)validtionPro.ModuleId;
                    int operatorId = validtionPro.OperatorId;

                    string valueExpression = validtionPro.ValueExpression;
                    string valueDataType = validtionPro.ValueDataType;
                    string errorMessasge = validtionPro.ErrorMessage;

                    bool isActive = (bool)validtionPro.IsActive;
                    string developerSynId = validtionPro.DeveloperSyncId;
                    string rowId = validtionPro.Row_Id;

                    long entryUserId = (long)validtionPro.Entry_User_Id;
                    string entryBy = validtionPro.Entry_By;
                    DateTime entryDate = (DateTime)validtionPro.Entry_Date;

                    string updateBy = validtionPro.Update_By;
                    DateTime updateDate = (DateTime)validtionPro.Update_Date;

                    string checkBy = validtionPro.Check_By;
                    DateTime checkDate = (DateTime)validtionPro.Check_Date;

                    string approveBy = validtionPro.Approve_By;
                    DateTime approveDate = (DateTime)validtionPro.Approve_Date;

                    string deleteBy = validtionPro.Delete_By;
                    DateTime deleteDate = (DateTime)validtionPro.Delete_Date;

                    string cancelBy = validtionPro.Cancel_By;
                    DateTime cancelDate = (DateTime)validtionPro.Cancel_Date;

                    bool isApproved = (bool)validtionPro.Is_Approved;
                    bool is_Active = (bool)validtionPro.Is_Active;
                    bool isDelete = (bool)validtionPro.Is_Deleted;
                    bool isCandel = (bool)validtionPro.Is_Cancel;
                    int workFollowPosition = validtionPro.Work_Follow_Position;

                    Utl_Dynamic_Validation_Property validationPropertyVM = new Utl_Dynamic_Validation_Property(propertyId, columnId, tableId, moduleId, operatorId, valueExpression, valueDataType,
                        errorMessasge, isActive, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                        deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);


                    validationPropertiesList.Add(validationPropertyVM);
                }


                //////////..........Utl_Dynamic_Validation_Property..............
                //
                foreach (var tableFunction in item.Utl_Dynamic_Table_Function)
                {
                    long tableFunctionId = (long)tableFunction.TableFunctionId;
                    long tableId = (long)tableFunction.TableId;
                    long moduleId = tableFunction.ModuleId;

                    string columnName = tableFunction.ColumnName;
                    string aggregateFunction = tableFunction.AggregateFunction;
                    string evaluateValue = tableFunction.EvaluateValue;
                    string effectColumn = tableFunction.EffectColumn;

                    bool isDataShort = (bool)tableFunction.IsDataShort;
                    bool isDescending = (bool)tableFunction.IsDescending;
                    decimal slNo = (decimal)tableFunction.SLNo;


                    string developerSynId = tableFunction.DeveloperSyncId;
                    string rowId = tableFunction.Row_Id;

                    long entryUserId = (long)tableFunction.Entry_User_Id;
                    string entryBy = tableFunction.Entry_By;
                    DateTime entryDate = (DateTime)tableFunction.Entry_Date;

                    string updateBy = tableFunction.Update_By;
                    DateTime updateDate = (DateTime)tableFunction.Update_Date;

                    string checkBy = tableFunction.Check_By;
                    DateTime checkDate = (DateTime)tableFunction.Check_Date;

                    string approveBy = tableFunction.Approve_By;
                    DateTime approveDate = (DateTime)tableFunction.Approve_Date;

                    string deleteBy = tableFunction.Delete_By;
                    DateTime deleteDate = (DateTime)tableFunction.Delete_Date;

                    string cancelBy = tableFunction.Cancel_By;
                    DateTime cancelDate = (DateTime)tableFunction.Cancel_Date;

                    bool isApproved = (bool)tableFunction.Is_Approved;
                    bool is_Active = (bool)tableFunction.Is_Active;
                    bool isDelete = (bool)tableFunction.Is_Deleted;
                    bool isCandel = (bool)tableFunction.Is_Cancel;
                    int workFollowPosition = tableFunction.Work_Follow_Position;


                    Utl_Dynamic_Table_Function tableFunVM = new Utl_Dynamic_Table_Function(tableFunctionId, tableId, moduleId, columnName, aggregateFunction, evaluateValue, effectColumn, isDataShort,
                        isDescending, slNo, developerSynId, rowId, entryUserId, entryBy, entryDate, updateBy, updateDate, checkBy, checkDate, approveBy, approveDate, deleteBy,
                        deleteDate, cancelBy, cancelDate, isApproved, is_Active, isDelete, isCandel, workFollowPosition);

                    tableFunctionsList.Add(tableFunVM);
                }
            }





            ViewBag.moduleData = ModuleList;
            ViewBag.tableData =TableList;
            ViewBag.tableColumnData = TableColumnList;
            ViewBag.AutocodeProperty = autocodePropertieList;
            ViewBag.dropdownProperty = dropdownProList;
            ViewBag.filterProperty = filterPropertiesList;
            ViewBag.validationProperty = validationPropertiesList;
            ViewBag.tableFunction = tableFunctionsList;


            return View();
        }
    }
}