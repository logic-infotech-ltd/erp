﻿using EnterpriceResourcePlaning_ERP_.BaseRepository;
using EnterpriceResourcePlaning_ERP_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Repository
{
    public class Repo_GetDynamicTable
    {

        public string Get_Dynamic_Table_Info(int ModuleId, int TableId, string PkId, string FkId, string DmlType /*, string pathAllModule*/)
        {

            JsonDataWriteAndRead jsonWriteRead = new JsonDataWriteAndRead();
            var jsonTableLists = jsonWriteRead.jsonFileRead(ModuleId);

            List<Utl_Dynamic_Table> json_Dynamic_Tables = jsonTableLists.Utl_Dynamic_Table;
            List<Utl_Dynamic_Table_Column> json_Dynamic_Table_Columns = jsonTableLists.Utl_Dynamic_Table_Column;
            List<Utl_Dynamic_Dropdown_Property> Dropdown_Property = jsonTableLists.Utl_Dynamic_Dropdown_Property;
            List<Utl_Dynamic_Table_Function> json_Dynamic_Table_Functions = jsonTableLists.Utl_Dynamic_Table_Function;

            

            string TableName = json_Dynamic_Tables.Where(s => s.TableId == TableId).Select(t => t.TableName).FirstOrDefault();
            string PK_NAME = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && t.ColumnProperty == "pk").Select(s => s.ColumnName).FirstOrDefault();
            string FK_NAME = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && t.ColumnProperty == "Fk").Select(s => s.ColumnName).FirstOrDefault();

            Base_GetDynamicTable base_GetDynamicTable = new Base_GetDynamicTable();
            //List<CommonModel> DmlSelectM = new List<CommonModel>();

            List<Mdl_TempColumnName> tempColumnName = new List<Mdl_TempColumnName>();


            var tmpFunction = json_Dynamic_Table_Functions.Where(f => f.TableId == TableId && (f.EffectColumn == null ? "" : f.EffectColumn) != "" && (f.EvaluateValue == null ? "" : f.EvaluateValue) != "").Distinct();


            string DmlSIU = "";
            string FKValue = "";

            if (FkId != "" && FkId != "0")
            {

                FKValue = base_GetDynamicTable.GetFKValue(jsonTableLists, TableId, FkId);
            }


            if (DmlType == "select")
            {
                IEnumerable<Utl_Dynamic_Table_Column> tableColumn = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && t.IsFvSelect == true && t.IsShow == true && t.TransPolicyId == 0).OrderBy(o => o.SlNo);

                DmlSIU = base_GetDynamicTable.GetDmlSelect(tableColumn);
            }
            else if (DmlType == "update" || DmlType == "force_update")
            {

                DmlSIU = base_GetDynamicTable.GetDmlUpdate(TableId, DmlType, json_Dynamic_Table_Columns, tmpFunction);
            }
            else if (DmlType == "insert")
            {
                DmlSIU = base_GetDynamicTable.GetDmlInsert(TableId, json_Dynamic_Table_Columns, tmpFunction);
            }
            else if (DmlType == "binddata_fv_select")
            {
                IEnumerable<Utl_Dynamic_Table_Column> DynamicTblCol = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && t.IsFvSelect == true && t.IsShow == true).OrderBy(s => s.SlNo);

                tempColumnName = base_GetDynamicTable.GetDmlBinddataSelect(DynamicTblCol, FKValue);
            }
            else if (DmlType == "binddata_fv_update" || DmlType == "binddata_fv_force_update")
            {
                IEnumerable<Utl_Dynamic_Table_Column> DynamicTblCol = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && t.IsShow == true).OrderBy(s => s.SlNo);

                tempColumnName = base_GetDynamicTable.GetDmlBinddataUpdate(DmlType, DynamicTblCol, FKValue);
            }
            else if (DmlType == "binddata_fv_insert")
            {
                IEnumerable<Utl_Dynamic_Table_Column> DynamicTblCol = json_Dynamic_Table_Columns.Where(s => s.TableId == TableId && s.IsFvInsert == true && s.IsShow == true).OrderBy(s => s.SlNo);

                tempColumnName = base_GetDynamicTable.GetDmlBinddataInsert(DynamicTblCol, FKValue);
            }

            string SqlQuery = "";

            if (DmlType == "binddata_fv_select" || DmlType == "binddata_fv_update" || DmlType == "binddata_fv_force_update" || DmlType == "binddata_fv_insert")
            {

                string ForeignTableName = "";
                string ForeignTableScript = null;

                

                var DynTable = json_Dynamic_Tables.Where(s => s.TableId == TableId).Select(s => new { s.ForeignTableId, s.ParentModuleId }).FirstOrDefault();

                long? foreignTableId = DynTable.ForeignTableId;
                long? parenteModuleId = (DynTable.ParentModuleId == null) ? 0 : DynTable.ParentModuleId;

                if (parenteModuleId != null && parenteModuleId != 0)
                {
                    JsonDataWriteAndRead JsonRead = new JsonDataWriteAndRead();
                    JsonTableList ModuleList = JsonRead.jsonFileRead(parenteModuleId);

                    List<Utl_Dynamic_Table> ModuleBaseTable = ModuleList.Utl_Dynamic_Table;
                    ForeignTableName = ModuleBaseTable.Where(s => s.TableId == foreignTableId).Select(s => s.TableName).FirstOrDefault();
                }
                
                var colAndForeignColName = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId &&  t.ForeignColumnName != ""
                && (t.ColumnProperty == "fdk" || t.ColumnProperty == "pk")).Select(s => new { s.ColumnName, s.ForeignColumnName }).FirstOrDefault();

                if(colAndForeignColName != null)
                {
                    ForeignTableScript = "LEFT OUTER JOIN " + ForeignTableName + " ft ON t." + colAndForeignColName.ColumnName + "=ft." + colAndForeignColName.ForeignColumnName;
                }
                else
                {
                    ForeignTableScript = "";
                }
                //ForeignTableScript = ForeignTableScript == null ? "" : ForeignTableScript;
                //string SqlQueryString = "";

                 SqlQuery = base_GetDynamicTable.DmlInsertUpdateDelete(TableId, TableName, PK_NAME, FK_NAME, PkId, FkId, DmlType, ForeignTableScript, tempColumnName, Dropdown_Property);

            }


            string UpdateSql = "";
            string InsertSql = "";
            string validatedParam = "";
            if (DmlType == "cmd_fv_update" || DmlType == "cmd_fv_force_update")
            {

                IEnumerable<Utl_Dynamic_Table_Column> filterTableColumn = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && (t.ControlType != "dfl" || t.ControlType != "dft" || t.ControlType != "dfd")
                && t.IsShow == true).OrderBy(s => s.SlNo);
                
                UpdateSql = base_GetDynamicTable.cmdFvUpdate(TableName, PK_NAME, PkId, DmlType, filterTableColumn);

            }
            else if (DmlType == "cmd_fv_insert")
            {
                
                IEnumerable<string> ColNames = json_Dynamic_Table_Columns.Where(t => t.TableId == TableId && t.IsFvInsert == true && t.IsShow == true
                && (t.ControlType != "dfl" || t.ControlType != "dft" || t.ControlType != "dfd")).OrderBy(o => o.SlNo).Select(s => s.ColumnName);

                InsertSql = base_GetDynamicTable.cmdFvInsert(TableName, PK_NAME,ColNames);

            }
            else if (DmlType == "cmd_fv_validate_param")
            {
                string validate_proc =  json_Dynamic_Tables.Where(t => t.TableId == TableId).Select(t => t.ProcedureValidation).FirstOrDefault();
                validate_proc = validate_proc == null ? "" : validate_proc;
                if (validate_proc != "")
                {
                    validatedParam = base_GetDynamicTable.SplitData(validate_proc, ":", 2); /*(select dbo.fn_SplitData(@validate_proc, ':', 2))*/
                }
            }




            if (SqlQuery != "")
            {
                return SqlQuery;
            }
            else if(InsertSql !="")
            {
                return InsertSql;
            }
            else if (UpdateSql != "")
            {
                return UpdateSql;
            }
            else if (DmlSIU != "")
            {
                return DmlSIU;
            }
            else if (validatedParam != "")
            {
                return validatedParam;
            }
            else
            {
                return "No Data Found...";
            }
            
            
        }
    }
}