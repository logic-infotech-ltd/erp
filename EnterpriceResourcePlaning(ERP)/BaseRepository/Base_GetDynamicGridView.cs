﻿using EnterpriceResourcePlaning_ERP_.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.BaseRepository
{
    public class Base_GetDynamicGridView
    {
        List<ColumnNameForGridView> GridViewModel = new List<ColumnNameForGridView>();
        public List<ColumnNameForGridView> DisplayOnParent(IEnumerable<Utl_Dynamic_Table_Column> dynTableColumn, string FK_VALUE)
        {
            // CASE WHEN @FK_VALUE<>'' AND ColumnProperty='fk' then @FK_VALUE when ControlType in ('dfl','dft','dfd') 
            //and isnull(ForeignColumnName,'') not in ('', '0') then 'ft.' + ForeignColumnName when ISNULL(SelectColumnName,'')= '' then ColumnName  else SelectColumnName end AS ColumnName,
            //'[' + ColumnDisplayName + ']' as ColumnAsName,SlNo,
            //ControlType,ColumnId,case when len(parentColumn)> 3 then 't.' + substring(parentColumn, 4, len(parentColumn) - 3) else '' end as ParentControl,
            //isnull(ForeignKeyName, 0) as ForeignKeyName,FilterBy,ISNULL(DefaultValue, '') ,ColumnName,GlobalParamId
            //,isnull(TableName, ''),isnull(ColumnValue, ''),isnull(ColumnText, ''),isnull(ColumnTextExp, ''),isnull(WhereExpression, ''),isnull(OrderByExpression, ''),isnull(SqlExpression, '')


            GridViewModel.Clear();

            foreach (var item in dynTableColumn)
            {
                string columnName;
                string ParentControl;

                string baseColumn = item.ColumnName;
                string ForeignColumnName = item.ForeignColumnName == null ? "" : item.ForeignColumnName;

                if (FK_VALUE != "" && item.ColumnProperty == "fk")
                {
                    columnName = FK_VALUE;
                }
                else if ((item.ControlType == "dfl" || item.ControlType == "dft" || item.ControlType == "dfd") && (ForeignColumnName != "" && ForeignColumnName != "0"))
                {
                    columnName = "ft." + ForeignColumnName;
                }
                else if ((item.SelectColumnName == null ? "" : item.SelectColumnName) == "")
                {
                    columnName = item.ColumnName;
                }
                else
                {
                    columnName = item.SelectColumnName;
                }

                string columnAsName = "[" + item.ColumnDisplayName + "]";

                ParentControl = item.ParentColumn.Length > 3 ? "t." + item.ParentColumn.Substring(4, item.ParentColumn.Length - 3) : "";

                long? ForeignKeyName = item.ForeignKeyName == null ? 0 : item.ForeignKeyName;
                string DefaultValue = item.DefaultValue == null ? "" : item.DefaultValue;
                string Tablename = item.TableName == null ? "" : item.TableName;
                string ColumnValue = item.ColumnValue == null ? "" : item.ColumnValue;
                string ColumnText = item.ColumnText == null ? "" : item.ColumnText;
                string ColumnTextExp = item.ColumnTextExp == null ? "" : item.ColumnTextExp;
                string WhereExpression = item.WhereExpression == null ? "" : item.WhereExpression;
                string OrderByExpression = item.OrderByExpression == null ? "" : item.OrderByExpression;
                string SqlExpression = item.SqlExpression == null ? "" : item.SqlExpression;

                ColumnNameForGridView forGridView = new ColumnNameForGridView(columnName, columnAsName, baseColumn, item.SlNo, item.ControlType, item.ColumnId, ParentControl, ForeignKeyName,
                    item.FilterBy, DefaultValue, item.GlobalParamId, Tablename, ColumnValue, ColumnText, ColumnTextExp, WhereExpression, OrderByExpression, SqlExpression);

                GridViewModel.Add(forGridView);
            }


            return GridViewModel;
        }

        public List<ColumnNameForGridView> DisplayOnModel(IEnumerable<Utl_Dynamic_Table_Column> dynamicTableColumn, string FK_VALUE)
        {

            GridViewModel.Clear();
            
            foreach (var item in dynamicTableColumn)
            {
                string columnName;
                string ParentControl;
                string ForeignColumnName = item.ForeignColumnName == null ? "" : item.ForeignColumnName;
                string baseColumn = item.ColumnName;

                if (FK_VALUE != "" && item.ColumnProperty == "fk")
                {
                    columnName = FK_VALUE;
                }
                else if ((item.ControlType == "dfl" || item.ControlType == "dft" || item.ControlType == "dfd") && (ForeignColumnName != "" && ForeignColumnName != "0"))
                {
                    columnName = "ft." + ForeignColumnName;
                }
                else if ((item.SelectColumnName == null ? "" : item.SelectColumnName) == "")
                {
                    columnName = item.ColumnName;
                }
                else
                {
                    columnName = item.SelectColumnName;
                }

                string columnAsName = item.ColumnProperty == "pk" ? "DataKey" : "[" + item.ColumnDisplayName + "]";

                ParentControl = item.ParentColumn.Length > 3 ? "t." + item.ParentColumn.Substring(4, item.ParentColumn.Length - 3) : "";

                long? ForeignKeyName = item.ForeignKeyName == null ? 0 : item.ForeignKeyName;
                string DefaultValue = item.DefaultValue == null ? "" : item.DefaultValue;
                string ddlTablename = item.TableName == null ? "" : item.TableName;
                string ColumnValue = item.ColumnValue == null ? "" : item.ColumnValue;
                string ColumnText = item.ColumnText == null ? "" : item.ColumnText;
                string ColumnTextExp = item.ColumnTextExp == null ? "" : item.ColumnTextExp;
                string WhereExpression = item.WhereExpression == null ? "" : item.WhereExpression;
                string OrderByExpression = item.OrderByExpression == null ? "" : item.OrderByExpression;
                string SqlExpression = item.SqlExpression == null ? "" : item.SqlExpression;

                ColumnNameForGridView forGridView = new ColumnNameForGridView(columnName, columnAsName, baseColumn, item.SlNo, item.ControlType, item.ColumnId, ParentControl, ForeignKeyName,
                    item.FilterBy, DefaultValue, item.GlobalParamId, ddlTablename, ColumnValue, ColumnText, ColumnTextExp, WhereExpression, OrderByExpression, SqlExpression);

                GridViewModel.Add(forGridView);
            }

            return GridViewModel;
        }


       
        //public void TmpExcuteEventLog(string ModuleId, string TableId, string UserId, string Datakeyid, string DML, string Event, string EventSql, string EventError)
        //{
        //    //readWrite.Write("people.json", "data", jSONString);
        //    string root = "wwwroot";
        //    var path = Path.Combine(
        //    Directory.GetCurrentDirectory(),
        //    root,
        //    location,
        //    fileName);
        //    string jSONString = JsonConvert.SerializeObject(people);
        //    using (var streamWriter = File.CreateText(path))
        //    {
        //        streamWriter.Write(jSONString);
        //    }
        //}

    }
}