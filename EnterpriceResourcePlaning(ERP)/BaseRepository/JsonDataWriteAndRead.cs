﻿using EnterpriceResourcePlaning_ERP_.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace EnterpriceResourcePlaning_ERP_.BaseRepository
{
    public class JsonDataWriteAndRead
    {
        public void JsonDataConvater(string path, JsonTableList modelList, string JsonFileName)
        {
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue, RecursionLimit = 100 };

            string ModuleJsonData = serializer.Serialize(modelList);
            string fulpath = System.Web.Hosting.HostingEnvironment.MapPath(path);
            //string path = Server.MapPath("~/App_Data/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText(fulpath + JsonFileName + ".json", ModuleJsonData);
        }
        public JsonTableList jsonFileRead(long? moduleId/*, string OperatorTbl*/)
        {
            string Path = "";
            JsonTableList jsonTableLists = null;
            //Utl_Dynamic_Operator OperatorTblList = null;

            if (moduleId != 0 && moduleId!= null /*&& OperatorTbl == ""*/)
            {
                Path = System.Web.Hosting.HostingEnvironment.MapPath("~/JsonDataStore/Page_" + moduleId + ".json");
                StreamReader streamReader = new StreamReader(Path);
                string data = streamReader.ReadToEnd();
                jsonTableLists = JsonConvert.DeserializeObject<JsonTableList>(data);
            }
            //else if (OperatorTbl !="" && (moduleId == 0 || moduleId ==null))
            //{
            //    Path = System.Web.Hosting.HostingEnvironment.MapPath("~/JsonDataStore/"+OperatorTbl);
            //    StreamReader streamReader = new StreamReader(Path);
            //    string data = streamReader.ReadToEnd();
            //    OperatorTblList = JsonConvert.DeserializeObject<Utl_Dynamic_Operator>(data);
            //}

            //if (OperatorTblList != null)
            //{
            //    return OperatorTblList;
            //}

            return jsonTableLists;
        }

        //public List<Utl_Dynamic_Operator> OpeTbljsonFile(string OperatorTbl)
        //{
        //    string Path = System.Web.Hosting.HostingEnvironment.MapPath("~/JsonDataStore/"+ OperatorTbl);
        //    StreamReader streamReader = new StreamReader(Path);
        //    string data = streamReader.ReadToEnd();
        //    List<Utl_Dynamic_Operator> OperatorTblList = JsonConvert.DeserializeObject<List<Utl_Dynamic_Operator>>(data);

        //    return OperatorTblList;
        //}

        public void SingleTbljsonFileWrite<T>(List<T> SingleTblList, string fileName)
        {
            //Json file create
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue, RecursionLimit = 100 };
            string OperatorTblJsonData = serializer.Serialize(SingleTblList);
            string fulpath = System.Web.Hosting.HostingEnvironment.MapPath("~/JsonDataStore/");
            System.IO.File.WriteAllText(fulpath + fileName, OperatorTblJsonData);
        }
        public List<T> SingleTbljsonFileRead<T>(string FileName)
        {
            string Path = System.Web.Hosting.HostingEnvironment.MapPath("~/JsonDataStore/" + FileName);
            StreamReader streamReader = new StreamReader(Path);
            string data = streamReader.ReadToEnd();
            List<T> SingleTblList = JsonConvert.DeserializeObject<List<T>>(data);

            return SingleTblList;
        }

    }
}