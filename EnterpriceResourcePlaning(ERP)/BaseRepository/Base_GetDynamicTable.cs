﻿using EnterpriceResourcePlaning_ERP_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.BaseRepository
{
    public class Base_GetDynamicTable
    {

        public string GetFKValue(JsonTableList jsonTableLists, int TableId, string FkId)
        {
            Mdl_Get_Dynamic_Table_Info MDTI = new Mdl_Get_Dynamic_Table_Info();
            List<Utl_Dynamic_Table> utl_Dynamic_Tables = jsonTableLists.Utl_Dynamic_Table;
            List<Utl_Dynamic_Table_Column> DynamicTableColumns = jsonTableLists.Utl_Dynamic_Table_Column;

           

            MDTI.FK_VALUE = "";

            long? ParentTblId = utl_Dynamic_Tables.Where(t => t.TableId == TableId).Select(p => p.ParentTableId).FirstOrDefault();

            MDTI.FK_TABLE_ID = ParentTblId == null ? 0: (long)utl_Dynamic_Tables.Where(t => t.TableId == ParentTblId).Select(s => s.TableId).FirstOrDefault();
            MDTI.FK_TABLE_NAME = utl_Dynamic_Tables.Where(t => t.TableId == ParentTblId).Select(s => s.TableName).FirstOrDefault();

            MDTI.FK_TABLE_PK_COLUMN = DynamicTableColumns.Where(t => t.TableId == MDTI.FK_TABLE_ID && t.ColumnProperty == "pk").Select(c => c.ColumnName).FirstOrDefault();
            MDTI.FK_TABLE_FK_COLUMN = DynamicTableColumns.Where(t => t.TableId == MDTI.FK_TABLE_ID && t.ColumnProperty == "pkt").Select(c => c.ColumnName).FirstOrDefault();

            if (MDTI.FK_TABLE_FK_COLUMN != "" && MDTI.FK_TABLE_NAME != "" && MDTI.FK_TABLE_PK_COLUMN != "")
            {
                string FkValue = "SELECT '" + FkId + "^''" + "+" + MDTI.FK_TABLE_FK_COLUMN + " FROM " + MDTI.FK_TABLE_NAME + " WHERE " + MDTI.FK_TABLE_PK_COLUMN + "=" + FkId + "";

                //SET @FK_VALUE = 'UPDATE #tmpFkValue SET FkValue=(SELECT ''' + @FkId + '^''' + '+' + @FK_TABLE_FK_COLUMN + ' FROM ' + @FK_TABLE_NAME + ' WHERE ' + @FK_TABLE_PK_COLUMN + '=' + @FkId + ')'
                //EXEC(@FK_VALUE)
                //SET @FK_VALUE = 'SELECT ''' + (SELECT FkValue FROM #tmpFkValue)+''''
            }

            // Demo.
            string fk_Value = "fkvalue";

            return fk_Value;
        }

        public string GetDmlSelect(IEnumerable<Utl_Dynamic_Table_Column> tableColumn)
        {

            string data = "";

            foreach (var item in tableColumn)
            {

                string columnName = "";

                string controlType = item.ControlType;
                columnName = (controlType == "dfl" || controlType == "dft" || controlType == "dfd") ? item.ColumnName + item.ColumnId.ToString() : item.ColumnName;

                string caption = item.ColumnDisplayName;
                int maxColWidth = caption.Length;
                int? colWidth = item.ColumnWidth == null ? 10 : item.ColumnWidth;
                int? dbcolWidth = item.DbColumnWidth == null ? 0 : item.DbColumnWidth;

                string colTblRow = "";
                colTblRow = item.ColumnId + "^%FK%^" + columnName + "^%FK%^" + caption + "^%FK%^" + item.ColumnProperty + "^%FK%^" + colWidth.ToString() + "^%FK%^"
                    + controlType + "^%FK%^" + item.ParentColumn + "^%FK%^" + maxColWidth.ToString() + "^%FK%^" + item.GlobalParamId.ToString() + "^%FK%^"
                    + item.GlobalParamValue.ToString() + "^%FK%^" + dbcolWidth.ToString() + "^%FK%^" + item.TransPolicyId.ToString();

                if (data == "")
                {
                    data = colTblRow;
                }
                else
                {
                    data += "^|^" + colTblRow;
                }

            }
            // 1.ColumnId; 2.ColumnName; 3.Caption; 4.ColumnProperty; 5.ColumnWidth; 6.ControlType; 7.ParentColumn; 8.maxColWidth; 9.GlobalParamId; 10.GlobalParamValue; 11.DbColumnWidth; 12.TransPolicyId
            return data;

        }

        public string GetDmlUpdate(int TableId, string DmlType, List<Utl_Dynamic_Table_Column> DynTblCol, IEnumerable<Utl_Dynamic_Table_Function> tmpFunction)
        {

            var jsonDmlTableColumn = from tc in DynTblCol
                                     join tf in tmpFunction on tc.ColumnName equals tf.ColumnName into JsonTableColumn
                                     from jsonTC in JsonTableColumn.DefaultIfEmpty()
                                     where tc.TableId == TableId && tc.IsShow == true
                                     orderby tc.SlNo
                                     select new
                                     {
                                         ColumnId = tc.ColumnId,
                                         ColumnName = tc.ColumnName,
                                         ControlType = tc.ControlType,
                                         ColumnProperty = tc.ColumnProperty,
                                         LovTableId = tc.LovTableId,
                                         ColumnWidth = tc.ColumnWidth,
                                         ColumnDisplayName = tc.ColumnDisplayName,
                                         ParentColumn = tc.ParentColumn,
                                         GlobalParamId = tc.GlobalParamId,
                                         GlobalParamValue = tc.GlobalParamValue,
                                         DbColumnWidth = tc.DbColumnWidth,
                                         TransPolicyId = tc.TransPolicyId,
                                         TableId = tc.TableId,
                                         IsFvUpdate = tc.IsFvUpdate,
                                         IsFvForceEdit = tc.IsFvForceEdit,
                                         IsShow = tc.IsShow,

                                         ColumnNametf = jsonTC == null ? "" : jsonTC.ColumnName
                                     };

            var dmlUpdateTableColumn = jsonDmlTableColumn;
            string data = "";
            int? MAX_COL_WIDTH = null;

            if (DmlType == "update")
            {
                MAX_COL_WIDTH = DynTblCol.Where(t => t.TableId == TableId && t.IsFvUpdate == true && t.IsShow == true)
                    .Select(i => i.ColumnDisplayName.Length).Max();
                MAX_COL_WIDTH = MAX_COL_WIDTH == null ? 0 : MAX_COL_WIDTH;

                dmlUpdateTableColumn = jsonDmlTableColumn.Where(t => t.IsFvUpdate == true);

            }
            else
            {
                dmlUpdateTableColumn = jsonDmlTableColumn.Where(t => (t.IsFvForceEdit == null ? false : t.IsFvForceEdit) == true);
                try
                {
                    MAX_COL_WIDTH = DynTblCol.Where(t => t.TableId == TableId && t.IsFvForceEdit == true && t.IsShow == true).Select(i => i.ColumnDisplayName.Length).Max();
                    MAX_COL_WIDTH = MAX_COL_WIDTH == null ? 0 : MAX_COL_WIDTH;
                }
                catch (Exception e)
                {
                    var msg = e.Message;
                }

            }

            foreach (var item in dmlUpdateTableColumn)
            {

                string columnName = "";

                string controlType = item.ControlType;
                columnName = (controlType == "dfl" || controlType == "dft" || controlType == "dfd") ? item.ColumnName + item.ColumnId.ToString() : item.ColumnName;

                long? lovTaboleId = item.LovTableId == null ? 0 : item.LovTableId;
                string columnProperty = lovTaboleId > 0 ? "lov" : item.ColumnProperty;
                string tfcolumnName = item.ColumnNametf == null ? "" : item.ColumnNametf;
                columnProperty = tfcolumnName != "" ? "sfd" : columnProperty;

                int? columnWidth = item.ColumnWidth == null ? 10 : item.ColumnWidth;
                string caption = item.ColumnDisplayName;


                int? globalParamId = item.GlobalParamId == null ? 0 : item.GlobalParamId;
                int? globalParamValue = item.GlobalParamValue == null ? 0 : item.GlobalParamValue;
                int? dbColumnWith = item.DbColumnWidth == null ? 0 : item.DbColumnWidth;


                string colTblRow = "";
                colTblRow = item.ColumnId + "^%FK%^" + columnName + "^%FK%^" + caption + "^%FK%^" + columnProperty + "^%FK%^" + columnWidth.ToString() + "^%FK%^"
                    + controlType + "^%FK%^" + item.ParentColumn + "^%FK%^" + MAX_COL_WIDTH.ToString() + "^%FK%^" + globalParamId.ToString() + "^%FK%^"
                    + globalParamValue.ToString() + "^%FK%^" + dbColumnWith.ToString() + "^%FK%^" + item.TransPolicyId.ToString();

                //1.ColumnId; 2.ColumnName; 3.ColumnDisplayName; 4.ColumnProperty; 5.ColumnWidth;  6.ControlType; 7.ParentColumn; 8.MAX_COL_WIDTH; 9.GlobalParamId; 10.GlobalParamValue; 11.DbColumnWidth; 12.TransPolicyId;
                if (data == "")
                {
                    data = colTblRow;
                }
                else
                {
                    data += "^|^" + colTblRow;
                }
            }

            return data;

        }

        public string GetDmlInsert(int TableId, List<Utl_Dynamic_Table_Column> DynTblCol, IEnumerable<Utl_Dynamic_Table_Function> tmpFunction)
        {

            int? MAX_COL_WIDTH = null;
            try
            {
                MAX_COL_WIDTH = DynTblCol.Where(t => t.TableId == TableId && t.IsFvInsert == true && t.IsShow == true).Select(s => s.ColumnDisplayName.Length).Max();
            }
            catch (Exception)
            {

                MAX_COL_WIDTH = MAX_COL_WIDTH == null ? 0 : MAX_COL_WIDTH;
            }

            var jsonDmlTableColumn = from tc in DynTblCol
                                     join tf in tmpFunction on tc.ColumnName equals tf.ColumnName into JsonTableColumn
                                     from jsonTC in JsonTableColumn.DefaultIfEmpty()
                                     where tc.TableId == TableId && tc.IsShow == true && tc.IsFvInsert == true
                                     orderby tc.SlNo
                                     select new
                                     {
                                         ColumnId = tc.ColumnId,
                                         ColumnName = tc.ColumnName,
                                         ControlType = tc.ControlType,
                                         ColumnProperty = tc.ColumnProperty,
                                         LovTableId = tc.LovTableId,
                                         ColumnWidth = tc.ColumnWidth,
                                         ColumnDisplayName = tc.ColumnDisplayName,
                                         ParentColumn = tc.ParentColumn,
                                         GlobalParamId = tc.GlobalParamId,
                                         GlobalParamValue = tc.GlobalParamValue,
                                         DbColumnWidth = tc.DbColumnWidth,
                                         TransPolicyId = tc.TransPolicyId,
                                         TableId = tc.TableId,                                     
                                         IsFvUpdate = tc.IsFvUpdate,
                                         IsFvForceEdit = tc.IsFvForceEdit,
                                         IsShow = tc.IsShow,

                                         ColumnNametf = jsonTC == null ? "" : jsonTC.ColumnName
                                     };


            var dmlUpdateTableColumn = jsonDmlTableColumn;
            string data = "";

            foreach (var item in dmlUpdateTableColumn)
            {

                string columnName = "";

                string controlType = item.ControlType;
                columnName = (controlType == "dfl" || controlType == "dft" || controlType == "dfd") ? item.ColumnName + item.ColumnId.ToString() : item.ColumnName;

                long? lovTaboleId = item.LovTableId == null ? 0 : item.LovTableId;
                string columnProperty = lovTaboleId > 0 ? "lov" : item.ColumnProperty;
                string tfcolumnName = item.ColumnNametf == null ? "" : item.ColumnNametf;
                columnProperty = tfcolumnName != "" ? "sfd" : columnProperty;

                int? columnWidth = item.ColumnWidth == null ? 10 : item.ColumnWidth;
                string caption = item.ColumnDisplayName;


                int? globalParamId = item.GlobalParamId == null ? 0 : item.GlobalParamId;
                int? globalParamValue = item.GlobalParamValue == null ? 0 : item.GlobalParamValue;
                int? dbColumnWith = item.DbColumnWidth == null ? 0 : item.DbColumnWidth;


                string colTblRow = "";
                colTblRow = item.ColumnId + "^%FK%^" + columnName + "^%FK%^" + caption + "^%FK%^" + columnProperty + "^%FK%^" + columnWidth.ToString() + "^%FK%^"
                    + controlType + "^%FK%^" + item.ParentColumn + "^%FK%^" + MAX_COL_WIDTH.ToString() + "^%FK%^" + globalParamId.ToString() + "^%FK%^"
                    + globalParamValue.ToString() + "^%FK%^" + dbColumnWith.ToString() + "^%FK%^" + item.TransPolicyId.ToString();

                //1.ColumnId; 2.ColumnName; 3.ColumnDisplayName; 4.ColumnProperty; 5.ColumnWidth;  6.ControlType; 7.ParentColumn; 8.MAX_COL_WIDTH; 9.GlobalParamId; 10.GlobalParamValue; 11.DbColumnWidth; 12.TransPolicyId;
                if (data == "")
                {
                    data = colTblRow;
                }
                else
                {
                    data += "^|^" + colTblRow;
                }
            }

            return data;
        }

        public List<Mdl_TempColumnName> GetDmlBinddataSelect(IEnumerable<Utl_Dynamic_Table_Column> DynamicTblCol, string FK_Value)
        {
   
            List<Mdl_TempColumnName> ColumnNameList = new List<Mdl_TempColumnName>();

            foreach (var item in DynamicTblCol)
            {

                string columnId = item.ColumnId.ToString();
                string columnName = item.ColumnName;
                string columnAsName = "";
                string controlType = item.ControlType;
                string parentControl = item.ParentColumn;
                string foreignColumnName = item.ForeignColumnName == null ? "" : item.ForeignColumnName;
                string defaultValue = "";

                string selectColumnName = item.SelectColumnName == null ? "" : item.SelectColumnName;

                if (FK_Value != "" && item.ColumnProperty == "fk")
                {
                    columnName = FK_Value;
                }
                else if ((controlType == "dfl" || controlType == "dft" || controlType == "dfd") && (foreignColumnName != "" || foreignColumnName != "0"))
                {
                    columnName = "ft." + foreignColumnName;
                }
                else
                {
                    columnName = selectColumnName;
                }

                columnAsName = (controlType == "dfl" || controlType == "dft" || controlType == "dfd") ? columnName + columnId : columnName;
                parentControl = parentControl.Length > 3 ? "t." + parentControl.Substring(4, parentControl.Length - 3) : "";

                Mdl_TempColumnName TempColumnName = new Mdl_TempColumnName(columnName, columnAsName, controlType, columnId, parentControl, defaultValue);
                ColumnNameList.Add(TempColumnName);
                
            }

            return ColumnNameList;

        }

        public List<Mdl_TempColumnName> GetDmlBinddataUpdate(string DmlType, IEnumerable<Utl_Dynamic_Table_Column> DynamicTblCol, string FK_Value)
        {
            List<Mdl_TempColumnName> columnNamesList = new List<Mdl_TempColumnName>();

            if (DmlType == "binddata_fv_update")
            {
                DynamicTblCol = DynamicTblCol.Where(d => d.IsFvUpdate == true);
            }
            else
            {
                DynamicTblCol = DynamicTblCol.Where(d => (d.IsFvForceEdit == null ? false : d.IsFvForceEdit) == true);
            }

            foreach (var item in DynamicTblCol)
            {
                string columnName = item.ColumnName;
                string columnAsName = "";
                string controlType = item.ControlType;
                string columnId = item.ColumnId.ToString();
                string defaultValue = "";

                string foreignColumnName = item.ForeignColumnName == null ? "" : item.ForeignColumnName;

                if (FK_Value != "" && item.ColumnProperty == "fk")
                {
                    columnName = FK_Value;
                }
                else if ((controlType == "dfl" || controlType == "dft" || controlType == "dfd") && (foreignColumnName != "" || foreignColumnName != "0"))
                {
                    columnName = "ft." + foreignColumnName;
                }
                else
                {
                    columnName = columnName == null ? "" : columnName;
                }

                columnAsName = (controlType == "dfl" || controlType == "dft" || controlType == "dfd") ? columnName + columnId : columnName;

                long? foreignKeyName = item.ForeignKeyName == null ? 0 : item.ForeignKeyName;
                controlType = foreignKeyName > 0 ? "altText" : controlType;

                string parentControl = item.ParentColumn;
                parentControl = parentControl.Length > 3 ? "t." + parentControl.Substring(3, parentControl.Length - 3) : "";


                Mdl_TempColumnName TempColumnName = new Mdl_TempColumnName(columnName, columnAsName, controlType, columnId, parentControl, defaultValue);
                columnNamesList.Add(TempColumnName);

            }

            return columnNamesList;
        }

        public List<Mdl_TempColumnName> GetDmlBinddataInsert(IEnumerable<Utl_Dynamic_Table_Column> DynamicTblCol, string FK_Value)
        {
            List<Mdl_TempColumnName> dmlBinddataInsert = new List<Mdl_TempColumnName>();

            foreach (var item in DynamicTblCol)
            {
                string controlType = item.ControlType;
                string foreignColumnName = item.ForeignColumnName == null?"": item.ForeignColumnName;
                string columnName = item.ColumnName == null ? "" : item.ColumnName;

                if(FK_Value != "" && item.ColumnProperty == "fk")
                {
                    columnName = FK_Value;
                }
                else if ((controlType == "dfl" || controlType =="dft"||controlType=="dfd") && (foreignColumnName !="" || foreignColumnName != "0"))
                {
                    columnName = "ft." + foreignColumnName;
                }

                string columnId = item.ColumnId.ToString();
                string columnAsName = (controlType == "dfl" || controlType == "dft" || controlType == "dfd") ? columnName + columnId : columnName;

                long? foreignKeyName = item.ForeignKeyName == null ? 0 : item.ForeignKeyName;
                controlType = foreignKeyName > 0 ? "altText" : controlType;

                string parentControl = item.ParentColumn;
                parentControl = parentControl.Length > 3 ? "t." + parentControl.Substring(3, (parentControl.Length -3)) : "";

                string defaultValue = "";

                Mdl_TempColumnName TempColumnName = new Mdl_TempColumnName(columnName, columnAsName, controlType, columnId, parentControl, defaultValue);
                dmlBinddataInsert.Add(TempColumnName);
            }

            return dmlBinddataInsert;
        }

       
        public string cmdFvUpdate(string TableName, string PK_NAME, string PkId, string DmlType, IEnumerable<Utl_Dynamic_Table_Column> filterTableColumn)
        {
            IEnumerable<string> columnNameList = null;
            string UpdateSql = "";

            if (DmlType == "cmd_fv_update")
            {
                columnNameList = filterTableColumn.Where(i => i.IsFvUpdate == true).Select(s => s.ColumnName);
            }
            else
            {
                columnNameList = filterTableColumn.Where(i => i.IsFvForceEdit == true).Select(s => s.ColumnName);
            }
            //columnName = columnName == null ? "" : columnName;
            foreach (var columnName in columnNameList)
            {

                if (columnName != "")
                {
                    if (UpdateSql == "")
                    {
                        UpdateSql = columnName + "=@" + columnName + "@";
                    }
                    else
                    {
                        UpdateSql = UpdateSql + "," + columnName + "=@" + columnName + "@";
                    }
                }
            }
            if (UpdateSql != "")
            {
                UpdateSql = "UPDATE " + TableName + " SET " + UpdateSql + " WHERE " + PK_NAME + "=" + PkId;
            }

            return UpdateSql;
        }

        public string cmdFvInsert(string TableName, string PK_NAME, IEnumerable<string> ColNames)
        {
            string InsertSql = "";
            string SqlQuery = "";

            if (ColNames != null)
            {
                foreach (var columnName in ColNames)
                {
                    if (InsertSql == "")
                    {
                        SqlQuery = columnName;
                        InsertSql = "@" + columnName + "@";
                    }
                    else
                    {
                        SqlQuery = SqlQuery + "," + columnName;
                        InsertSql = InsertSql + "@" + columnName + "@";
                    }
                }
                //columnName = columnName == null ? "" : columnName;
            }

            if (SqlQuery != "")
            {
                InsertSql = InsertSql.Replace("@" + PK_NAME + "@", "@@" + PK_NAME + "@@");
                //InsertSql = InsertSql.Replace("@" , "@@");
                InsertSql = "INSERT INTO " + TableName + " (" + SqlQuery + ") VALUES(" + InsertSql + ")";
            }

            return InsertSql;
        }

        public string DmlInsertUpdateDelete(int TableId, string TableName, string PK_NAME, string FK_NAME, string PkId, string FkId, string DmlType, string ForeignTableScript, List<Mdl_TempColumnName> tempColumnName, List<Utl_Dynamic_Dropdown_Property> dropdown_Properties)
        {
            string SqlQuery = "";

            foreach (var item in tempColumnName)
            {
                while (item.ColumnName != null)
                {

                    if (item.ColumnName != "")
                    {
                        if (SqlQuery != "")
                        {
                            SqlQuery = SqlQuery + ",";
                        }
                    }

                    if (DmlType == "binddata_fv_select")
                    {
                        if (item.ColumnName.Length >= 7 && item.ColumnName.TrimStart().Substring(0, 7).ToUpper() == "SELECT ")
                        {
                            SqlQuery = SqlQuery + "(" + item.ColumnName + ") AS " + item.ColumnAsName;
                        }
                        else if (item.ControlType == "ddl" || item.ControlType == "dfd")
                        {
                            string SubQueryScript = "";
                            // Procedure...
                            SubQueryScript = TMP_GET_TABLE_DEPENDED_DATA(TableId, Convert.ToInt64(item.ColumnId),  FkId, item.ColumnName, item.ParentControl, dropdown_Properties);

                            if (SubQueryScript != "")
                            {
                                SqlQuery = SqlQuery + "(" + SubQueryScript + ") AS " + item.ColumnAsName;
                            }
                            else
                            {
                                SqlQuery = SqlQuery + "(t." + item.ColumnName + ") AS " + item.ColumnAsName;
                            }
                        }
                        else if ((item.ControlType == "dfl" || item.ControlType == "dft") && ForeignTableScript != "")
                        {
                            SqlQuery = SqlQuery + item.ColumnName + " AS " + item.ColumnAsName;
                        }
                        else
                        {
                            SqlQuery = SqlQuery + "(t." + item.ColumnName + ") AS " + item.ColumnAsName;
                        }

                    }
                    else if (item.ColumnName.Length >= 7 && item.ColumnName.Substring(0, 7).ToUpper() == "SELECT ")
                    {

                        SqlQuery = SqlQuery + "(" + item.ColumnName + ") AS " + item.ColumnAsName;

                    }
                    else if (item.ControlType == "dfl" || item.ControlType == "dft")
                    {
                        if (ForeignTableScript != "")
                        {
                            SqlQuery = SqlQuery + item.ColumnName + " AS " + item.ColumnAsName;
                        }
                        else
                        {
                            SqlQuery = SqlQuery + "''''''" + " AS " + item.ColumnAsName;
                        }
                    }
                    else if (item.ControlType == "altText")
                    {
                        SqlQuery = SqlQuery + "(CAST(t." + item.ColumnName + " AS VARCHAR)) AS " + item.ColumnAsName;
                    }
                    else
                    {
                        SqlQuery = SqlQuery + "(t." + item.ColumnName + ") AS " + item.ColumnAsName;
                    }


                    item.ColumnName = null;
                }
            }

            if (SqlQuery != "")
            {
                SqlQuery = "SELECT " + SqlQuery + " FROM " + TableName + " t " + ForeignTableScript;

                if (DmlType == "binddata_fv_insert")
                {
                    if (PK_NAME != "")
                    {
                        string PK_VALUE = "";
                        PK_VALUE = "Table_23"; //[dbo].[TMP_GET_NEW_PK_VALUE] @TableId,''

                        SqlQuery = SqlQuery.Replace("t." + PK_NAME, PK_VALUE);

                        if (FK_NAME != "" && FK_NAME != null)
                        {
                            SqlQuery = SqlQuery.Replace("t." + FK_NAME, FkId);

                            SqlQuery = SqlQuery + " RIGHT OUTER JOIN (SELECT " + PK_VALUE + " as " + PK_NAME + "," + FkId + " as " + FK_NAME + " ) d ON t." + PK_NAME + "=d." + PK_NAME + " WHERE d." + PK_NAME + "=" + PK_VALUE;

                        }
                        else
                        {
                            SqlQuery = SqlQuery + " RIGHT OUTER JOIN (SELECT " + PK_VALUE + " as " + PK_NAME + " ) d ON t." + PK_NAME + "=d." + PK_NAME + " WHERE d." + PK_NAME + "=" + PK_VALUE;
                        }


                    }
                }
                else
                {
                    SqlQuery = SqlQuery + " WHERE t." + PK_NAME + "=" + PkId;
                }

            }

            return SqlQuery;
        }

        public string TMP_GET_TABLE_DEPENDED_DATA(long TableId, long ColumnId, string FkId, string ColumnName, string ParentControl, List<Utl_Dynamic_Dropdown_Property> dropdown_Properties)
        {

            var dropdownProperty = dropdown_Properties.Where(s => s.ColumnId == ColumnId && s.IsActive == true && (s.SqlExpression == null ? "" : s.SqlExpression) == "" &&
            ((s.TableName == null ? "" : s.TableName) != "" && (s.TableName == null ? "" : s.TableName) != "0") && ((s.ColumnValue == null ? "" : s.ColumnValue) != "" && (s.ColumnValue == null ? "" : s.ColumnValue) != "0") &&
            (((s.ColumnText == null ? "" : s.ColumnText) != "" && (s.ColumnText == null ? "" : s.ColumnText) != "0") || ((s.ColumnTextExp == null ? "" : s.ColumnTextExp) != "" && (s.ColumnTextExp == null ? "" : s.ColumnTextExp) != "0")))
                .Select(s => new { s.TableName, s.ColumnValue, s.ColumnText, s.ColumnTextExp, s.WhereExpression }).ToList();

            string Sql = "";
            string ColumnText = "";

            if (dropdownProperty != null)
            {
                foreach (var item in dropdownProperty)
                {
                    
                    Sql = Sql != "" ? Sql + " UNION ALL " : "";

                    if ((item.ColumnText == null ? "" : item.ColumnText) != "" && (item.ColumnText == null ? "" : item.ColumnText) != "0")
                    {
                        ColumnText = "i." + item.ColumnText;
                    }
                    else if ((item.ColumnTextExp == null ? "" : item.ColumnTextExp) != "" && (item.ColumnTextExp == null ? "" : item.ColumnTextExp) != "0")
                    {
                        ColumnText = item.ColumnTextExp.Replace("@.", "i.");
                    }
                    Sql = Sql + "SELECT " + ColumnText + " AS DataText ";
                    Sql = Sql + " FROM " + item.TableName + " i where i." + item.ColumnValue + "=t." + ColumnName + " and i." + item.ColumnValue + "<>" + "'0'";

                    if (item.WhereExpression != "")
                    {
                        Sql = Sql + " AND " + item.WhereExpression;
                    }
                }

                if ((Sql == null? "": Sql) == "")
                {
                    var colId = dropdown_Properties.Where(w => w.ColumnId == ColumnId && w.IsActive == true && (w.SqlExpression == null ? "" : w.SqlExpression) == ""
                        && (w.TableName == null ? "" : w.TableName) == "" && (w.TableName == null ? "" : w.TableName) == "0" && (w.ColumnValue == null ? "" : w.ColumnValue) != ""
                        && (w.ColumnValue == null ? "" : w.ColumnValue) != "0" && (w.ColumnText == null ? "" : w.ColumnText) != "" && (w.ColumnText == null ? "" : w.ColumnText) != "0").
                        Select(c => c.ColumnId).Distinct();

                    if (colId != null)
                    {
                        Sql = "SELECT ISNULL(i.ColumnText,'''') AS DataText FROM UTL_DYNAMIC_DROPDOWN_PROPERTY i where i.ColumnValue = t." + ColumnName + " and i.ColumnId = " + ColumnId
                            + "  and i.IsActive=1 and ISNULL(i.TableName,'''') in ('''',0) and ISNULL(i.SqlExpression,'''')=''''";
                    }
                }
            }

            if (Sql != "")
            {
                Sql = Sql.Replace("@fk", FkId);
                Sql = Sql.Replace("@parent", ParentControl);
                Sql = Sql.Replace("User_Id=@user and", " ");
                Sql = Sql.Replace("@.", "");
            }


            return Sql;
        }

        public string SplitData(string expression, string SplitExp, int Position)
        {

            string data = "";
            string expres = "";

            List<string> dataList = new List<string>();

            if (expression.IndexOf(SplitExp) > 0)
            {
                data = expression.Substring(1, expression.IndexOf(SplitExp) - 1);
                expres = expression.Substring(expression.IndexOf(SplitExp) + 1, expression.Length - (expression.IndexOf(SplitExp) + 1));

                dataList.Add(data);
            }
            data = expres.Trim();
            dataList.Add(data);

            string value = dataList[Position -1];

            return value;
        }

    }
    
}