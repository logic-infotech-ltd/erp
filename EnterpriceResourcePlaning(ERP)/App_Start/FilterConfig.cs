﻿using System.Web;
using System.Web.Mvc;

namespace EnterpriceResourcePlaning_ERP_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
