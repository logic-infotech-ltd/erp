﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class Utl_Event_Execution_Log
    {
        public int EventLogId { set; get; }
        public string ModuleId { set; get; }
        public string TableId { set; get; }
        public string UserId { set; get; }
        public string DataKeyId { set; get; }

        //  [EventLogId]
        //,[ModuleId]
        //,[TableId]
        //,[UserId]
        //,[DataKeyId]
        public string DML { set; get; }
        public string Event { set; get; }
        public string EventSql { set; get; }
        public string EventError { set; get; }
        public DateTime ExecutionTime { set; get; }

        //,[DML]
        //,[Event]
        //,[EventSql]
        //,[EventError]
        //,[ExecutionTime]
        public bool IsSolved { set; get; }
        public bool Is_Active { set; get; }
        public long Entry_User_Id { set; get; }
        public string Cancel_By { set; get; }
        public DateTime Cancel_Date { set; get; }
        //,[IsSolved]
        //,[Is_Active]
        //,[Entry_User_Id]
        //,[Cancel_By]
        //,[Cancel_Date]
        public string Delete_By { set; get; }
        public DateTime Delete_Date { set; get; }
        //,[Delete_By]
        //,[Delete_Date]
        public bool Is_Cancel { set; get; }
        public bool Is_Deleted { set; get; }
        //,[Is_Cancel]
        //,[Is_Deleted]
        public string Approve_By { set; get; }
        public DateTime Approve_Date { set; get; }
        //,[Approve_By]
        //,[Approve_Date]
        public string Check_By { set; get; }
        public DateTime Check_Date { set; get; }
        //,[Check_By]
        //,[Check_Date]
        public string Entry_By { set; get; }
        public DateTime Entry_Date { set; get; }
        //,[Entry_By]
        //,[Entry_Date]
        public bool Is_Approved { set; get; }
        public string Update_By { set; get; }
        public DateTime Update_Date { set; get; }
        public int Work_Follow_Position { set; get; }
        //,[Is_Approved]
        //,[Update_By]
        //,[Update_Date]
        //,[Work_Follow_Position]
    }
}