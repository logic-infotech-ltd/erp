﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class ColumnNameForGridView
    {

        public ColumnNameForGridView(string columnName, string columnAsName, string baseColumn, decimal slNo, string controlType, long columnId, string parentControl, long? foreignKeyName, int? filterBy, string defaultValue, int? globalParamId, string tablename, string columnValue, string columnText, string columnTextExp, string whereExpression, string orderByExpression, string sqlExpression)
        {
            ColumnName = columnName;
            ColumnAsName = columnAsName;
            SlNo = Convert.ToInt64(slNo);
            ControlType = controlType;
            BaseColumn = baseColumn;
            ColumnId = columnId.ToString();
            ParentControl = parentControl;
            ForeignKeyName = foreignKeyName;
            FilterBy = filterBy;
            DefaultValue = defaultValue;
            GlobalParam = globalParamId;
            ddlTableName = tablename;
            ColumnValue = columnValue;
            ColumnText = columnText;
            ColumnTextExp = columnTextExp;
            WhereExpression = whereExpression;
            OrderByExpression = orderByExpression;
            SqlExpression = sqlExpression;
        }

        public string ColumnName { set; get; }
        public string ColumnAsName { set; get; }
        public long SlNo { set; get; }
        public string ControlType { set; get; }
        public string ColumnId { set; get; }
        public string ParentControl { set; get; }
        public long AltTableId { set; get; }
        public int? FilterBy { set; get; }
        public string DefaultValue { set; get; }
        public string BaseColumn { set; get; }
        public int? GlobalParam { set; get; }
        public long? ForeignKeyName { set; get; }
        public string ddlTableName { set; get; }
        public string ColumnValue { set; get; }
        public string ColumnText { set; get; }
        public string ColumnTextExp { set; get; }
        public string WhereExpression { set; get; }
        public string OrderByExpression { set; get; }
        public string SqlExpression { set; get; }
    }

}