﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class Mdl_TempColumnName
    {

        public Mdl_TempColumnName(string columnName, string columnAsName, string controlType, string columnId, string parentControl, string defaultValue)
        {
            ColumnName = columnName;
            ColumnAsName = columnAsName;
            ControlType = controlType;
            ColumnId = columnId;
            ParentControl = parentControl;
            DefaultValue = defaultValue;
        }

        public string ColumnName { set; get; }
        public string ColumnAsName { set; get; }
        public string ControlType { set; get; }
        public string ColumnId { set; get; }
        public string ParentControl { set; get; }
        public string DefaultValue { set; get; }

    }
}