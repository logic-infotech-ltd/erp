﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class wf_User_Registration
    {
        public int UserId { set; get; }
        public string LogInId { set; get; }
        public string PassWord { set; get; }
        public bool Is_Encrypt { set; get; }
        public bool Do_Encrypt { set; get; }
        public long Emp_Id { set; get; }
        public string UserName { set; get; }
        public int DepartmentId { set; get; }
        public int DesignationId { set; get; }
        public int DefaultModuleId { set; get; }
        public int DefaultCompanyId { set; get; }
        public string Theme_Name { set; get; }
        public string SignaturePath { set; get; }
        public int AdminPrivilegeType { set; get; }
        public int TopPrivilegeId { set; get; }
        public int PvlUserId { set; get; }
        public int PvlModuleId { set; get; }
        public DateTime PW_Set_Date { set; get; }
        public int Expire_Days { set; get; }
        public int Wrong_Try { set; get; }
        public bool IsActive { set; get; }



        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }
        
    }
}