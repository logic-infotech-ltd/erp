﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class CommonModel
    {
        public long ModuleId { set; get; }
        public string TableId { set; get; }
        public string TblFkId { set; get; }
        public string RowId { set; get; }
        public string DataProperty { set; get; }
        public string DataScript { set; get; }

    }
}