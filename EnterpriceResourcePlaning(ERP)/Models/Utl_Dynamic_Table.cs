using System;
using System.Collections.Generic;

namespace EnterpriceResourcePlaning_ERP_.Models
{
 
    public partial class Utl_Dynamic_Table
    {
        ///////////22222222222222
        public Utl_Dynamic_Table(long tableId, long moduleId, long? parentModuleId, long? foreignModuleId, long applicationId, string tableCaption, string moduleTblCaption, string tableName, string refreshType, 
            long parentTableId, long foreignTableId, bool isExternalDataShow, string externalDataCaption, string externalDataFilter, long existingTableId, int moduleWidth, 
            int? moduleHeight, int formWidth, string positionExp, int rowPosition, int colPosition, bool isReadonly, bool isWorkFollow, bool isAutoToggle, 
            string procedureName, string procedureCaption, string procedureValidation, bool proFireOntrans, string reportProcName, bool rptProcForWorksheet, 
            string procFooterView, string procFooterDml, string rptFileName, bool showOnReport, string sqlStatement, string objectType, string templateType, decimal slNo, 
            string developerSynId, string rowId, long entryUserId, string entryBy, DateTime? entryDate, string updateBy, DateTime? updateDate, string checkBy, DateTime? checkDate, 
            string approveBy, DateTime? approveDate, string deleteBy, DateTime? deleteDate, string cancelBy, DateTime? cancelDate, bool isApproved, bool is_Active, bool isDelete, bool 
            isCandel, int workFollowPosition)
        {
            TableId = tableId;
            ModuleId = moduleId;
            ParentModuleId = parentModuleId; //
            ForeignModuleId = foreignModuleId; //
            ApplicationId = applicationId;
            TableCaption = tableCaption;
            ModuleTableCaption = moduleTblCaption;
            TableName = tableName;
            RefreshType = refreshType;
            ParentTableId = parentTableId;
            ForeignTableId = foreignTableId;
            IsExternalDataShow = isExternalDataShow;
            ExternalDataCaption = externalDataCaption;
            ExternalDataFilter = externalDataFilter;
            ExistingTableId = existingTableId;
            ModuleWidth = moduleWidth;
            ModuleHeight = moduleHeight;
            FormWidth = formWidth;
            PositionExp = positionExp;
            RowPosition = rowPosition;
            ColPosition = colPosition;
            IsReadOnly = isReadonly;
            IsWorkFollow = isWorkFollow;
            IsAutoToggle = isAutoToggle;
            ProcedureName = procedureName;
            ProcedureCaption = procedureCaption;
            ProcedureValidation = procedureValidation;
            ProcFireOnTrans = proFireOntrans;
            ReportProcedureName = reportProcName;
            RptProcForWorksheet = rptProcForWorksheet;
            ProcFooterView = procFooterView;
            ProcFooterDml = procFooterDml;
            ReportFileName = rptFileName;
            ShowOnReport = showOnReport;
            SqlStatement = sqlStatement;
            ObjectType = objectType;
            TemplateType = templateType;
            SlNo = slNo;
            DeveloperSyncId = developerSynId;
            Row_Id = rowId;

            Entry_User_Id = entryUserId;
            Entry_By = entryBy;
            Entry_Date = entryDate;
            Update_By = updateBy;
            Update_Date = updateDate;
            Check_By = checkBy;
            Check_Date = checkDate;
            Approve_By = approveBy;
            Approve_Date = approveDate;
            Delete_By = deleteBy;
            Delete_Date = deleteDate;
            Cancel_By = cancelBy;
            Cancel_Date = cancelDate;
            Is_Approved = isApproved;
            Is_Active = is_Active;
            Is_Deleted = isDelete;
            Is_Cancel = isCandel;
            Work_Follow_Position = workFollowPosition;           
        }



        public Nullable<long> TableId { get; set; }
        public Nullable<long> ModuleId { get; set; }
        public long? ParentModuleId { set; get; }
        public long? ForeignModuleId { set; get; }
        public Nullable<long> ApplicationId { get; set; }
        public string TableCaption { get; set; }
        public string ModuleTableCaption { get; set; }
        public string TableName { get; set; }
        public string RefreshType { get; set; }
        public Nullable<long> ParentTableId { get; set; }
        public Nullable<long> ForeignTableId { get; set; }
        public bool IsExternalDataShow { get; set; }
        public string ExternalDataCaption { get; set; }
        public string ExternalDataFilter { get; set; }
        public Nullable<long> ExistingTableId { get; set; }
        public Nullable<int> ModuleWidth { get; set; }
        public Nullable<int> ModuleHeight { get; set; }
        public Nullable<int> FormWidth { get; set; }
        public string PositionExp { get; set; }
        public Nullable<int> RowPosition { get; set; }
        public Nullable<int> ColPosition { get; set; }
        public Nullable<bool> IsReadOnly { get; set; }
        public Nullable<bool> IsWorkFollow { get; set; }
        public Nullable<bool> IsAutoToggle { get; set; }
        public string ProcedureName { get; set; }
        public string ProcedureCaption { get; set; }
        public string ProcedureValidation { get; set; }
        public Nullable<bool> ProcFireOnTrans { get; set; }
        public string ReportProcedureName { get; set; }
        public Nullable<bool> RptProcForWorksheet { get; set; }
        public string ProcFooterView { get; set; }
        public string ProcFooterDml { get; set; }
        public string ReportFileName { get; set; }
        public Nullable<bool> ShowOnReport { get; set; }
        public string SqlStatement { get; set; }
        public string ObjectType { get; set; }
        public string TemplateType { get; set; }
        public Nullable<decimal> SlNo { get; set; }
        public string DeveloperSyncId { get; set; }
        public string Row_Id { get; set; }


        //-----------------------------------
        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<System.DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<System.DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<System.DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<System.DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<System.DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<System.DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }

        //-------------------------------
    }
}
