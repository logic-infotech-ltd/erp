﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class JsonTableList
    {
        public List<Utl_Dynamic_Module> Utl_Dynamic_Module { set; get; }
        public List<Utl_Dynamic_Table> Utl_Dynamic_Table { set; get; }
        public List<Utl_Dynamic_Table_Column> Utl_Dynamic_Table_Column { set; get; }
        public List<Utl_Dynamic_Autocode_Property> Utl_Dynamic_Autocode_Property { set; get; }
        public List<Utl_Dynamic_Dropdown_Property> Utl_Dynamic_Dropdown_Property { set; get; }
        public List<Utl_Dynamic_Filter_Property> Utl_Dynamic_Filter_Property { set; get; }
        public List<Utl_Dynamic_Validation_Property> Utl_Dynamic_Validation_Property { set; get; }
        public List<Utl_Dynamic_Table_Function> Utl_Dynamic_Table_Function { set; get; }



       
    }
}