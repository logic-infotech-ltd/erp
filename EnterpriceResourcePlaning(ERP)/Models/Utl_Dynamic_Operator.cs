﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class Utl_Dynamic_Operator
    {
        public Utl_Dynamic_Operator(int operatorId, string operatorText, string operatorValue, long entryUserId, string entryBy, DateTime? entryDate, string updateBy, DateTime? updateDate, string checkBy, DateTime? checkDate, string approveBy, DateTime? approveDate, string deleteBy, DateTime? deleteDate, string cancelBy, DateTime? cancelDate, bool isApproved, bool is_Active, bool isDelete, bool isCandel, int workFollowPosition)
        {
            OperatorId = operatorId;
            OperatorText = operatorText;
            OperatorValue = operatorValue;
            Entry_User_Id = entryUserId;
            Entry_By = entryBy;
            Entry_Date = entryDate;
            Update_By = updateBy;
            Update_Date = updateDate;
            Check_By = checkBy;
            Check_Date = checkDate;
            Approve_By = approveBy;
            Approve_Date = approveDate;
            Delete_By = deleteBy;
            Delete_Date = deleteDate;
            Cancel_By = cancelBy;
            Cancel_Date = cancelDate;
            Is_Approved = isApproved;
            Is_Active = is_Active;
            Is_Deleted = isDelete;
            Is_Cancel = isCandel;
            Work_Follow_Position = workFollowPosition;
        }

        public int OperatorId { set; get; }
        public string OperatorValue { set; get; }
        public string OperatorText { set; get; }
        public bool Is_Active { set; get; }
        public string Cancel_By { set; get; }
        public DateTime? Cancel_Date { set; get; }
        public string Delete_By { set; get; }
        public DateTime? Delete_Date { set; get; }
        public bool Is_Cancel { set; get; }
        public bool Is_Deleted { set; get; }
        public string Approve_By { set; get; }
        public DateTime? Approve_Date { set; get; }
        public string Check_By { set; get; }
        public DateTime? Check_Date { set; get; }
        public string Entry_By { set; get; }
        public DateTime? Entry_Date { set; get; }
        public long Entry_User_Id { set; get; }
        public bool Is_Approved { set; get; }
        public string Update_By { set; get; }
        public DateTime? Update_Date { set; get; }
        public int Work_Follow_Position { set; get; }
    }
}