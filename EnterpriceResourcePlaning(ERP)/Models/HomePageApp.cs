﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class HomePageApp
    {
       
        public HomePageApp(int id, string application, string imageUrl, string applicationLink)
        {
            this.id = id;
            this.application = application;
            this.imageUrl = imageUrl;
            this.applicationLink = applicationLink;
        }

        public int id { set; get; }
        public string application { set; get; }
        public string imageUrl { set; get; }
        public string applicationLink { set; get; }
    }

    public class MaterialModule
    {
        public MaterialModule(int id, string module, string moduleLink)
        {
            this.id = id;
            this.module = module;
            this.moduleLink = moduleLink;
        }

        public int id { get; set; }
        public string module { get; set; }
        public string moduleLink { get; set; }
    }
    public class ManuBar
    {
        public string ControllerName { set; get; }
        public string ActionName { set; get; }
    }
}