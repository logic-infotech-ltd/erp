using System;
using System.Collections.Generic;

namespace EnterpriceResourcePlaning_ERP_.Models
{

    public partial class Utl_Dynamic_Module
    {
        /////////1111111111111
        public Utl_Dynamic_Module(long moduleId, int applicationId, long exiModulId, bool isNewCopy, string moduleName, string moduleCaption, string reportTitle, bool isDrillDownReport, 
            int categoryId, string imagePath, int moduleType, decimal siNo, bool isActive, string developerSyId, string rowId, long entryUserId, string entryBy, DateTime? entryDate, 
            string updateBy, DateTime? updateDate, string checkBy, DateTime? checkDate, string approveBy, DateTime? approveDate, string deleteBy, DateTime? deleteDate, string cancelBy, 
            DateTime? cancelDate, bool isApproved, bool is_Active, bool isDelete, bool isCandel, int workFollowPosition)
        {
            ModuleId = moduleId;
            ApplicationId = applicationId;
            this.ExistingModuleId = exiModulId;
            IsNewCopy = isNewCopy;
            ModuleName = moduleName;
            ModuleCaption = moduleCaption;
            ReportTitle = reportTitle;
            IsDrilldownReport = isDrillDownReport;
            CategoryId = categoryId;
            ImagePath = imagePath;
            ModuleType = moduleType;
            this.SlNo = siNo;
            IsActive = isActive;
            this.DeveloperSyncId = developerSyId;
            Row_Id = rowId;
            this.Entry_User_Id = entryUserId;
            this.Entry_By = entryBy;
            this.Entry_Date = entryDate;
            this.Update_By = updateBy;
            this.Update_Date = updateDate;
            this.Check_By = checkBy;
            this.Check_Date = checkDate;
            this.Approve_By = approveBy;
            this.Approve_Date = approveDate;
            this.Delete_By = deleteBy;
            this.Delete_Date = deleteDate;
            this.Cancel_By = cancelBy;
            this.Cancel_Date = cancelDate;
            this.Is_Approved = isApproved;
            Is_Active = is_Active;
            this.Is_Deleted = isDelete;
            this.Is_Cancel = isCandel;
            this.Work_Follow_Position = workFollowPosition;
        }


        public long ModuleId { get; set; }
        public Nullable<int> ApplicationId { get; set; }
        public Nullable<long> ExistingModuleId { get; set; }
        public Nullable<bool> IsNewCopy { get; set; }
        public string ModuleName { get; set; }
        public string ModuleCaption { get; set; }
        public string ReportTitle { get; set; }
        public Nullable<bool> IsDrilldownReport { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string ImagePath { get; set; }
        public int ModuleType { get; set; }
        public Nullable<decimal> SlNo { get; set; }
        public bool IsActive { get; set; }
        public string DeveloperSyncId { get; set; }
        public string Row_Id { get; set; }


        //-----------------------------------
        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<System.DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<System.DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<System.DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<System.DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<System.DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }
        //-------------------------------
    }
}
