using System;
using System.Collections.Generic;

namespace EnterpriceResourcePlaning_ERP_.Models
{

    public partial class Utl_Dynamic_Dropdown_Property
    {

        public Utl_Dynamic_Dropdown_Property(long propertyId, long columnId, long moduleId, long tableId, int propertyType, string tableName, string columnValue, string columnText, string columnTextExp, string whereExpression, string orderByExpression, string sqlExpression, bool isActive, string developerSynId, string rowId, long entryUserId, string entryBy, DateTime? entryDate, string updateBy, DateTime? updateDate, string checkBy, DateTime? checkDate, string approveBy, DateTime? approveDate, string deleteBy, DateTime? deleteDate, string cancelBy, DateTime? cancelDate, bool isApproved, bool is_Active, bool isDelete, bool isCandel, int workFollowPosition)
        {
            PropertyId = propertyId;
            ColumnId = columnId;
            ModuleId = moduleId; //
            TableId = tableId;   //
            PropertyType = propertyType;
            TableName = tableName;
            ColumnValue = columnValue;
            ColumnText = columnText;
            ColumnTextExp = columnTextExp;
            WhereExpression = whereExpression;
            OrderByExpression = orderByExpression;
            SqlExpression = sqlExpression;
            IsActive = isActive;
            DeveloperSyncId = developerSynId;
            Row_Id = rowId;

            Entry_User_Id = entryUserId;
            Entry_By = entryBy;
            Entry_Date = entryDate;
            Update_By = updateBy;
            Update_Date = updateDate;
            Check_By = checkBy;
            Check_Date = checkDate;
            Approve_By = approveBy;
            Approve_Date = approveDate;
            Delete_By = deleteBy;
            Delete_Date = deleteDate;
            Cancel_By = cancelBy;
            Cancel_Date = cancelDate;
            Is_Approved = isApproved;
            Is_Active = is_Active;
            Is_Deleted = isDelete;
            Is_Cancel = isCandel;
            Work_Follow_Position = workFollowPosition;

        }

        //5

        public Nullable<long> PropertyId { get; set; }
        public Nullable<long> ColumnId { get; set; }
        public long ModuleId { get; set; }
        public long TableId { get; set; }
        public int PropertyType { get; set; }
        public string TableName { get; set; }
        public string ColumnValue { get; set; }
        public string ColumnText { get; set; }
        public string ColumnTextExp { get; set; }
        public string WhereExpression { get; set; }
        public string OrderByExpression { get; set; }
        public string SqlExpression { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string DeveloperSyncId { get; set; }
        public string Row_Id { get; set; }

        //----------------------------------
        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<System.DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<System.DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<System.DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<System.DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<System.DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<System.DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }
        //-------------------------------

    }
}
