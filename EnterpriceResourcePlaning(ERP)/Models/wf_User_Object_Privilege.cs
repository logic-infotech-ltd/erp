﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class wf_User_Object_Privilege
    {
        public int Privilege_Id { set; get; }
        public int User_Id { set; get; }
        public int Object_Id { set; get; }
        public int Obj_Item_Id { set; get; }
        public int Work_Id { set; get; }


        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }
    }
}