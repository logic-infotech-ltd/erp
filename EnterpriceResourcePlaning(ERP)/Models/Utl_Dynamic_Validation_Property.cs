using System;
using System.Collections.Generic;

namespace EnterpriceResourcePlaning_ERP_.Models
{
   
    public partial class Utl_Dynamic_Validation_Property
    {
       
        public Utl_Dynamic_Validation_Property(long propertyId, long columnId, long tableId, long moduleId, int operatorId, string valueExpression, string valueDataType, string errorMessasge, bool isActive, string developerSynId, string rowId, long entryUserId, string entryBy, DateTime? entryDate, string updateBy, DateTime? updateDate, string checkBy, DateTime? checkDate, string approveBy, DateTime? approveDate, string deleteBy, DateTime? deleteDate, string cancelBy, DateTime? cancelDate, bool isApproved, bool is_Active, bool isDelete, bool isCandel, int workFollowPosition)
        {
            PropertyId = propertyId;
            ColumnId = columnId;
            TableId = tableId;
            ModuleId = moduleId;
            OperatorId = operatorId;
            ValueExpression = valueExpression;
            ValueDataType = valueDataType;
            ErrorMessage = errorMessasge;
            IsActive = isActive;
            DeveloperSyncId = developerSynId;
            Row_Id = rowId;

            Entry_User_Id = entryUserId;
            Entry_By = entryBy;
            Entry_Date = entryDate;
            Update_By = updateBy;
            Update_Date = updateDate;
            Check_By = checkBy;
            Check_Date = checkDate;
            Approve_By = approveBy;
            Approve_Date = approveDate;
            Delete_By = deleteBy;
            Delete_Date = deleteDate;
            Cancel_By = cancelBy;
            Cancel_Date = cancelDate;
            Is_Approved = isApproved;
            Is_Active = is_Active;
            Is_Deleted = isDelete;
            Is_Cancel = isCandel;
            Work_Follow_Position = workFollowPosition;
        }

        //7


        public long PropertyId { get; set; }
        public long ColumnId { get; set; }
        public Nullable<long> TableId { get; set; }
        public Nullable<long> ModuleId { get; set; }
        public int OperatorId { get; set; }
        public string ValueExpression { get; set; }
        public string ValueDataType { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsActive { get; set; }
        public string DeveloperSyncId { get; set; }
        public string Row_Id { get; set; }

        //-------------------------------
        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<System.DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<System.DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<System.DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<System.DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<System.DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<System.DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }

        //-------------------------------
    }
}
