﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class EmployeeDepModel
    {
        public EmployeeDepModel(int empId, string empName, int age, string depName, string depCode, int depId)
        {
            this.empId = empId;
            this.empName = empName;
            this.age = age;
            this.depName = depName;
            this.depCode = depCode;
            this.depId = depId;
        }

        public int empId { set; get; }
        public string empName { set; get; }
        public int age { set; get; }
        public string depName { set; get; }
        public string depCode { set; get; }
        public int depId { set; get; }
    }
}