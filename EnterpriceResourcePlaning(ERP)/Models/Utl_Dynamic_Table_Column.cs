using System;
using System.Collections.Generic;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    ////////////33333333333333
    public partial class Utl_Dynamic_Table_Column
    {

        public Utl_Dynamic_Table_Column(long columnId, long tableId, long moduleId, long lovModuleId, string columnName, string selectColName, string colDisplayName, string colProperty, string controlType, string parentcolumn, long foreignKeyName, string foreignColName, string foreignDataFilterBy, int columnWidth, int dbColumnWidth, int transPolicyId, int globalParamId, int globalParamValue, string defaultValue, int filterBy, int userPrivilege, bool isModuleSelect, bool isFvSelect, bool isGvSelect, bool? isForeignTable, bool? isDefaultFocus, bool isFvUpdate, bool isFvInsert, bool? isFvDataFixed, bool? isFvForceEdit, int dblDataSource, long lovTableId, string tableName, string columnValue, string columnText, string columnTextExp, string whereExpression, string orderByExpression, string sqlExpression,string linkType, int linkPageId, int linkTableId, string linkColumnName, string linkUrl, bool isShow, int reportColumn, decimal slNo, string developerSynId, string rowId, long entryUserId, string entryBy, DateTime? entryDate, string updateBy, DateTime? updateDate, string checkBy, DateTime? checkDate, string approveBy, DateTime? approveDate, string deleteBy, DateTime? deleteDate, string cancelBy, DateTime? cancelDate, bool isApproved, bool is_Active, bool isDelete, bool isCandel, int workFollowPosition)
        {
            ColumnId = columnId;
            TableId = tableId;
            ModuleId = moduleId;
            ColumnName = columnName;
            SelectColumnName = selectColName;
            ColumnDisplayName = colDisplayName;
            ColumnProperty = colProperty;
            ControlType = controlType;
            ParentColumn = parentcolumn;
            ForeignKeyName = foreignKeyName;
            ForeignColumnName = foreignColName;
            ForeignDataFilterBy = foreignDataFilterBy;
            ColumnWidth = columnWidth;
            DbColumnWidth = dbColumnWidth;
            TransPolicyId = transPolicyId;
            GlobalParamId = globalParamId;
            GlobalParamValue = globalParamValue;
            DefaultValue = defaultValue;
            FilterBy = filterBy;
            User_Privilege = userPrivilege;
            IsModuleSelect = isModuleSelect;
            IsFvSelect = isFvSelect;
            IsGvSelect = isGvSelect;
            IsForeignTable = isForeignTable;
            IsDefaultFocus = isDefaultFocus;
            IsFvUpdate = isFvUpdate;
            IsFvInsert = isFvInsert;
            IsFvDataFixed = isFvDataFixed;
            IsFvForceEdit = isFvForceEdit;
            DdlDataSource = dblDataSource;
            LovTableId = lovTableId;
            LovModuleId = lovModuleId;
            TableName = tableName;
            ColumnValue = columnValue;
            ColumnText = columnText;
            ColumnTextExp = columnTextExp;
            WhereExpression = whereExpression;
            OrderByExpression = orderByExpression;
            SqlExpression = sqlExpression;
            LinkType = linkType;
            LinkPageId = linkPageId;
            LinkTableId = linkTableId;
            LinkColumnName = linkColumnName;
            LinkUrl = linkUrl;
            IsShow = isShow;
            ReportColumn = reportColumn;
            SlNo = slNo;
            DeveloperSyncId = developerSynId;
            Row_Id = rowId;

            Entry_User_Id = entryUserId;
            Entry_By = entryBy;
            Entry_Date = entryDate;
            Update_By = updateBy;
            Update_Date = updateDate;
            Check_By = checkBy;
            Check_Date = checkDate;
            Approve_By = approveBy;
            Approve_Date = approveDate;
            Delete_By = deleteBy;
            Delete_Date = deleteDate;
            Cancel_By = cancelBy;
            Cancel_Date = cancelDate;
            Is_Approved = isApproved;
            Is_Active = is_Active;
            Is_Deleted = isDelete;
            Is_Cancel = isCandel;
            Work_Follow_Position = workFollowPosition;
           
        }

        public long ColumnId { get; set; }
        public Nullable<long> TableId { get; set; }
        public Nullable<long> ModuleId { get; set; }
        public string ColumnName { get; set; }
        public string SelectColumnName { get; set; }
        public string ColumnDisplayName { get; set; }
        public string ColumnProperty { get; set; }
        public string ControlType { get; set; }
        public string ParentColumn { get; set; }
        public Nullable<long> ForeignKeyName { get; set; }
        public string ForeignColumnName { get; set; }
        public string ForeignDataFilterBy { get; set; }
        public Nullable<int> ColumnWidth { get; set; }
        public Nullable<int> DbColumnWidth { get; set; }
        public int TransPolicyId { get; set; }
        public Nullable<int> GlobalParamId { get; set; }
        public Nullable<int> GlobalParamValue { get; set; }
        public string DefaultValue { get; set; }
        public Nullable<int> FilterBy { get; set; }
        public Nullable<int> User_Privilege { get; set; }
        public Nullable<bool> IsModuleSelect { get; set; }
        public Nullable<bool> IsFvSelect { get; set; }
        public Nullable<bool> IsGvSelect { get; set; }
        public Nullable<bool> IsForeignTable { get; set; }
        public Nullable<bool> IsDefaultFocus { get; set; }
        public bool IsFvUpdate { get; set; }
        public bool IsFvInsert { get; set; }
        public Nullable<bool> IsFvDataFixed { get; set; }
        public Nullable<bool> IsFvForceEdit { get; set; }
        public Nullable<int> DdlDataSource { get; set; }
        public Nullable<long> LovTableId { get; set; }
        public Nullable<long> LovModuleId { get; set; }
        public string TableName { get; set; }
        public string ColumnValue { get; set; }
        public string ColumnText { get; set; }
        public string ColumnTextExp { get; set; }
        public string WhereExpression { get; set; }
        public string OrderByExpression { get; set; }
        public string SqlExpression { get; set; }
        public string LinkType { get; set; }
        public Nullable<int> LinkPageId { get; set; }
        public Nullable<int> LinkTableId { get; set; }
        public string LinkColumnName { get; set; }
        public string LinkUrl { get; set; }
        public bool IsShow { get; set; }
        public Nullable<int> ReportColumn { get; set; }
        public decimal SlNo { get; set; }
        public string DeveloperSyncId { get; set; }
        public string Row_Id { get; set; }

        //-----------------------------------------------
        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<System.DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<System.DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<System.DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<System.DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<System.DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<System.DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }



    }
}
