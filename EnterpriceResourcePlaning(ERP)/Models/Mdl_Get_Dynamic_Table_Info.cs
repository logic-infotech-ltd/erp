﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class Mdl_Get_Dynamic_Table_Info
    {

        public string Sql { get; set; }
        public string UpdateSql { get; set; }
        public string InsertSql { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string ColumnAsName { get; set; }
        public string PK_NAME { get; set; }
        public string FK_NAME { get; set; }
        public long? FK_TABLE_ID { get; set; }
        public string FK_TABLE_NAME { get; set; }
        public string FK_TABLE_PK_COLUMN { get; set; }
        public string FK_TABLE_FK_COLUMN { get; set; }
        public string FK_VALUE { get; set; }

    }

    
}