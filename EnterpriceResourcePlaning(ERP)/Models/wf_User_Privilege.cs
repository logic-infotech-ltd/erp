﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriceResourcePlaning_ERP_.Models
{
    public class wf_User_Privilege
    {
        public int PrivilegeId { set; get; }
        public int ModuleId { set; get; }
        public int UserId { set; get; }
        public int DesignationId { set; get; }
        public int DepartmentId { set; get; }
        public int WorkId { set; get; }
        public bool IsRevert { set; get; }
        public bool IsOnBehalf { set; get; }
        public bool IsNew { set; get; }
        public bool IsEdit { set; get; }
        public bool IsDelete { set; get; }
        public bool IsReadOnly { set; get; }
        public bool IsReport { set; get; }
        public bool IsPrint { set; get; }
        public long ConditionColumn { set; get; }
        public string Condition { set; get; }
        public bool IsCancel { set; get; }
        public bool IsCheckFor { set; get; }
        public bool IsApproveFor { set; get; }
        public bool IsEditTransDate { set; get; }
        public bool IsDisApprove { set; get; }


        public Nullable<long> Entry_User_Id { get; set; }
        public string Entry_By { get; set; }
        public Nullable<DateTime> Entry_Date { get; set; }
        public string Update_By { get; set; }
        public Nullable<DateTime> Update_Date { get; set; }
        public string Check_By { get; set; }
        public Nullable<DateTime> Check_Date { get; set; }
        public string Approve_By { get; set; }
        public Nullable<DateTime> Approve_Date { get; set; }
        public string Cancel_By { get; set; }
        public Nullable<DateTime> Cancel_Date { get; set; }
        public string Delete_By { get; set; }
        public Nullable<DateTime> Delete_Date { get; set; }
        public bool Is_Approved { get; set; }
        public bool Is_Active { get; set; }
        public bool Is_Cancel { get; set; }
        public bool Is_Deleted { get; set; }
        public int Work_Follow_Position { get; set; }
        
    }
}